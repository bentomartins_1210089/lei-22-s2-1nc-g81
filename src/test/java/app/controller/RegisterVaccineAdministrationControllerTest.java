package app.controller;

import app.domain.model.*;
import app.domain.store.VaccineAdministrationNurseStore;
import app.dto.VaccineAdministrationNurseDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class RegisterVaccineAdministrationControllerTest {
    VaccinationCenter vaccinationCenter = new VaccinationCenter("Centro de Vacinação COVID-19 - ACeS Porto Ocidental", "Quartel Regimento de Transmissões " +
            " | Rua 14 de Agosto, 4250-120 Porto", "961402216", "8", "18", "5", 10, new Email("cv.pt-ocidental@arsnorte.min-saude.pt"));

    Company company = App.getInstance().getCompany();

    VaccineType vType = new VaccineType("VA123", "descricao", "technology", "", "");

    Email email = App.getInstance().getCurrentUserSession().getUserId();

    LocalDateTime ldtime = LocalDateTime.now();

    @Test
    void registerVaccineNurseAdministration() throws ParseException {
        final String DATE_FORMAT = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setLenient(false);
        Date date = formatter.parse("01/01/2000");
        SNSUser user = new SNSUser("Joao", date,154789659, 915246589, "mail@mail.com", 15425638, "rua das flores", "Masculino");
        long snsNumber = 123456789;
        String lotNumber = "21C16-05";
        Integer doseNumber = 1;
        RegisterVaccineAdministrationController controller = new RegisterVaccineAdministrationController();
        VaccineAdministrationNurseStore store = App.getInstance().getCompany().getVaccineAdministrationNurseStore();
        VaccineAdministrationNurseDTO dto = new VaccineAdministrationNurseDTO(snsNumber, vType, vaccinationCenter, ldtime, lotNumber, doseNumber,email);
        VaccineAdministrationNurse vaccineAdministrationNurse = store.create(new VaccineAdministrationNurseDTO(dto.getSnsNumber(), dto.getVaccine(), dto.getVaccinationCenter(), dto.getLocalDateTime(), dto.getLotNumber(), dto.getDoseNumber(),dto.getEmail()));
        Assertions.assertEquals(dto, vaccineAdministrationNurse);
    }

    @Test
    void registerVaccineNurseAdministrationTestFails() throws ParseException {
        final String DATE_FORMAT = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setLenient(false);
        Date date = formatter.parse("01/01/2000");
        SNSUser user = new SNSUser("Joao", date,154789659, 915246589, "mail@mail.com", 15425638, "rua das flores", "Masculino");
        long snsNumber = 123456789;
        String lotNumber = "21C16-05";
        Integer doseNumber = 1;
        RegisterVaccineAdministrationController controller = new RegisterVaccineAdministrationController();
        VaccineAdministrationNurseStore store = App.getInstance().getCompany().getVaccineAdministrationNurseStore();
        VaccineAdministrationNurseDTO dto = new VaccineAdministrationNurseDTO(snsNumber, vType, vaccinationCenter, ldtime, lotNumber, doseNumber,email);
        VaccineAdministrationNurse vaccineAdministrationNurse = store.create(new VaccineAdministrationNurseDTO(dto.getSnsNumber(), dto.getVaccine(), dto.getVaccinationCenter(), dto.getLocalDateTime(), dto.getLotNumber(), dto.getDoseNumber(),dto.getEmail()));
        Assertions.assertNotEquals(dto, vaccineAdministrationNurse);
    }
}