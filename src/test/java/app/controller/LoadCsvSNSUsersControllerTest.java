package app.controller;

import app.domain.model.SNSUser;
import app.domain.store.SNSUserStore;
import app.dto.SNSUserDto;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Bento Martins <1210089>
 */
public class LoadCsvSNSUsersControllerTest {

    LoadCsvSNSUsersController control = new LoadCsvSNSUsersController();
    String path = "src/test/data/randomusers_withheader.csv";

    private App app = App.getInstance();

    @Test
    public void testCsvFileExists() throws FileNotFoundException {

        Boolean tester = control.CsvFileExists(path);

        assertEquals(tester,true);

    }

    @Test
    public void testReadCsv() throws FileNotFoundException {

        List<SNSUserDto> listDto1 = control.readCsv(path);

        assertEquals(listDto1.get(0).getName(),"Ana Morgado");

    }

    @Test
    public void testCheckSNSUsersOnStore() throws FileNotFoundException, ParseException {

        SNSUserStore store= app.getCompany().getSNSUserStore();

        final String DATE_FORMAT = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setLenient(false);

        Date date = formatter.parse("28/06/1927");

        SNSUser snsuser = new SNSUser("Marcos Gama",date,363243814,979647232,"marcos.gama@gmail.com",70738127,"Rua 1","MALE");
        store.addSNSUserToStore(snsuser);

        List<SNSUserDto> listDto3 = control.readCsv(path);

        List<SNSUserDto> listDto4 = control.GetSNSUsersNotInStore(listDto3);

        assertEquals(listDto4.size(),14);

    }

    @Test
    public void testAddSNSUsersDtoToStore() throws FileNotFoundException, ParseException {

        SNSUserStore store= app.getCompany().getSNSUserStore();

        final String DATE_FORMAT = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setLenient(false);

        Date date = formatter.parse("28/06/1927");

        List<SNSUserDto> listDto5 = new ArrayList<>();

        SNSUserDto snsUserDto = new SNSUserDto("John Doe", date, 123456789L, 900000000L, "test1@test.com", 1000001L, "Av Liberdade 1", "MALE");
        SNSUserDto snsUserDto2 = new SNSUserDto("Mary Doe", date, 123456790L, 900000001L, "test2@test.com", 1000002L, "Av Liberdade 2", "FEMALE");

        listDto5.add(snsUserDto);
        listDto5.add(snsUserDto2);

        List<SNSUserDto> listDto6 = control.addSNSUsersDtoToStore(listDto5);

        assertEquals(listDto6.size(),2);

    }

}


