import app.domain.model.VaccineAdministration;
import app.controller.VaccineAdministrationController;
import java.util.ArrayList;

import app.domain.store.VaccineAdministrationStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class VaccineAdministrationControllerTest {

    ArrayList<Double> dosages= new ArrayList<Double>(3);
    ArrayList<Integer> intervals= new ArrayList<Integer>(2);
    VaccineAdministrationController ctrl = new VaccineAdministrationController();
    VaccineAdministrationStore str;
    double dosage1 = 0.1, dosage2 = 0.2, dosage3 = 0.3;
    int interval1 = 10, interval2 = 20;

    @Test
    void createAdminProcess() {
        ArrayList<Double> dosages= new ArrayList<Double>(3);
        ArrayList<Integer> intervals= new ArrayList<Integer>(2);

        dosages.add(dosage1);
        dosages.add(dosage2);
        dosages.add(dosage3);
        intervals.add(interval1);
        intervals.add(interval2);

        VaccineAdministration vaccineAdmin = new VaccineAdministration("Pfizer", "18+", 3, dosages, intervals);
        Assertions.assertEquals(vaccineAdmin, ctrl.createVaccineAdmin("Pfizer", "18+", 3, dosages, intervals));
    }

    @Test
    void validateVaccineAdmin() {
        ArrayList<VaccineAdministration> adminProcessList = new ArrayList<>();
        VaccineAdministration vaccineAdmin = new VaccineAdministration("Pfizer", "18+", 3, dosages, intervals);

        adminProcessList.add(vaccineAdmin);
        str = new VaccineAdministrationStore(adminProcessList);
        ctrl = new VaccineAdministrationController(str);

        Assertions.assertFalse(ctrl.validateVaccineAdmin(vaccineAdmin));
    }
}