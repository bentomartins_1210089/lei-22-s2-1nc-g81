package app.controller;

import app.domain.model.Employee;
import app.domain.store.EmployeeStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mário Borja <1200586>
 */

public class EmployeeRegisterControllerTest {

    @Test
    public void createEmployee() {
        EmployeeRegisterController e = new EmployeeRegisterController();
        String name = "Joao";
        String address = "porto";
        String email = "email@email.com";
        String organizationRole = "administrator";
        String phoneNumber = "123456789";
        String cc = "10000010";
        Employee employee = new Employee(name, address, phoneNumber, email, cc , organizationRole);
        Assertions.assertEquals(employee, e.registerEmployee(name, address, phoneNumber, email, cc, organizationRole));
    }

    @Test
    public void validateEmployee() {

        List<Employee> employeeList = new ArrayList<>();
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        employeeList.add(employee);
        EmployeeStore employeeStore = new EmployeeStore(employeeList);
        EmployeeRegisterController c = new EmployeeRegisterController(employeeStore);
        Assertions.assertFalse(c.validateEmployee(employee));

    }

    @Test
    public void nameValid() {
        EmployeeRegisterController c = new EmployeeRegisterController();
        Assertions.assertTrue(c.nameValid("Joao"));
    }

    @Test
    public void phoneNumberValid() {
        EmployeeRegisterController c = new EmployeeRegisterController();
        Assertions.assertTrue(c.phoneNumberValid("123456789"));
    }

    @Test
    public void ccValid() {
        EmployeeRegisterController c = new EmployeeRegisterController();
        Assertions.assertTrue(c.ccValid("12345678"));
    }

    @Test
    public void addressValid() {
        EmployeeRegisterController c = new EmployeeRegisterController();
        Assertions.assertTrue(c.addressValid("rua das moleiras"));
    }

    @Test
    public void organizationRoleValid() {
        EmployeeRegisterController c = new EmployeeRegisterController();
        Assertions.assertTrue(c.organizationRoleValid("administrator"));
    }

}