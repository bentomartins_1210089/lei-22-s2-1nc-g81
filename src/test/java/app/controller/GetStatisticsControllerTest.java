package app.controller;

import app.domain.model.Company;
import app.domain.model.Employee;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineAdministrationNurse;
import app.domain.shared.Constants;
import app.domain.store.EmployeeStore;
import app.domain.store.VaccineAdministrationNurseStore;
import app.ui.gui.ccoordinator_menu.GetStatisticsController;
import javafx.util.converter.LocalDateTimeStringConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class GetStatisticsControllerTest {

    Company company = App.getInstance().getCompany();

    GetStatisticsController getStatisticsController = new GetStatisticsController();

    VaccinationCenter vaccinationCenter = new VaccinationCenter("Centro de Vacinação COVID-19 - ACeS Porto Ocidental", "Quartel Regimento de Transmissões " +
            " | Rua 14 de Agosto, 4250-120 Porto", "961402216", "8", "18", "5", 10, new Email("cv.pt-ocidental@arsnorte.min-saude.pt"));

    DateTimeFormatter fmtDate = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    VaccineAdministrationNurseStore store = new VaccineAdministrationNurseStore();

    Employee employee = new Employee("Joaquim d'Almeida", "Avenida da Boavista 1670", "910000001", "ccord@lei.sem2.pt",
            "10000001", Constants.ROLE_CENTER_COORDINATOR, vaccinationCenter);

    EmployeeStore employeeStore = company.getEmployeeStore();


    @Test
    public void getAdministratedVaccinesFromIniToEndDateByCenterTest() {

        ArrayList<VaccineAdministrationNurse> vaccineAdmin = getStatisticsController.getAdministratedVaccinesFromIniToEndDateByCenter
                (vaccinationCenter, LocalDate.parse("06-06-2022 10:00", fmtDate), LocalDate.parse("12-06-2022 10:00", fmtDate));

        Assertions.assertEquals(vaccineAdmin.size(), 9);

    }

    @Test
    public void getVaccinationCenterOfCoordinatorTest() {

        employeeStore.addEmployee(employee);

        company.getAuthFacade().addUserWithRole("Joaquim d'Almeida", "ccord@lei.sem2.pt", "123456", Constants.ROLE_CENTER_COORDINATOR);

        company.getAuthFacade().doLogin("ccord@lei.sem2.pt", "123456");

        Email emailTemp = company.getAuthFacade().getCurrentUserSession().getUserId();

        VaccinationCenter vaccinationCenterGetter = getStatisticsController.getVaccinationCenterOfCoordinator();

        Assertions.assertEquals(vaccinationCenterGetter.getName(), vaccinationCenter.getName());

    }

    @Test
    public void getStatisticsFromGivenIntervalCenterTest() {

        employeeStore.addEmployee(employee);

        company.getAuthFacade().addUserWithRole("Joaquim d'Almeida", "ccord@lei.sem2.pt", "123456", Constants.ROLE_CENTER_COORDINATOR);

        company.getAuthFacade().doLogin("ccord@lei.sem2.pt", "123456");

        ArrayList<String> data = getStatisticsController.getStatisticsFromGivenInterval(LocalDate.parse("06-06-2022 10:00", fmtDate), LocalDate.parse("12-06-2022 10:00", fmtDate));;

        Assertions.assertEquals("2022-06-06,1\n",data.get(0).toString());
    }
}