
import app.domain.model.SNSUserArrival;
import app.domain.model.VaccinationCenter;
import app.ui.console.ReceptionistUI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

class SNSUserArrivalTest {

    long snsNumber = 123456789;
    String date = "27/05/2022";
    String time = "20:49";
    SNSUserArrival regUserArrival = new SNSUserArrival(snsNumber, time, date);

    @Test
    void getSnsNumber() {
        Assertions.assertEquals(snsNumber, regUserArrival.getSnsNumber());
    }

    @Test
    void getTime() {
        Assertions.assertEquals(time, regUserArrival.getTime());
    }

    @Test
    void getDate() {
        Assertions.assertEquals(date, regUserArrival.getDate());
    }

    @Test
    void setSnsNumber() {
        regUserArrival.setSnsNumber(snsNumber);
        Assertions.assertEquals(snsNumber, regUserArrival.getSnsNumber());
    }

    @Test
    void setTime() {
        regUserArrival.setTime(time);
        Assertions.assertEquals(time, regUserArrival.getTime());
    }

    @Test
    void setDate() {
        regUserArrival.setDate(date);
        Assertions.assertEquals(date, regUserArrival.getDate());
    }
}