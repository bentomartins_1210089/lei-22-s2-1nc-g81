package app.controller;

import app.domain.model.*;
import app.domain.store.SNSUserStore;
import app.domain.store.VaccineAdministrationStore;
import app.domain.store.VaccineTypeStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class LegacySystemDataControllerTest {

    Company company = App.getInstance().getCompany();
    LegacySystemDataController ctrl = new LegacySystemDataController();
    VaccineTypeStore vcTypeStr = company.getVaccineTypeStore();
    SNSUserStore snsUserStr = company.getSNSUserStore();

    Date date = Date.from(Instant.now());
    SNSUser user = new SNSUser("John Doe", date, 161593120, 900000000L, "test1@test.com", 1000001L, "Av Liberdade 1", "MASCULINO");
    SNSUser user2 = new SNSUser("Mario Dante", date, 123456789, 953456789L, "test2@test.com", 1015210L, "Av D Pedro", "MASCULINO");
    LegacySystemData legSys = new LegacySystemData("161593120", "Spikevax", 1, "21C12-02", "06/11/2022 15:00", "6/11/2022 16:00", "6/11/2022 17:00", "6/11/2022 18:00");
    LegacySystemData legSys2 = new LegacySystemData("123456789", "Cominarty", 1, "21C16-02", "07/11/2022 15:00", "07/11/2022 16:00", "07/11/2022 17:00", "07/11/2022 18:00");

    @Test
    void validateList() {
        snsUserStr.addSNSUserToStore(user);
        snsUserStr.addSNSUserToStore(user2);

        ArrayList<LegacySystemData> list = new ArrayList<>();
        ArrayList<LegacySystemData> newList = new ArrayList<>();
        list.add(legSys);
        list.add(legSys2);

        legSys.setSnsUserName("John Doe");
        legSys2.setSnsUserName("Mario Dante");
        legSys.setVaccineDesc("Covid-19 Moderna");
        legSys2.setVaccineDesc("Covid-19 Pfizer");

        newList.add(legSys);
        newList.add(legSys2);

        Assertions.assertEquals(newList, ctrl.validateList(list));
    }

    @Test
    void csvDosesStringToInt() {
        String string = "Terceira";
        Assertions.assertEquals(3, ctrl.csvDosesStringToInt(string));

        string = "Decima";
        Assertions.assertEquals(10, ctrl.csvDosesStringToInt(string));

        string = "Ultima";
        Assertions.assertEquals(0, ctrl.csvDosesStringToInt(string));
    }

    @Test
    void getSnsUserName() {
        snsUserStr.addSNSUserToStore(user);
        long snsNumber = 161593120;
        Assertions.assertEquals("John Doe", ctrl.getSnsUserName(snsNumber));
    }

    @Test
    void getVaccineDescription() {
        String name = "Cominarty", description = "Covid-19 Pfizer";
        Assertions.assertEquals(description, ctrl.getVaccineDescription(name));

        name = "Spikevax";
        description = "Covid-19 Moderna";
        Assertions.assertEquals(description, ctrl.getVaccineDescription(name));
    }
}