package app.controller;

import app.controller.SNSUserArrivalController;
import app.domain.model.SNSUserArrival;
import app.domain.model.VaccinationCenter;
import app.domain.model.Validator;
import app.domain.store.WaitingRoomStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.util.ArrayList;
import java.util.List;

class SNSUserArrivalControllerTest {

    SNSUserArrivalController ctrl = new SNSUserArrivalController();
    WaitingRoomStore str = new WaitingRoomStore();
    Validator validator = new Validator();
    long snsNumber = 123456789;
    String date = "27/05/2022";
    String time = "20:49";
    VaccinationCenter vaccinationCenter = new VaccinationCenter("Centro de Vacinação Matosinhos", "Travessa da Ferreira 21",
            "221234567","String", new Email("cvmatosinhos@sapo.pt") , "cvmatosinhos.com.pt", "9:00",
            "18:00", "slot",500);
    SNSUserArrival regUserArrival = new SNSUserArrival(snsNumber, time, date);

    @Test
    void createUserArrival() {
        Assertions.assertEquals(regUserArrival, ctrl.createUserArrival(snsNumber, time, date));
    }

    @Test
    void validateDuplicateUserArrival() {
        ArrayList<SNSUserArrival> waitingRoomList = new ArrayList<>();
        waitingRoomList.add(regUserArrival);
        str = new WaitingRoomStore(waitingRoomList);
        ctrl = new SNSUserArrivalController(str);

        Assertions.assertFalse(ctrl.validateDuplicateUserArrival(regUserArrival));

        waitingRoomList.remove(regUserArrival);
        str = new WaitingRoomStore(waitingRoomList);
        ctrl = new SNSUserArrivalController(str);

        Assertions.assertTrue(ctrl.validateDuplicateUserArrival(regUserArrival));
    }

    @Test
    void getVaccinationCenter() {
        ctrl.setVaccinationCenter(vaccinationCenter);
        Assertions.assertEquals(vaccinationCenter, ctrl.getVaccinationCenter());
    }

    @Test
    void setVaccinationCenter() {
        ctrl.setVaccinationCenter(vaccinationCenter);
        Assertions.assertEquals(vaccinationCenter, ctrl.getVaccinationCenter());
    }
}