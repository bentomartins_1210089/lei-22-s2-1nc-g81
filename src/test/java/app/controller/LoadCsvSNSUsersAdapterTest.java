package app.controller;

import app.adapter.LoadCsvSNSUsersAdapter;
import app.dto.SNSUserDto;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Bento Martins <1210089>
 */
public class LoadCsvSNSUsersAdapterTest {

    LoadCsvSNSUsersAdapter adapter = new LoadCsvSNSUsersAdapter();
    String pathWithHeader = "src/test/data/randomusers_withheader.csv";
    String pathWithoutHeader = "src/test/data/randomusers_withoutheader.csv";

    @Test
    public void testSizeGetSNSUsersDtoFromCsv() throws FileNotFoundException {

        List<SNSUserDto> listDto1 = adapter.GetSNSUsersDtoFromCsv(pathWithoutHeader);
        List<SNSUserDto> listDto2 = adapter.GetSNSUsersDtoFromCsv(pathWithHeader);

        assertEquals(listDto1.size(),listDto2.size());

    }

    @Test
    public void testGetSNSUsersDtoFromCsv() throws FileNotFoundException {

        List<SNSUserDto> listDto1 = adapter.GetSNSUsersDtoFromCsv(pathWithoutHeader);
        List<SNSUserDto> listDto2 = adapter.GetSNSUsersDtoFromCsv(pathWithHeader);

        assertEquals(listDto1.get(0).getCcNumber(),listDto2.get(0).getCcNumber());

    }

    @Test
    public void tryReadCsvWithoutHeaders() throws FileNotFoundException {

        List<SNSUserDto> listDto1 = adapter.readCsvWithoutHeaders(pathWithoutHeader);

        assertEquals(listDto1.size(),15);

    }

    @Test
    public void tryReadCsvWithHeaders() throws FileNotFoundException {

        List<SNSUserDto> listDto2 = adapter.readCsvWithHeaders(pathWithHeader);

        assertEquals(listDto2.size(),15);

    }


}


