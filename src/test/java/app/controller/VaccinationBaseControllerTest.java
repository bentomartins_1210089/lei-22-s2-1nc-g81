package app.controller;

import app.domain.model.SNSUser;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineBase;
import app.domain.model.VaccineType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Tassio Gomes <1170065>
 * @author Mário Borja <1200586>
 */

public class VaccinationBaseControllerTest {
    @Test
    public void createVaccine() throws ParseException {
        RegisterSNSUserController c = new RegisterSNSUserController();
        final String DATE_FORMAT = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setLenient(false);
        Date date = formatter.parse("01/01/2000");
        SNSUser user = new SNSUser("Joao", date,154789659, 915246589, "mail@mail.com", 15425638, "rua das flores", "male");
        VaccineType vType = new VaccineType("VA123", "descricao", "technology", "", "");
        VaccinationCenter vc = new VaccinationCenter("Center Health Care Center", "Rua B", "916842345", "fax", new Email("centro2@lei.sem2.pt"), "xtb.pt", "8", "18", "5", 10);
        VaccineBase vaccineBase = new VaccineBase(user, vType, vc);
        app.domain.model.VaccineBase tVaccine = new VaccineBase(user, vType, vc);
        Assertions.assertEquals(vaccineBase, tVaccine);
    }

}