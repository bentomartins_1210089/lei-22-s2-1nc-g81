package app.dto;

import app.domain.model.SNSUser;
import app.dto.SNSUserDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.w3c.dom.ls.LSOutput;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 *
 * @author Bento Martins <1210089>
 */
public class SNSUserDtoTest {

    Date date = Date.from(Instant.now());
    SNSUserDto snsUserDto = new SNSUserDto("John Doe", date, 123456789L, 900000000L, "test1@test.com", 1000001L, "Av Liberdade 1", "MALE");
    SNSUserDto snsUserDtoCopy = new SNSUserDto("John Doe", date, 123456789L, 900000000L, "test1@test.com", 1000001L, "Av Liberdade 1", "MALE");

    @Test
    public void getCCNumber() {
        assertEquals(snsUserDto.getCcNumber(), 1000001L);
    }

    @Test
    public void setCCNumber() {
        snsUserDto.setCcNumber(9000000L);
        assertEquals(snsUserDto.getCcNumber(), 9000000L);
    }

    @Test
    public void getSNSNumber() {
        assertEquals(snsUserDto.getSnsNumber(), 123456789L);
    }

    @Test
    public void setSNSNumber() {
        snsUserDto.setSnsNumber(987654321L);
        assertEquals(snsUserDto.getSnsNumber(), 987654321L);
    }

    @Test
    public void getPhoneNumber() {
        assertEquals(snsUserDto.getPhone(), 900000000L);
    }

    @Test
    public void setPhoneNumber() {
        snsUserDto.setPhone(900000001L);
        assertEquals(snsUserDto.getPhone(), 900000001);
    }

    @Test
    public void getName() {
        assertEquals(snsUserDto.getName(), "John Doe");
    }

    @Test
    public void setName() {
        snsUserDto.setName("Mary Doe");
        assertEquals(snsUserDto.getName(), "Mary Doe");
    }

    @Test
    public void getSex() {
        assertEquals(snsUserDto.getSexUser(), "MALE");
    }

    @Test
    public void setSex() {
        snsUserDto.setSexUser("Female");
        assertEquals(snsUserDto.getSexUser(), "Female");
    }

    @Test
    public void getEmailAddress() {
        assertEquals(snsUserDto.getEmail(), "test1@test.com");
    }

    @Test
    public void setEmailAddress() {
        snsUserDto.setEmail("test2@test.com");
        assertEquals(snsUserDto.getEmail(), "test2@test.com");
    }

    @Test
    public void getBirthDate() {
        assertEquals(snsUserDto.getBirthDate(), date);
    }

    @Test
    public void setBirthDate() throws ParseException {

        final String DATE_FORMAT = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setLenient(false);
        Date date = formatter.parse("01/01/2000");

        snsUserDto.setBirthDate(date);
        assertEquals(snsUserDto.getBirthDate(), date);
    }

    @Test
    public void getAddress() { assertEquals(snsUserDto.getUserAddress(), "Av Liberdade 1");
    }

    @Test
    public void setAddress() {
        snsUserDto.setUserAddress("Av Liberdade 2");
        assertEquals(snsUserDto.getUserAddress(), "Av Liberdade 2");
    }

    SNSUserDto snsUserDto2 = new SNSUserDto("John Doe", "test1@test.com", "123ABCT");

    @Test
    public void getPassword() {
        assertEquals(snsUserDto2.getPassword(), "123ABCT");
    }

    @Test
    public void createSNSUserDTOFull() {
        Assertions.assertNotNull(snsUserDto);
    }

    @Test
    public void createSNSUserDTOSimple() {
        SNSUserDto snsUserDto4 = new SNSUserDto("John Doe", "test1@test.com", "123ABCT");
        Assertions.assertNotNull(snsUserDto2);
    }

    @Test
    public void toStringSNSUserDTO() {
        assertEquals(snsUserDto.toString(), snsUserDtoCopy.toString());
    }

}