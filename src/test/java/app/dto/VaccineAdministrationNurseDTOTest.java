package app.dto;

import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.time.LocalDateTime;

public class VaccineAdministrationNurseDTOTest {
    VaccinationCenter vc = new VaccinationCenter("Center Health Care Center", "Rua B", "916842345", "fax", new Email("centro2@lei.sem2.pt"), "xtb.pt", "8", "18", "5", 10);
    LocalDateTime lc = null;
    String lotNumber = "21C16-05";
    Integer doseNumber = 1;
    VaccineType vType = new VaccineType("VA123", "descricao", "technology", "", "");
    VaccineAdministrationNurseDTO vacDTO = new VaccineAdministrationNurseDTO(123456789, vType, vc, lc , lotNumber, doseNumber, new Email("nurse_2@lei.sem2.pt"));

    @Test
    void getSnsNumber() {
        Assertions.assertEquals(123456789, vacDTO.getSnsNumber());
    }

    @Test
    void getSnsNumberFail() {
        Assertions.assertNotEquals(1234567, vacDTO.getSnsNumber());
    }

    @Test
    void getVaccine() {
        Assertions.assertEquals(vType, vacDTO.getVaccine());
    }

    @Test
    void getVaccineFail() {
        Assertions.assertNotEquals(145, vacDTO.getVaccine());
    }

    @Test
    void getVaccCenter() {
        Assertions.assertEquals(vc, vacDTO.getVaccinationCenter());
    }

    @Test
    void getVaccCenterFail(){
        Assertions.assertNotEquals("ExemploErrado", vacDTO.getVaccinationCenter());
    }

    @Test
    void getLotNumber(){
        Assertions.assertEquals(lotNumber, vacDTO.getLotNumber());
    }

    @Test
    void getLotNumberFail(){
        Assertions.assertNotEquals("1432-1B", vacDTO.getLotNumber());
    }

    @Test
    void getDoseNumber(){
        Assertions.assertEquals(doseNumber, vacDTO.getDoseNumber());
    }

    @Test
    void getDoseNumberFail(){
        Assertions.assertNotEquals(1234, vacDTO.getDoseNumber());
    }

}
