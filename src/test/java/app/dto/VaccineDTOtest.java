package app.dto;

import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineType;
import app.dto.VaccineScheduleDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.text.ParseException;
import java.time.LocalDateTime;

/**
 * @author Tassio Gomes <1170065>
 * @author Mário Borja <1200586>
 */

public class VaccineDTOtest {
    VaccineType vType = new VaccineType("VA123", "descricao", "technology", "", "");
    VaccinationCenter vc = new VaccinationCenter("Center Health Care Center", "Rua B", "916842345", "fax", new Email("centro2@lei.sem2.pt"), "xtb.pt", "8", "18", "5", 10);
    LocalDateTime lc = null;
    VaccineScheduleDto vaccine = new VaccineScheduleDto(123154897, vType, vc, lc);

    @Test
    void getSnsNumber(){
        Assertions.assertEquals(123154897, vaccine.getSnsNumber());
        }

    @Test
    public void getVaccineType() {
        Assertions.assertEquals(vType, vaccine.getVaccineType());
    }

    @Test
    public void getVacCenter() {
        Assertions.assertEquals(vc, vaccine.getVaccinationCenter());
    }

    @Test
    public void getLocalTime() {
        Assertions.assertEquals(lc, vaccine.getLocalDateTime());
    }

}
