package app.model;

import app.domain.model.VaccineAdministration;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class VaccineAdministrationTest {

    ArrayList<Double> dosages= new ArrayList<Double>(3);
    ArrayList<Integer> intervals= new ArrayList<Integer>(2);
    VaccineAdministration adminProcess = new VaccineAdministration("Pfizer", "18+", 3, dosages, intervals);
    double dosage1 = 0.1, dosage2 = 0.2, dosage3 = 0.3;
    int interval1 = 10, interval2 = 20;

    @Test
    void getVaccine() {
        Assertions.assertEquals("Pfizer", adminProcess.getVaccine());
    }

    @Test
    void getAge() {
        Assertions.assertEquals("18+", adminProcess.getAge());
    }

    @Test
    void getNumDoses() {
        Assertions.assertEquals(3, adminProcess.getNumDoses());
    }

    @Test
    void getDosages() {
        ArrayList<Double> dosages= new ArrayList<Double>(3);
        dosages.add(dosage1);
        dosages.add(dosage2);
        dosages.add(dosage3);

        adminProcess = new VaccineAdministration("Pfizer", "18+", 3, dosages, intervals);
        Assertions.assertEquals(dosages, adminProcess.getDosages());
    }

    @Test
    void getIntervals() {
        ArrayList<Integer> intervals= new ArrayList<Integer>(2);
        intervals.add(interval1);
        intervals.add(interval2);

        adminProcess = new VaccineAdministration("Pfizer", "18+", 3, dosages, intervals);
        Assertions.assertEquals(intervals, adminProcess.getIntervals());
    }

    @Test
    void setVaccine() {
        adminProcess.setVaccine("Pfizer");
        Assertions.assertEquals("Pfizer", adminProcess.getVaccine());
    }

    @Test
    void setAge() {
        adminProcess.setAge("18+");
        Assertions.assertEquals("18+", adminProcess.getAge());
    }

    @Test
    void setNumDoses() {
        adminProcess.setNumDoses(3);
        Assertions.assertEquals(3, adminProcess.getNumDoses());
    }

    @Test
    void setDosages() {
        ArrayList<Double> dosages= new ArrayList<Double>(3);
        adminProcess.setDosages(dosages);
        Assertions.assertEquals(dosages, adminProcess.getDosages());
    }

    @Test
    void setInterval() {
        ArrayList<Integer> intervals= new ArrayList<Integer>(2);
        adminProcess.setInterval(intervals);
        Assertions.assertEquals(intervals, adminProcess.getIntervals());
    }

    @Test
    void addDosage() {
        ArrayList<Double> dosages= new ArrayList<Double>(3);
        dosages.add(dosage1);
        dosages.add(dosage2);
        dosages.add(dosage3);

        adminProcess.addDosage(dosage1);
        adminProcess.addDosage(dosage2);
        adminProcess.addDosage(dosage3);
        Assertions.assertEquals(dosages, adminProcess.getDosages());
    }

    @Test
    void addInterval() {
        ArrayList<Integer> intervals= new ArrayList<Integer>(2);
        intervals.add(interval1);
        intervals.add(interval2);

        adminProcess.addInterval(interval1);
        adminProcess.addInterval(interval2);
        Assertions.assertEquals(intervals, adminProcess.getIntervals());
    }
}