package app.model;

import app.domain.model.Employee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mário Borja <1200586>
 */

public class EmployeeTest {

    @Test
    public void getOrganizationRole() throws IllegalArgumentException {
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        Assertions.assertEquals("administrator", employee.getRole());
    }

    @Test
    public void setOrganizationRole() throws IllegalArgumentException {
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        employee.setRole("administrator");
        Assertions.assertEquals("administrator", employee.getRole());
    }

    @Test
    public void getPhoneNumber() throws IllegalArgumentException {
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        Assertions.assertEquals("123456789", employee.getPhoneNumber());
    }

    @Test
    public void setPhoneNumber() throws IllegalArgumentException {
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        employee.setPhoneNumber("123456789");
        Assertions.assertEquals("123456789", employee.getPhoneNumber());
    }

    @Test
    public void getAddress() throws IllegalArgumentException {
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        Assertions.assertEquals("estrada das moleiras", employee.getAddress());
    }

    @Test
    public void setAddress() throws IllegalArgumentException {
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        employee.setAddress("estrada das moleiras");
        Assertions.assertEquals("estrada das moleiras", employee.getAddress());
    }

    @Test
    public void getCC() throws IllegalArgumentException {
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        Assertions.assertEquals("12345678", employee.getCc());
    }

    @Test
    public void setSOCCode() throws IllegalArgumentException {
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        employee.setCc("12345678");
        Assertions.assertEquals("12345678", employee.getCc());
    }

    @Test
    public void getName() throws IllegalArgumentException {
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        Assertions.assertEquals("Joao", employee.getName());
    }

    @Test
    public void setName() throws IllegalArgumentException {
        Employee employee = new Employee("", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        employee.setName("Beatriz Cardoso Borges");
        Assertions.assertEquals("Beatriz Cardoso Borges", employee.getName());
    }

    @Test
    public void getEmail() throws IllegalArgumentException {
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        Assertions.assertEquals("email@email.com", employee.getEmail());
    }

    @Test
    public void setEmail() throws IllegalArgumentException {
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "", "12345678", "administrator");
        employee.setEmail("email@email.com");
        Assertions.assertEquals("email@email.com", employee.getEmail());
    }

    @Test
    public void testEquals() throws IllegalArgumentException {
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "", "12345678", "administrator");
        Assertions.assertTrue(employee.equals(employee));
    }

    @Test()
    public void createEmployeeByOmission() throws IllegalArgumentException {
        Employee employee = new Employee();
        Assertions.assertNotNull(employee);
    }

    @Test()
    public void createValidEmployee() throws IllegalArgumentException {
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "", "12345678", "administrator");
        Assertions.assertNotNull(employee);
    }

}