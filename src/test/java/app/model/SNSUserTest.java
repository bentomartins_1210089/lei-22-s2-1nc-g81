package app.model;

import app.domain.model.SNSUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Bento Martins <1210089>
 */
public class SNSUserTest {

    Date date = Date.from(Instant.now());
    SNSUser snsUser = new SNSUser("John Doe", date, 123456789L, 900000000L, "test1@test.com", 1000001L, "Av Liberdade 1", "MALE");

    @Test
    public void getCCNumber() {
        assertEquals(snsUser.getCcNumber(), 1000001L);
    }

    @Test
    public void setCCNumber() {
        snsUser.setCcNumber(9000000L);
        assertEquals(snsUser.getCcNumber(), 9000000L);
    }

    @Test
    public void getSNSNumber() {
        assertEquals(snsUser.getSnsNumber(), 123456789L);
    }

    @Test
    public void setSNSNumber() {
        snsUser.setSnsNumber(987654321L);
        assertEquals(snsUser.getSnsNumber(), 987654321L);
    }

    @Test
    public void getPhoneNumber() {
        assertEquals(snsUser.getPhone(), 900000000L);
    }

    @Test
    public void setPhoneNumber() {
        snsUser.setPhone(900000001L);
        assertEquals(snsUser.getPhone(), 900000001);
    }

    @Test
    public void getName() {
        assertEquals(snsUser.getName(), "John Doe");
    }

    @Test
    public void setName() {
        snsUser.setName("Mary Doe");
        assertEquals(snsUser.getName(), "Mary Doe");
    }

    @Test
    public void getSex() {
        assertEquals(snsUser.getSexUser(), "MALE");
    }

    @Test
    public void setSex() {
        snsUser.setSexUser("Female");
        assertEquals(snsUser.getSexUser(), "Female");
    }

    @Test
    public void getEmailAddress() {
        assertEquals(snsUser.getEmail(), "test1@test.com");
    }

    @Test
    public void setEmailAddress() {
        snsUser.setEmail("test2@test.com");
        assertEquals(snsUser.getEmail(), "test2@test.com");
    }

    @Test
    public void getBirthDate() {
        assertEquals(snsUser.getBirthDate(), date);
    }

    @Test
    public void setBirthDate() throws ParseException {

        final String DATE_FORMAT = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setLenient(false);
        Date date = formatter.parse("01/01/2000");

        snsUser.setBirthDate(date);
        assertEquals(snsUser.getBirthDate(), date);
    }

    @Test
    public void getAddress() { assertEquals(snsUser.getUserAddress(), "Av Liberdade 1");
    }

    @Test
    public void setAddress() {
        snsUser.setUserAddress("Av Liberdade 2");
        assertEquals(snsUser.getUserAddress(), "Av Liberdade 2");
    }

    @Test
    public void createSNSUser() {
        Assertions.assertNotNull(snsUser);
    }

    SNSUser snsUser2 = new SNSUser("John Doe", date, 123456789L, 900000000L, "test1@test.com", 1000001L, "Av Liberdade 1");

    @Test
    public void createSNSUserWithoutSex() {
        Assertions.assertNotNull(snsUser2);
    }
    @Test
    public void toStringSNSUserDTO() {
        Assertions.assertNotEquals(snsUser.toString(), snsUser2.toString());
    }

}