package app.store;

import app.domain.model.SNSUser;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineBase;
import app.domain.model.VaccineType;
import app.domain.store.VaccinationCenterStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Tassio Gomes <1170065>
 * @author Mário Borja <1200586>
 */

public class VaccinationCenterStoreTest {
    @Test
    public void validateVaccine() throws ParseException {
        final String DATE_FORMAT = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setLenient(false);
        Date date = formatter.parse("01/01/2000");
        SNSUser user = new SNSUser("Joao", date,154789659, 915246589, "mail@mail.com", 15425638, "rua das flores", "male");
        VaccineType vType = new VaccineType("VA123", "descricao", "technology", "", "");
        VaccinationCenter vc = new VaccinationCenter("Center Health Care Center", "Rua B", "916842345", "fax", new Email("centro2@lei.sem2.pt"), "xtb.pt", "8", "18", "5", 10);
        VaccineBase vaccineBase = new VaccineBase(user, vType, vc);
        app.domain.model.VaccineBase tVaccine = new VaccineBase(user, vType, vc);

        List<VaccineBase> vaccineBaseList = new ArrayList<>();
        vaccineBaseList.add(tVaccine);
        VaccinationCenterStore store = new VaccinationCenterStore(vaccineBaseList);

        Assertions.assertTrue(store.validateVaccine(vaccineBase));
    }

    @Test
    public void selectUser() {
        Date date = Date.from(Instant.now());
        SNSUser snsUser = new SNSUser("John Doe", date, 123456789L, 900000000L, "test1@test.com", 1000001L, "Av Liberdade 1", "MALE");
        List<SNSUser> snsUserList = new ArrayList<>();
        snsUserList.add(snsUser);
        VaccinationCenterStore store = new VaccinationCenterStore();


        Assertions.assertEquals(snsUser, store.selectUser(123456789L, snsUserList));
    }

    @Test
    public void showClient() {
        Date date = Date.from(Instant.now());
        SNSUser snsUser = new SNSUser("John Doe", date, 123456789L, 900000000L, "test1@test.com", 1000001L, "Av Liberdade 1", "MALE");
        VaccinationCenterStore store = new VaccinationCenterStore();

        Assertions.assertEquals(snsUser, store.showClient(snsUser));
    }
}
