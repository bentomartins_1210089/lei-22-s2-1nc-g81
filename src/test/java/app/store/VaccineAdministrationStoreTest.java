package app.store;

import app.domain.model.VaccineAdministration;
import app.domain.store.VaccineAdministrationStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class VaccineAdministrationStoreTest {

    VaccineAdministration adminProcess = new VaccineAdministration();
    VaccineAdministrationStore str = new VaccineAdministrationStore();

    @Test
    void addAdminProcess() {
        ArrayList<VaccineAdministration> adminProcessList = new ArrayList<>();
        adminProcessList.add(adminProcess);
        str.addAdminProcess(adminProcess);
        Assertions.assertEquals(adminProcessList, str.getAdminProcessList());
    }

    @Test
    void getAdminProcessList() {
        ArrayList<VaccineAdministration> adminProcessList = new ArrayList<>();
        adminProcessList.add(adminProcess);
        str = new VaccineAdministrationStore(adminProcessList);
        Assertions.assertEquals(adminProcessList, str.getAdminProcessList());
    }
}