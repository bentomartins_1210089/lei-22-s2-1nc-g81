package app.store;

import app.domain.model.SNSUserArrival;
import app.domain.model.VaccinationCenter;
import app.domain.store.WaitingRoomStore;
import app.ui.console.ReceptionistUI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.util.ArrayList;
import java.util.List;

class WaitingRoomStoreTest {

    WaitingRoomStore str = new WaitingRoomStore();
    long snsNumber = 100000000;
    String date = "27/05/2022";
    String time = "20:49";
    SNSUserArrival regUserArrival = new SNSUserArrival(snsNumber, time, date);

    @Test
    void addWaitingUser() {
        ArrayList<SNSUserArrival> waitingRoomList = new ArrayList<>();
        waitingRoomList.add(regUserArrival);
        str.addWaitingUser(regUserArrival);
        Assertions.assertEquals(waitingRoomList, str.getWaitingRoomList());
    }

    @Test
    void getWaitingRoomList() {
        ArrayList<SNSUserArrival> waitingRoomList = new ArrayList<>();
        waitingRoomList.add(regUserArrival);
        str = new WaitingRoomStore(waitingRoomList);
        Assertions.assertEquals(waitingRoomList, str.getWaitingRoomList());
    }
}