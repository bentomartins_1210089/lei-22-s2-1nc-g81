package app.store;

import app.controller.App;
import app.controller.LegacySystemDataController;
import app.domain.model.Company;
import app.domain.model.LegacySystemData;
import app.domain.model.SNSUser;
import app.domain.store.LegacySystemStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class LegacySystemStoreTest {

    Date date = Date.from(Instant.now());
    SNSUser user = new SNSUser("John Doe", date, 161593120, 900000000L, "test1@test.com", 1000001L, "Av Liberdade 1", "MASCULINO");
    SNSUser user2 = new SNSUser("Mario Dante", date, 123456789, 953456789L, "test2@test.com", 1015210L, "Av D Pedro", "MASCULINO");
    LegacySystemData legSys = new LegacySystemData("161593120", "Spikevax", 1, "21C12-02", "06/11/2022 15:00", "6/11/2022 16:00", "6/11/2022 17:00", "6/11/2022 18:00");
    LegacySystemData legSys2 = new LegacySystemData("123456789", "Cominarty", 1, "21C16-02", "07/11/2022 15:00", "07/11/2022 16:00", "07/11/2022 17:00", "07/11/2022 18:00");

    @Test
    void addUser() {
        LegacySystemStore str = new LegacySystemStore();
        ArrayList<LegacySystemData> list = new ArrayList<>();
        list.add(legSys);
        str.addUser(legSys);
        Assertions.assertEquals(list, str.getLegacySystemStore());
    }

    @Test
    void removeUser() {
        LegacySystemStore str = new LegacySystemStore();
        ArrayList<LegacySystemData> list = new ArrayList<>();
        list.add(legSys);
        list.add(legSys2);

        str.setLegacySysList(list);

        str.removeUser(legSys);
        list.remove(0);
        Assertions.assertEquals(list, str.getLegacySystemStore());
    }

    @Test
    void validateDuplicateList() {
        LegacySystemStore str = new LegacySystemStore();
        ArrayList<LegacySystemData> list = new ArrayList<>();
        list.add(legSys);

        Assertions.assertTrue(str.validateDuplicate(list));

        list.add(legSys);
        Assertions.assertFalse(str.validateDuplicate(list));
    }

    @Test
    void validateDuplicateUser() {
        LegacySystemStore str = new LegacySystemStore();
        str.addUser(legSys);

        Assertions.assertFalse(str.validateDuplicate(legSys));
        Assertions.assertTrue(str.validateDuplicate(legSys2));
    }

    @Test
    void getLegacySystemStore() {
        LegacySystemStore str = new LegacySystemStore();
        ArrayList<LegacySystemData> list = new ArrayList<>();
        list.add(legSys);
        str.addUser(legSys);
        Assertions.assertEquals(list, str.getLegacySystemStore());
    }

    @Test
    void setLegacySysList() {
        LegacySystemStore str = new LegacySystemStore();
        ArrayList<LegacySystemData> list = new ArrayList<>();
        list.add(legSys);
        str.setLegacySysList(list);
        Assertions.assertEquals(list, str.getLegacySystemStore());
    }
}