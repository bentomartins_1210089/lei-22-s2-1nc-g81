package app.store;

import app.domain.model.Employee;
import app.domain.store.EmployeeStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mário Borja <1200586>
 */

public class EmployeeStoreTest {

    @Test
    public void addEmployeeSuccessfully() throws IllegalArgumentException {
        List<Employee> employees=new ArrayList<>();
        EmployeeStore employeeStore=new EmployeeStore(employees);
        Employee employee = employeeStore.registerEmployee("123456789", "estrada das moleiras", "12345678", "administrator", "Joao", "email@email.com");
        employeeStore.addEmployee(employee);
    }

    @Test
    public void createValidEmployee() throws IllegalArgumentException {
        List<Employee> employees = new ArrayList<>();
        EmployeeStore employeeStore = new EmployeeStore(employees);
        Employee employee = employeeStore.registerEmployee("123456789", "estrada das moleiras", "12345678", "administrator", "Joao", "email@email.com");
        EmployeeStore store = new EmployeeStore(new ArrayList<>());
        Assertions.assertTrue(store.validateEmployee(employee));
    }

    @Test
    public void saveTwoEmployeesWithSameAttributes() throws IllegalArgumentException{
        EmployeeStore store = new EmployeeStore(new ArrayList<Employee>());
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        store.addEmployee(employee);
        Employee employee1 = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        Assertions.assertFalse(store.validateEmployee(employee1));
    }

    @Test
    public void add() {
        List<Employee> employeeList = new ArrayList<>();
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        employeeList.add(employee);
        EmployeeStore es = new EmployeeStore(employeeList);
        Assertions.assertTrue(es.addEmployee(employee));
    }

    @Test
    public void existsEmployee() {
        List<Employee> employeeList = new ArrayList<>();
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        employeeList.add(employee);
        EmployeeStore es = new EmployeeStore(employeeList);
        Assertions.assertTrue(es.existsEmployee(employee));
    }

    @Test
    public void numberOfEmployees() {
        List<Employee> employeeList = new ArrayList<>();
        Employee employee = new Employee("Joao", "estrada das moleiras", "123456789", "email@email.com", "12345678", "administrator");
        employeeList.add(employee);
        EmployeeStore es = new EmployeeStore(employeeList);
        Assertions.assertEquals(1,es.numberOfEmployees());
    }
}