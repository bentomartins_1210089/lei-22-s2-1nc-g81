package app.store;

import app.controller.App;
import app.controller.RegisterVaccineAdministrationController;
import app.domain.model.*;
import app.domain.store.VaccineAdministrationNurseStore;
import app.dto.VaccineAdministrationNurseDTO;
import app.dto.VaccineScheduleDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VaccineAdministrationNurseTest {

    VaccinationCenter vaccinationCenter = new VaccinationCenter("Centro de Vacinação COVID-19 - ACeS Porto Ocidental", "Quartel Regimento de Transmissões " +
            " | Rua 14 de Agosto, 4250-120 Porto", "961402216", "8", "18", "5", 10, new Email("cv.pt-ocidental@arsnorte.min-saude.pt"));

    @Test
    public void getVaccineAdministrationNurseBySnsNumberAndVaccine() {
        Company company = App.getInstance().getCompany();
        int snsNumber=123456789;
        VaccineType vt1 = company.getVaccineTypeStore().getVcTypeList().get(0);
        Assertions.assertDoesNotThrow(() -> {
            company.getVaccineAdministrationNurseStore().getVaccineAdministrationBySnsNumber(snsNumber, vt1);
        });
    }

    @Test

    public void getVaccineAdministrationList() throws ParseException {
        final String DATE_FORMAT = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setLenient(false);
        Date date = formatter.parse("01/01/2000");

        SNSUser user = new SNSUser("Joao", date,154789659, 915246589, "mail@mail.com", 15425638, "rua das flores", "Masculino");

        Company company = App.getInstance().getCompany();

        List<VaccineAdministrationNurse> result = company.getVaccineAdministrationNurseStore().getVaccineAdministrationList();

        result.clear();

        VaccineType vt1 = company.getVaccineTypeStore().getVcTypeList().get(0);

        company.getVaccineTypeStore().addVcType(vt1);

        LocalDateTime localDateTime = LocalDateTime.now();

        VaccineBase vaccine1 = company.getVaccinationCenterStore().createVaccine(user, vt1, vaccinationCenter);

        company.getVaccinationCenterStore().addVaccine(vaccine1);

        VaccineAdministrationNurseDTO va = new VaccineAdministrationNurseDTO(123456782, vt1, vaccinationCenter, localDateTime, "21C16-05", 1, new Email("nurse_2@lei.sem2.pt"));

        VaccineAdministrationNurse va1 = company.getVaccineAdministrationNurseStore().create(va);

        company.getVaccineAdministrationNurseStore().save(va1);

        List<VaccineAdministrationNurse> expected = new ArrayList<>();

        expected.add(va1);

        Assertions.assertEquals(expected.size(), result.size());

        Assertions.assertEquals(expected, result);

    }

    @Test
    void invalidVaccineAdministrationFails() {
        VaccineAdministrationNurseStore vaStore = App.getInstance().getCompany().getVaccineAdministrationNurseStore();
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            vaStore.validate(new VaccineAdministrationNurse(0, null, null, null, "", 0, new Email(null)));
        });
    }

    @Test
    void ensureInvalidVaccineAdministrationIsNotSaved() {

        VaccineAdministrationNurseStore vaStore = App.getInstance().getCompany().getVaccineAdministrationNurseStore();
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            vaStore.save(new VaccineAdministrationNurse(0, null, null, null, "", 0, new Email(null)));
        });

    }

    @Test
    void ensureValidVaccineAdministrationIsSaved() throws ParseException {
        RegisterVaccineAdministrationController controller = new RegisterVaccineAdministrationController();

        VaccineAdministrationNurseStore vaStore = App.getInstance().getCompany().getVaccineAdministrationNurseStore();

        Company company = App.getInstance().getCompany();

        final String DATE_FORMAT = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setLenient(false);
        Date date = formatter.parse("01/01/2000");

        SNSUser user = new SNSUser("Joao", date,154789659, 915246589, "mail@mail.com", 15425638, "rua das flores", "Masculino");

        VaccineType vt1 = company.getVaccineTypeStore().getVcTypeList().get(0);

        long snsNumber = user.getSnsNumber();

        LocalDateTime localDateTime = LocalDateTime.now();

        Assertions.assertTrue(vaStore.save(new VaccineAdministrationNurse(snsNumber, vt1, vaccinationCenter, localDateTime, "12345-67", 1, new Email("nurse_2@lei.sem2.pt"))));

    }

}
