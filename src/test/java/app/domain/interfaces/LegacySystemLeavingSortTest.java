package app.domain.interfaces;

import app.domain.model.LegacySystemData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LegacySystemLeavingSortTest {

    LegacySystemLeavingSort leavingSortTest = new LegacySystemLeavingSort();
    int result;

    @Test
    void compare() {
    }

    @Test
    public void equalTest() {
        String leaving1 = "06/11/2022 15:00";
        String leaving2 = "06/11/2022 15:00";

        LegacySystemData legSys = new LegacySystemData("161593120", "Spikevax", 1, "21C12-02", "06/11/2022 15:00", "6/11/2022 16:00", "6/11/2022 17:00", leaving1);
        LegacySystemData legSys2 = new LegacySystemData("123456789", "Cominarty", 1, "21C16-02", "07/11/2022 15:00", "07/11/2022 16:00", "07/11/2022 17:00", leaving2);

        result = leavingSortTest.compare(legSys, legSys2);
        Assertions.assertEquals (0, result);
    }

    @Test
    public void highTest() {
        String leaving1 = "06/11/2022 16:00";
        String leaving2 = "06/11/2022 15:00";

        LegacySystemData legSys = new LegacySystemData("161593120", "Spikevax", 1, "21C12-02", "06/11/2022 15:00", "6/11/2022 16:00", "6/11/2022 17:00", leaving1);
        LegacySystemData legSys2 = new LegacySystemData("123456789", "Cominarty", 1, "21C16-02", "07/11/2022 15:00", "07/11/2022 16:00", "07/11/2022 17:00", leaving2);

        result = leavingSortTest.compare(legSys, legSys2);
        Assertions.assertEquals (1, result);
    }

    @Test
    public void lowTest() {
        String leaving1 = "06/11/2022 15:00";
        String leaving2 = "07/11/2022 15:00";

        LegacySystemData legSys = new LegacySystemData("161593120", "Spikevax", 1, "21C12-02", "06/11/2022 15:00", "6/11/2022 16:00", "6/11/2022 17:00", leaving1);
        LegacySystemData legSys2 = new LegacySystemData("123456789", "Cominarty", 1, "21C16-02", "07/11/2022 15:00", "07/11/2022 16:00", "07/11/2022 17:00", leaving2);

        result = leavingSortTest.compare(legSys, legSys2);
        Assertions.assertEquals (-1, result);
    }
}