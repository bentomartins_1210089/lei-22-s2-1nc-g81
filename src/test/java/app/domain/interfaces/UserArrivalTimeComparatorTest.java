package app.domain.interfaces;

import app.domain.model.SNSUserArrival;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UserArrivalTimeComparatorTest {

    public UserArrivalTimeComparator userArrivalTimeComparator = new UserArrivalTimeComparator();

    @Test
    public void testEqual() {

        SNSUserArrival snsUserArrival1 = new SNSUserArrival(1, "12:00", "29/05/2022");
        SNSUserArrival snsUserArrival2 = new SNSUserArrival(2, "12:00", "29/05/2022");
        int            result = userArrivalTimeComparator.compare(snsUserArrival1, snsUserArrival2);
        Assertions.assertEquals (0, result);
    }

    @Test
    public void testGreaterThan() {

        SNSUserArrival snsUserArrival1 = new SNSUserArrival(1, "12:00", "29/05/2022");
        SNSUserArrival snsUserArrival2 = new SNSUserArrival(2, "15:00", "28/05/2022");
        int            result = userArrivalTimeComparator.compare(snsUserArrival1, snsUserArrival2);
        Assertions.assertEquals (1, result);
    }

    @Test
    public void testLessThan() {

        SNSUserArrival snsUserArrival1 = new SNSUserArrival(2, "15:00", "28/05/2022");
        SNSUserArrival snsUserArrival2 = new SNSUserArrival(1, "12:00", "29/05/2022");
        int            result = userArrivalTimeComparator.compare(snsUserArrival1, snsUserArrival2);
        Assertions.assertEquals (-1, result);
    }
}