package app.controller;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashMap;


/**
 * Scheduler class.
 * @Tassio Gomes 1170065
 */


/**
 * This class write a csv file and generate a message dialog
 */

public class VaccinatedCsvController {

    public static void writeCSVFile(String filepath, int day, int month, int year, HashMap<String, Integer>  hashMap) {
        try {
            FileWriter fw = new FileWriter(filepath,true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            String date;
            if (day<10 && month<10) {
                date= "0"+day+"/0"+month+"/"+year+";";
            } else if(day<10){
                date ="0"+day+"/"+month+"/"+year+";";
            } else if (month<10) {
                date = day+"/0"+month+"/"+year+";";
            }else{
                date = day+"/"+month+"/"+year+";";
            }
            hashMap.forEach((centername,total) -> pw.print(date + centername + ";" + total +";\n"));
            pw.flush();
            pw.close();
            JOptionPane.showMessageDialog(null,"Records Saved");
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null,"Records not Saved");
        }
    }
}