package app.controller;

import app.domain.model.SNSUser;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineBase;
import app.domain.model.VaccineType;
import app.domain.store.VaccinationCenterStore;

import java.util.List;

/**
 * @author Tassio Gomes <1170065>
 * @author Mário Borja <1200586>
 */
public class VaccinationBaseController {
    private VaccinationCenterStore vaccinationCenterStore = new VaccinationCenterStore();

    /**
     * Gets all users.
     *
     * @return the all users
     */
    public List<SNSUser> getAllUsers()
    {
        return App.getInstance().getCompany().getSNSUserStore().getSnsUserList();
    }

    /**
     * Select client sns user.
     *
     * @param userNr the user nr
     * @return the sns user
     */
    public SNSUser selectClient(long userNr)
    {
        return vaccinationCenterStore.selectUser(userNr, getAllUsers());
    }

    /**
     * Show client string.
     *
     * @param snsUser the sns user
     * @return the string
     */
    public String showClient (SNSUser snsUser){
        return vaccinationCenterStore.showClient(snsUser);
    }

    /**
     * Create vaccine vaccine base.
     *
     * @param snsUser           the sns user
     * @param vaccineType       the vaccine type
     * @param vaccinationCenter the vaccination center
     * @return the vaccine base
     */
    public VaccineBase createVaccine(SNSUser snsUser, VaccineType vaccineType, VaccinationCenter vaccinationCenter)
    {
        return vaccinationCenterStore.createVaccine(snsUser, vaccineType, vaccinationCenter);
    }

    /**
     * Gets vac centers.
     *
     * @return the vac centers
     */
    public List<VaccinationCenter> getVacCenters() {
        return App.getInstance().getCompany().getVaccinationCenterStore().getAllVacCenters();
    }
}
