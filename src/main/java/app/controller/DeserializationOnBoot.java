package app.controller;

import app.domain.model.*;
import app.domain.shared.Constants;
import app.domain.store.*;
import app.files.*;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Deserialization on boot.
 * @author Bento Martins <1210089>
 */
public class DeserializationOnBoot {

    /**
     * Instantiates a new Deserialization on boot.
     */
    public DeserializationOnBoot() {
    }

    /**
     * Deserialization on boot sns users.
     *
     * @param store      the store
     * @param authFacade the auth facade
     */
    public void DeserializationOnBootSNSUsers(SNSUserStore store, AuthFacade authFacade) {

        SNSUserFile snsUserFile = new SNSUserFile();
        ArrayList<SNSUser> snsUsers = snsUserFile.load();

        for (SNSUser snsUser : snsUsers) {

            store.addSNSUserToStore(snsUser);

            authFacade.addUserWithRole(snsUser.getName(),snsUser.getEmail(),snsUser.getPassword(), Constants.ROLE_USER);

        }

    }

    /**
     * Deserialization on boot employees.
     *
     * @param store      the store
     * @param authFacade the auth facade
     */
    public void DeserializationOnBootEmployees(EmployeeStore store, AuthFacade authFacade) {

        EmployeeFile employeeFile = new EmployeeFile();
        List<Employee> employeeList = employeeFile.load();

        for (Employee employee : employeeList) {

            store.addEmployee(employee);

            authFacade.addUserWithRole(employee.getName(),employee.getEmail(),"123456", employee.getRole());

        }

    }

    /**
     * Deserialization on boot vaccine type.
     *
     * @param store the store
     */
    public void DeserializationOnBootVaccineType(VaccineTypeStore store) {

        VaccineTypeFile file = new VaccineTypeFile();
        ArrayList<VaccineType> list = file.load();

        for (VaccineType vcType : list) {
            store.addVcType(vcType);
        }
    }

    /**
     * Deserialization on boot vaccine administration.
     *
     * @param store the store
     */
    public void DeserializationOnBootVaccineAdministration(VaccineAdministrationStore store) {

        VaccineAdministrationFile file = new VaccineAdministrationFile();
        ArrayList<VaccineAdministration> list = file.load();

        for (VaccineAdministration adminProcess : list) {
            store.addAdminProcess(adminProcess);
        }
    }

    /**
     * Deserialization on boot register sns user arrival.
     *
     * @param store the store
     */
    public void DeserializationOnBootRegisterSNSUserArrival(WaitingRoomStore store) {

        SNSUserArrivalFile file = new SNSUserArrivalFile();
        ArrayList<SNSUserArrival> list = file.load();

        for (SNSUserArrival userArrival : list) {
            store.addWaitingUser(userArrival);
        }
    }

    /**
     * Deserialization on boot vaccination center.
     *
     * @param store the store
     */
    public void DeserializationOnBootVaccinationCenter(VaccinationCenterStore store) {
        // Deserialization of the Vaccination Center
        VaccinationCenterSerialization vaccinationCenterSerialization = new VaccinationCenterSerialization();
        List<VaccinationCenter> vaccinationCenterList = vaccinationCenterSerialization.load();
        for (VaccinationCenter vaccinationCenter : vaccinationCenterList) {
            store.add(vaccinationCenter);
        }
    }

    public void DeserializationOnBootVaccineBase(VaccinationCenterStore store) {
        // Deserialization of Vaccine Base
        VaccineBaseFile vaccineBaseFile = new VaccineBaseFile();
        List<VaccineBase> vaccineBaseList = vaccineBaseFile.load();
        for (VaccineBase vaccineBase : vaccineBaseList) {
            store.addVaccine(vaccineBase);
        }
    }

    public void DeserializationOnBootVaccineAdministrationNurse(VaccineAdministrationNurseStore store) {
        // Deserialization of Vaccine Administration Nurse
        VaccineAdministrationNurseFile vaccineAdministrationNurseFile = new VaccineAdministrationNurseFile();
        List<VaccineAdministrationNurse> vaccineAdministrationNurseList = vaccineAdministrationNurseFile.load();
        for (VaccineAdministrationNurse vaccineAdministrationNurse : vaccineAdministrationNurseList) {
            store.add(vaccineAdministrationNurse);
        }
    }
}
