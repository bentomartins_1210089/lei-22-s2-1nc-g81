package app.controller;

import app.domain.model.*;
import app.domain.store.VaccinationCenterStore;
import app.domain.store.WaitingRoomStore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

/**
 * @author Tiago Oliveira <1211669>
 */
public class SNSUserArrivalController {

    private Company company;

    /**
     * Waiting room store
     */
    private WaitingRoomStore wrStore;

    /**
     * Sns user arrival model
     */
    private SNSUserArrival snsUserArrival;

    private VaccinationCenterStore vcCenterStore;
    /**
     * Vaccination center
     */
    public static VaccinationCenter vaccinationCenter;

    /**
     * Empty contructor instantiates an sns user arrival controller
     */
    public SNSUserArrivalController() {
        this.company = App.getInstance().getCompany();
        this.wrStore = company.getWaitingRoomStore();
        this.vcCenterStore = company.getVaccinationCenterStore();
    }

    /**
     * Contructor instantiates an sns user arrival controller
     * @param wrStore Waiting room store
     */
    public SNSUserArrivalController(WaitingRoomStore wrStore) {
        this.company = App.getInstance().getCompany();
        this.wrStore = wrStore;
        this.vcCenterStore = company.getVaccinationCenterStore();
    }

    /**
     * Creates a new model for the arrival of a user to be vaccinated
     * @param snsNumber SNS user number
     * @param time      User arrival time
     * @param date      User arrival date
     * @return user arrival model
     */
    public SNSUserArrival createUserArrival(long snsNumber, String time, String date) {
        return snsUserArrival = wrStore.createUserArrival(snsNumber, time, date);
    }

    /**
     * Validates a duplicate in store
     * @param user Arrived SNS user
     * @return true if there are no duplicates, false otherwise
     */
    public boolean validateDuplicateUserArrival(SNSUserArrival user) {
        return this.wrStore.validateDuplicateUserArrival(user);
    }

    /**
     * Validates an user scheduled vaccine for that day and vaccination center
     *
     * @param snsNumber SNS user number
     * @param date User arrival date
     * @return true if has scheduled, false otherwise
     */
    public boolean validateScheduledVaccination(long snsNumber, String date) throws ParseException {
        List<VaccineBase> vcSchedules = vcCenterStore.getAllVacs();

        for (VaccineBase vaccineBase : vcSchedules) {
            if (vaccineBase.getVaccinationCenter().getEmail().equals(getVaccinationCenter().getEmail()))
                if (vaccineBase.getSnsUser().getSnsNumber() == snsNumber) {
                    String dateAndTimeReg = vaccineBase.getDateAndTimeRegistration();
                    String[] dateTimeParts = dateAndTimeReg.split( " ");

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date newDate = formatter.parse(date);


                    if (dateTimeParts[0].equals(new SimpleDateFormat("dd/MM/yyyy").format(newDate)))
                        return true;
                }
        }
        return false;
    }

    public VaccinationCenter getVaccinationCenter() {
        return vaccinationCenter;
    }

    public void setVaccinationCenter(VaccinationCenter vaccinationCenter) {
        this.vaccinationCenter = vaccinationCenter;
    }
}
