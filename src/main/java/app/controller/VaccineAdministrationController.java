package app.controller;

import app.domain.model.VaccineAdministration;
import app.domain.store.VaccineAdministrationStore;

import java.util.ArrayList;

/**
 * @author Tiago Oliveira <1211669>
 */

public class    VaccineAdministrationController {

    /**
     * Admin process store store
     */
    private VaccineAdministrationStore store;

    public static String vaccine;

    /**
     * Contructor instantiates an admin process controller
     */
    public VaccineAdministrationController() {
        this.store = App.getInstance().getCompany().getAdminProcessStore();
    }

    /**
     * Contructor instantiates an admin process controller
     * @param store store
     */
    public VaccineAdministrationController(VaccineAdministrationStore store) {
        this.store = store;
    }

    /**
     * Creates an administration process
     *
     * @param vaccine           vaccine
     * @param ageGroup          age group
     * @param numDoses          number of doses
     * @param dosages           dosages (mL)
     * @param dosingIntervals   recovery intervals for each dose (days)
     * @return model
     */
    public VaccineAdministration createVaccineAdmin (String vaccine, String ageGroup, int numDoses, ArrayList<Double> dosages, ArrayList<Integer> dosingIntervals) {
        VaccineAdministration vaccineAdmin = store.createVaccineAdmin(vaccine, ageGroup, numDoses, dosages, dosingIntervals);
        return vaccineAdmin;
    }

    /**
     * Validates a duplicate in store
     * @param vaccineAdmin administration process
     * @return true if there are no duplicates, false otherwise
     */
    public boolean validateVaccineAdmin(VaccineAdministration vaccineAdmin) {
        return this.store.validateVaccineAdmin(vaccineAdmin);
    }
}
