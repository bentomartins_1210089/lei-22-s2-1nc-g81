package app.controller;

import app.domain.model.*;
import app.domain.store.EmployeeStore;

/**
 * The type Register employee controller.
 */
public class EmployeeRegisterController {
    /**
     * Employee store employee store.
     */
    private final EmployeeStore employeeStore;

    /**
     * Instantiates an employee controller.
     */
    public EmployeeRegisterController() {
        this.employeeStore = App.getInstance().getCompany().getEmployeeStore();
    }

    /**
     * Instantiates an employee controller.
     *
     * @param employeeStore employee store
     */
    public EmployeeRegisterController(EmployeeStore employeeStore) {
        this.employeeStore = employeeStore;
    }

    /*public List<Employee> getAllEmployees() {
        List <Employee> list = new ArrayList<>();

    }*/


    /**
     * Register employee.
     *
     * @param phoneNumber      phone number
     * @param address          address
     * @param organizationRole organization role
     * @param name             name
     * @param cc               cc number
     * @param email            email
     * @return employee
     */
    public Employee registerEmployee(String phoneNumber, String address, String organizationRole, String name, String cc, String email) {
        //return EmployeeStore.registerEmployee(phoneNumber,address,organizationRole,name, cc, email);
        return this
                .employeeStore
                .createEmployee(name, address, phoneNumber, email, cc, organizationRole);
    }

    /**
     * Validate employee boolean.
     *
     * @param employee employee
     * @return boolean
     */
    public boolean validateEmployee(Employee employee) {
        return this.employeeStore.validateEmployee(employee);
    }

    /**
     * Validate employee boolean.
     *
     * @return boolean
     */
    public Employee createEmployee(String name, String address, String phoneNumber, String email, String cc, String role) {
        return employeeStore.createEmployee(name, address, phoneNumber, email, cc, role);
    }

    /**
     * Name valid boolean.
     *
     * @param name name
     * @return boolean
     * @throws IllegalArgumentException illegal argument exception
     */
    public boolean nameValid(String name) throws IllegalArgumentException {
        return Employee.checkName(name);
    }

    /**
     * Phone number valid boolean.
     *
     * @param phoneNumber phone number
     * @return boolean
     * @throws IllegalArgumentException illegal argument exception
     */
    public boolean phoneNumberValid(String phoneNumber) throws IllegalArgumentException {
        return Employee.checkPhoneNumber(phoneNumber);
    }

    /**
     * Email valid boolean.
     *
     * @param cc cc
     * @return boolean
     * @throws IllegalArgumentException illegal argument exception
     */
    public boolean ccValid(String cc) throws IllegalArgumentException {
        return Employee.checkCc(cc);
    }

    /**
     * Phone number valid boolean.
     *
     * @param email phone number
     * @return boolean
     * @throws IllegalArgumentException illegal argument exception
     */
    public boolean emailValid(String email) throws IllegalArgumentException {
        return Employee.checkEmail(email);
    }


    /**
     * Address valid boolean.
     *
     * @param address address
     * @return boolean
     * @throws IllegalArgumentException illegal argument exception
     */
    public boolean addressValid(String address) throws IllegalArgumentException {
        return Employee.checkAddress(address);
    }

    /**
     * Organization role valid boolean.
     *
     * @param organizationRole organization role
     * @return boolean
     * @throws IllegalArgumentException illegal argument exception
     */
    public boolean organizationRoleValid(String organizationRole) throws IllegalArgumentException {
        return Employee.checkOrganizationRole(organizationRole);
    }

}