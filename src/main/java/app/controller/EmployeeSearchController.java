package app.controller;

import app.domain.store.EmployeeStore;

import java.util.List;

import app.domain.model.Employee;

/**
 * The Search Employees Controller.
 *
 * @author Tassio Gomes <1170065>
 */


public class EmployeeSearchController {

    private EmployeeStore employeeStore;

    /**
     * Instantiates Employees Controller.
     */

    public EmployeeSearchController()
    {
        this.employeeStore = App.getInstance().getCompany().getEmployeeStore();


    }

    /**
     * list Employee By Role.
     */


    public List<Employee> getListEmployeesByRole(String role){

        return this.employeeStore.getEmployeeListByRole(role);
    }

    /**
     * Employee List.
     */

    public List<Employee> getListEmployees(){

        return this.employeeStore.getEmployeeList();
    }

    public Employee getEmployeeByEmail(String email){
        return this.employeeStore.getEmployeeByEmail(email);
    }

}