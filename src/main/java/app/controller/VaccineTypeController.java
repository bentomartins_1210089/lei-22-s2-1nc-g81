package app.controller;

import app.domain.model.VaccineType;
import app.domain.store.VaccineTypeStore;

/**
 * @author Tiago Oliveira <1211669>
 */
public class VaccineTypeController {

    private VaccineTypeStore store;

    public VaccineTypeController() {
        this.store = App.getInstance().getCompany().getVaccineTypeStore();
    }

    public VaccineTypeController(VaccineTypeStore store) {
        this.store = store;
    }

    public VaccineType createVaccineType(String vaccineTypeCode, String vaccineTypeDescription, String vaccineTypeTechnology, String vcName, String vcBrand) {

        VaccineType vcType = new VaccineType(vaccineTypeCode, vaccineTypeDescription, vaccineTypeTechnology, vcName, vcBrand);

        return vcType;
    }

    public VaccineType createVaccineType(String vaccineTypeCode, String vaccineTypeTechnology, String vcName, String vcBrand) {

        VaccineType vcType = new VaccineType(vaccineTypeCode, vaccineTypeTechnology, vcName, vcBrand);

        return vcType;
    }

    public boolean validateVcType(VaccineType vcType){
        return store.validateVcType(vcType);
    }

    /**
     * @author Tiago Oliveira <1211669>
     *
     * Get Vaccine Type by Name
     * @param vcName vaccine name
     * @return  vaccine type
     */
    public VaccineType getVaccineByName(String vcName) {
        for (VaccineType vcType: store.getVcTypeList()) {
            if (vcType.getVaccineName().equals(vcName))
                return vcType;
        }
        return null;
    }
}
