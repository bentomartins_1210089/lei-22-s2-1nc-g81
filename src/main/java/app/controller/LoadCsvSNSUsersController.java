package app.controller;
import app.domain.model.Company;
import app.domain.model.PasswordGenerator;
import app.domain.model.SNSUser;
import app.dto.SNSUserDto;
import app.domain.store.SNSUserStore;
import app.adapter.LoadCsvSNSUsersAdapter;
import app.files.SNSUserFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The Load CSV with SNS Users Controller.
 *
 * @author Bento Martins <1210089>
 */
public class LoadCsvSNSUsersController {

    //    .\src\test\data\SNSUserDataFromGaoFuNationalCenterDoPortoVaccinationCenter.csv
    //    .\src\test\data\randomusers_withoutheader.csv
    //    Augusto Rodrigues | 953231379@gmail.com | 9GFW4Wv


    private App app;

    /**
     * Instantiates a new Load csv sns users controller.
     */
    public LoadCsvSNSUsersController() {
        this.app = App.getInstance();
    }


    /**
     * Check if CSV file exists
     *
     * @param path the path to the CSV file
     * @return the boolean
     */
    public boolean CsvFileExists(String path) {

        try {
            Scanner sc = new Scanner(new File(path));
            return true;
        } catch (FileNotFoundException e) {
            System.out.println("File not found. Please input a correct path.\nNew path:");
            return false;
        }
    }

    /**
     * Read CSV file.
     *
     * @param path the path to the CSV file
     * @return the list of SNS Users DTO
     * @throws FileNotFoundException the file not found exception
     */
    public List<SNSUserDto> readCsv(String path) throws FileNotFoundException {

        LoadCsvSNSUsersAdapter adapter = new LoadCsvSNSUsersAdapter();

        List<SNSUserDto> listDto = adapter.GetSNSUsersDtoFromCsv(path);

        List<SNSUserDto> listDtoFinal = null;

        if (listDto==null) {
            return listDtoFinal=null;
        }

        listDtoFinal = GetSNSUsersNotInStore(listDto);

        return listDtoFinal;
    }

    /**
     * Check SNS Users on Store.
     *
     * @param listDto the list of SNS Users DTO
     * @return the list of SNS Users DTO
     */
    public List<SNSUserDto> GetSNSUsersNotInStore(List<SNSUserDto> listDto) {

        SNSUserStore store= app.getCompany().getSNSUserStore();

        List<SNSUserDto> listDtoFinal = new ArrayList<>();

        for (SNSUserDto snsUserDto : listDto) {

            SNSUser snsuser = new SNSUser(snsUserDto.getName(),snsUserDto.getBirthDate(),snsUserDto.getSnsNumber(),
                    snsUserDto.getPhone(),snsUserDto.getEmail(),snsUserDto.getCcNumber(),snsUserDto.getUserAddress(),
                    snsUserDto.getSexUser());

            boolean existsUserStore = store.existsSNSUserStore(snsuser);
            boolean existsUserAuth = app.getCompany().existsUser(snsuser);

            if (existsUserStore && !(existsUserAuth)) {
                listDtoFinal.add(snsUserDto);
            }

            else  {
                System.out.println("SNS User "+snsUserDto.getName()+" with e-mail "+snsUserDto.getEmail()+ " already exists and won't be loaded.");
            }
        }

        return listDtoFinal;
    }

    /**
     * Add SNS Users to Store
     *
     * @param listDtoFinal the list of SNS Users DTO
     * @return the list
     */
    public List<SNSUserDto> addSNSUsersDtoToStore(List<SNSUserDto> listDtoFinal) {

        List<SNSUserDto> listDtoAdded = new ArrayList<>();

        SNSUserStore store= app.getCompany().getSNSUserStore();

        for (SNSUserDto snsUserDto : listDtoFinal) {

            SNSUser snsuser = new SNSUser(snsUserDto.getName(),snsUserDto.getBirthDate(),snsUserDto.getSnsNumber(),snsUserDto.getPhone(),snsUserDto.getEmail(),snsUserDto.getCcNumber(),snsUserDto.getUserAddress(),snsUserDto.getSexUser());

            PasswordGenerator passGen = new PasswordGenerator();
            String passToUser = passGen.generatePassword();

            snsuser.setPassword(passToUser);

            store.addSNSUserToStore(snsuser);

            app.getCompany().createAuthUser(snsuser);

            SNSUserDto snsUserAddedDto = new SNSUserDto(snsUserDto.getName(),snsUserDto.getEmail(),passToUser);

            listDtoAdded.add(snsUserAddedDto);
        }

        return listDtoAdded;

    }

}

