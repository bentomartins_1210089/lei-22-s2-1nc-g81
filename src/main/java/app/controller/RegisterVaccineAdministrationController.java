package app.controller;

import app.domain.model.*;
import app.domain.store.VaccinationCenterStore;
import app.domain.store.VaccineAdministrationNurseStore;
import app.domain.store.WaitingRoomStore;
import app.dto.SNSUserArrivalDTO;
import app.dto.VaccineAdministrationNurseDTO;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Timer;

/**
 * Register vaccine administration controller.
 * @Mario Borja 1200586
 */
public class RegisterVaccineAdministrationController {
    private Company company;
    private VaccineAdministrationNurseStore vacNurseStore;
    private VaccineAdministrationNurse vaccineAdministrationNurse;
    private WaitingRoomStore wrStore;
    private SNSUserArrival userArrival;
    private VaccinationCenterStore vStore;
    private String age;
    private Integer dosesTaken = 0;
    private VaccineBase vaccine;
    private VaccineType vaccineType;
    private Integer doseNumber;
    private String lotNumber;
    private Email email;
    private SNSUser snSuser;

    private static Timer timer;

    /**
     * Instantiates a new vaccine administration controller.
     */
    public RegisterVaccineAdministrationController(){
        this.company = App.getInstance().getCompany();
        this.vacNurseStore = company.getVaccineAdministrationNurseStore();
        this.wrStore = company.getWaitingRoomStore();
        this.vStore = company.getVaccinationCenterStore();
    }

    /**
     * Register vaccine nurse administration vaccine administration nurse.
     *
     * @param SNSnumber         the sn snumber
     * @param vaccineType       the vaccine type
     * @param vaccinationCenter the vaccination center
     * @param lotNumber         the lot number
     * @param doseNumber        the dose number
     * @return the vaccine administration nurse
     */
    public VaccineAdministrationNurse registerVaccineNurseAdministration(long SNSnumber, VaccineType vaccineType, VaccinationCenter vaccinationCenter, String lotNumber, Integer doseNumber){
        Email email = App.getInstance().getCurrentUserSession().getUserId();
        LocalDateTime ldtime = LocalDateTime.now();
        vaccineAdministrationNurse = vacNurseStore.create(new VaccineAdministrationNurseDTO(SNSnumber, vaccineType, vaccinationCenter, ldtime, lotNumber, doseNumber, email));
        return vaccineAdministrationNurse;
    }

    /**
     * Get the sns number from User Arrival
     *
     * @param user SNSUserArrival
     * @return the user number
     */
    public long getSnsNumberArrival(SNSUserArrival user){
        return user.getSnsNumber();
    }

    /**
     * Save vaccine
     */
    public void saveVaccine() {
        vacNurseStore.save(vaccineAdministrationNurse);
    }

    /**
     * Gets vaccine administration  class by sns number and vaccine.
     *
     * @param SNSnumber   the user snsNumber
     * @param vaccineType the vaccine type
     * @return the vaccine administration nurse by sns number and vaccine
     */
    public VaccineAdministrationNurse getVaccineAdministrationNurseBySnsNumberAndVaccine(long SNSnumber, VaccineType vaccineType) {
        return vacNurseStore.getVaccineAdministrationBySnsNumber(SNSnumber, vaccineType);
    }

    /**
     * Get name by user sns number.
     *
     * @param snsNumber the sns number
     * @return the string
     */
    public String getNameBySnsNumber(long snsNumber){
        return company.getSNSUserStore().getNameBySnsNumber(snsNumber);
    }

    /**
     * Return vaccines associated to each user.
     *
     * @param snsNumber the sns number
     * @return the list
     */
    public List<VaccineBase> returnVaccinesFromUser(long snsNumber) {
        List<VaccineBase> vaccineList = new ArrayList<>();
        SNSUserArrivalController ctrl = new SNSUserArrivalController();
        List<VaccineBase> vcSchedules = company.getVaccinationCenterStore().getAllVacs();

        for (VaccineBase vaccineBase : vcSchedules) {
            if (vaccineBase.getVaccinationCenter().equals(ctrl.getVaccinationCenter()))
                if (vaccineBase.getSnsUser().getSnsNumber() == snsNumber) {
                    vaccineList.add(vaccineBase);
                }
        }
        return vaccineList;
    }

    /**
     * Send sms to user with time to leave.
     */
    public void sendSmsToUserWithTimeToLeave () {
        FileWriter writer = null;
        try {
            writer = new FileWriter("SMS.txt", StandardCharsets.UTF_8, true);
            BufferedWriter bw = new BufferedWriter(writer);
            PrintWriter print = new PrintWriter(bw);
            String sms = LocalDateTime.now().getDayOfMonth() + "/" + LocalDateTime.now().getMonthValue() +
                    "/" + LocalDateTime.now().getYear() + "-" + LocalDateTime.now().getHour() + " " + LocalDateTime.now().getMinute() +
                    "Feel free to leave the center";
            print.println(sms);
            print.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
