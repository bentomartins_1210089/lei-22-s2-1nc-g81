package app.controller;

import app.domain.interfaces.UserArrivalTimeComparator;
import app.domain.model.Company;
import app.domain.model.Employee;
import app.domain.model.SNSUserArrival;
import app.domain.model.VaccinationCenter;
import app.domain.shared.Enums.EmployeeRole;
import app.domain.store.EmployeeStore;
import app.domain.store.VaccinationCenterStore;
import app.domain.store.WaitingRoomStore;
import app.dto.SNSUserArrivalDTO;
import app.mappers.OrderedSNSUserDTO;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.Serializable;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The type Vacination center controller.
 */
public class VacinationCenterController implements Serializable {

    private Company company;
    private VaccinationCenterStore vaccinationCenterStore;
    private VaccinationCenter vaccinationCenter;
    private EmployeeStore    employeeStore;
    private WaitingRoomStore waitingRoomStore;

    /**
     * Instantiates a new Vacination center controller.
     */
    public VacinationCenterController()
    {
        // redo soaressf
//        if(!App.getInstance().getCurrentUserSession().isLoggedInWithRole(Constants.ROLE_ADMIN))
//            throw new IllegalStateException("Not authorized user");
        this.company = App.getInstance().getCompany();
        this.vaccinationCenterStore = company.getVaccinationCenterStore();
        this.employeeStore = company.getEmployeeStore();
        this.waitingRoomStore = company.getWaitingRoomStore();
    }

    /**
     * New vaccination center boolean.
     *
     * @param name                 the name
     * @param address              the address
     * @param phone                the phone
     * @param fax                  the fax
     * @param email                the email
     * @param website              the website
     * @param openingHours         the opening hours
     * @param closingHours         the closing hours
     * @param slot                 the slot
     * @param strMaxNumberVaccines the str max number vaccines
     * @param coordinator          the coordinator
     * @return the boolean
     */
    public boolean newVaccinationCenter(String name, String address, String phone, String fax, String email,
                                        String website, String openingHours, String closingHours, String slot,
                                        String strMaxNumberVaccines, String coordinator)
    {
        try
        {
            int maxNumberVaccines = Integer.valueOf(strMaxNumberVaccines);
            Email newEmail = new Email(email);
            //waitingRoomStore.

            Employee newCoordinator = null;
            List<Employee> employeeList = this.getCoordinatorsList();
            if(!employeeList.isEmpty()){
                Iterator var2 = employeeList.iterator();
                do {
                    newCoordinator = (Employee) var2.next();
                } while(!newCoordinator.getEmail().equals(coordinator));

                if(newCoordinator != null) {
                    this.vaccinationCenter = this.vaccinationCenterStore.newVaccinationCenter(name, address, phone, fax, newEmail,
                                                                                              website, openingHours, closingHours, slot, maxNumberVaccines, newCoordinator);
                    return true;
                }
            }
            return false;
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.vaccinationCenter = null;
            return false;
        }
    }

    /**
     * Save vaccination center boolean.
     *
     * @return the boolean
     */
    public Boolean saveVaccinationCenter()
    {
        return this.vaccinationCenterStore.add(this.vaccinationCenter);
    }

    /**
     * Gets vaccination center to string.
     *
     * @return the vaccination center to string
     */
    public String getVaccinationCenterToString()
    {
        return this.vaccinationCenter.toString();
    }

    /**
     * Gets by id.
     *
     * @param email the email
     * @return the by id
     */
    public Optional<VaccinationCenter> getById(String email) {
        return this.getById(new Email(email));
    }

    public List<VaccinationCenter> getVaccinationCenterList() {
        return vaccinationCenterStore.getAllVacCenters();
    }

    /**
     * Gets by id.
     *
     * @param email the email
     * @return the by id
     */
    public Optional<VaccinationCenter> getById(Email email) {
        return this.vaccinationCenterStore.getById(email);
    }

    /**
     * Vaccination center list set.
     *
     * @return the set
     */
    public Set<VaccinationCenter> vaccinationCenterList(){
        return this.vaccinationCenterStore.getAll();
    }

    /**
     * Gets coordinators list.
     *
     * @return the coordinators list
     */
    public List<Employee> getCoordinatorsList()
    {
        return employeeStore.getEmployeeListByRole(EmployeeRole.ROLE_CENTER_COORDINATOR.value());

    }

    /**
     * Add coordinator to vaccination center boolean.
     *
     * @param vaccinationCenterEmail the vaccination center email
     * @param coordinator            the coordinator
     * @return the boolean
     */
    public boolean addCoordinatorToVaccinationCenter (String vaccinationCenterEmail, Employee coordinator){

        Optional<VaccinationCenter> vaccinationCenterOPT = this.getById(vaccinationCenterEmail);

        if(vaccinationCenterOPT.isPresent()) {
            VaccinationCenter vaccinationCenter = vaccinationCenterOPT.get();
            return vaccinationCenter.setCoordinator(coordinator);
        }

        return false;
    }

    /**
     * Add coordinator to vaccination center boolean.
     *
     * @param vaccinationCenterEmail the vaccination center email
     * @param coordinator            the coordinator
     * @return the boolean
     */
    public boolean addCoordinatorToVaccinationCenter (Email vaccinationCenterEmail, Employee coordinator){

        Optional<VaccinationCenter> vaccinationCenterOPT = this.getById(vaccinationCenterEmail);

        if(vaccinationCenterOPT.isPresent()) {
            VaccinationCenter vaccinationCenter = vaccinationCenterOPT.get();
            return vaccinationCenter.setCoordinator(coordinator);
        }

        return false;
    }

    /**
     * Add waiting room to vaccination center boolean.
     *
     * @param vaccinationCenterEmail the vaccination center email
     * @return the boolean
     */
    public boolean addWaitingRoomToVaccinationCenter (Email vaccinationCenterEmail){

        Optional<VaccinationCenter> vaccinationCenterOPT = this.getById(vaccinationCenterEmail);

        if(vaccinationCenterOPT.isPresent()) {
            VaccinationCenter vaccinationCenter = vaccinationCenterOPT.get();
            return vaccinationCenter.setWaitingRoom(waitingRoomStore);
        }

        return false;
    }

    /**
     * Add sns user arrival to waiting room boolean.
     *
     * @param vaccinationCenterEmail the vaccination center email
     * @param snsUserArrival         the sns user arrival
     * @return the boolean
     */
    public boolean addSNSUserArrivalToWaitingRoom (Email vaccinationCenterEmail, SNSUserArrival snsUserArrival){

        Optional<VaccinationCenter> vaccinationCenterOPT = this.getById(vaccinationCenterEmail);

        if(vaccinationCenterOPT.isPresent()) {
            VaccinationCenter vaccinationCenter = vaccinationCenterOPT.get();
            return vaccinationCenter.setNewSNSUserArrival(snsUserArrival);
        }

        return false;
    }

    /**
     * Users in the waiting room list.
     *
     * @param vaccinationCenterEmail the vaccination center email
     * @return the list
     */
    public List<SNSUserArrival> getUsersInTheWaitingRoom (Email vaccinationCenterEmail) {
        Optional<VaccinationCenter> vaccinationCenterOPT = this.getById(vaccinationCenterEmail);
        List<SNSUserArrival> snsUserArrivalList;

        if(vaccinationCenterOPT.isPresent()) {
            VaccinationCenter vaccinationCenter = vaccinationCenterOPT.get();
            snsUserArrivalList = vaccinationCenter.getWaitingRoom().getWaitingRoomList();

            if(!snsUserArrivalList.isEmpty()) {

                return snsUserArrivalList;
            }
        }

        return null;
    }

    /**
     * Consult users in the waiting room list.
     *
     * @param vaccinationCenter the vaccination center
     * @return the list of waiting SNS users
     */
    public List<SNSUserArrivalDTO> consultUsersInTheWaitingRoom (VaccinationCenter vaccinationCenter) {
        List<SNSUserArrival> snsUserArrivalList = vaccinationCenter.getWaitingRoom().getWaitingRoomList();
        Collections.sort(snsUserArrivalList, new UserArrivalTimeComparator());

        OrderedSNSUserDTO orderedSNSUserDTO = new OrderedSNSUserDTO();
        List<SNSUserArrivalDTO> orderedSNSUserDTOList = orderedSNSUserDTO.toDTO(snsUserArrivalList);

        return orderedSNSUserDTOList;
    }
}
