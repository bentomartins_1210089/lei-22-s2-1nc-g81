package app.controller;

import app.domain.model.*;
import app.domain.store.*;
import app.files.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static app.ui.gui.ccoordinator_menu.ImportLegacySystemController.LEGACY_SYSTEM_NAME;

/**
 * The type Serialization on close.
 * @author Bento Martins <1210089>
 */
public class SerializationOnClose {

    private App app;

    /**
     * Instantiates a new Serialization on close.
     */
    public SerializationOnClose() {
        this.app = App.getInstance();

        // Serialization Legacy System
        LegacySystemDataController legSysCtrl = new LegacySystemDataController();
        legSysCtrl.saveLegacySystemData(LEGACY_SYSTEM_NAME);

        // Serialize SNSUser
        SNSUserStore storeSNSUser = app.getCompany().getSNSUserStore();

        ArrayList<SNSUser> snsUsersList = storeSNSUser.getSnsUserList();

        SNSUserFile snsUserFile = new SNSUserFile();
        snsUserFile.serialize(snsUsersList);

        // Serialize Employees
        EmployeeStore storeEmployee = app.getCompany().getEmployeeStore();

        List<Employee> employeeList = storeEmployee.getEmployeeList();

        EmployeeFile employeeFile = new EmployeeFile();
        employeeFile.serialize(employeeList);


        // Serialization Vaccine Type
        VaccineTypeStore vcTypeStr = app.getCompany().getVaccineTypeStore();
        ArrayList<VaccineType> vcTypeList = vcTypeStr.getVcTypeList();
        VaccineTypeFile vcTypeFile = new VaccineTypeFile();

        vcTypeFile.serialize(vcTypeList);

        // Serialization Vaccine Administration Process
        VaccineAdministrationStore vcAdminStore = app.getCompany().getAdminProcessStore();
        ArrayList<VaccineAdministration> vcAdminList = vcAdminStore.getAdminProcessList();
        VaccineAdministrationFile file = new VaccineAdministrationFile();

        file.serialize(vcAdminList);

        // Serialization SNS User Arrival Process
        WaitingRoomStore wrStore = app.getCompany().getWaitingRoomStore();
        ArrayList<SNSUserArrival> wrList = wrStore.getWaitingRoomList();
        SNSUserArrivalFile snsUserArivalFile = new SNSUserArrivalFile();

        snsUserArivalFile.serialize(wrList);

        // Serialization of the Vaccination Center
        VaccinationCenterStore vcStore = app.getCompany().getVaccinationCenterStore();
        List<VaccinationCenter> vaccinationCenterList = vcStore.getAllVacCenters();
        VaccinationCenterSerialization vaccinationCenterSerialization = new VaccinationCenterSerialization();
        vaccinationCenterSerialization.serialize(vaccinationCenterList);

        // Serialization Vaccine Base
        List<VaccineBase> vaccineBaseList = vcStore.getAllVacs();
        VaccineBaseFile vaccineBaseFile = new VaccineBaseFile();
        vaccineBaseFile.serialize(vaccineBaseList);

        // Serialization Vaccine Administration Nurse
        VaccineAdministrationNurseStore vaccineNurseStore = app.getCompany().getVaccineAdministrationNurseStore();
        List<VaccineAdministrationNurse> vaccineAdministrationNurseList = vaccineNurseStore.getVaccineAdministrationList();
        VaccineAdministrationNurseFile vaccineAdministrationNurseFile = new VaccineAdministrationNurseFile();
        vaccineAdministrationNurseFile.serialize(vaccineAdministrationNurseList);
    }
}
