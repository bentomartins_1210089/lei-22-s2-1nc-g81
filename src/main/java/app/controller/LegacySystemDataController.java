package app.controller;

import app.domain.interfaces.LegacySystemArrivalSort;
import app.domain.interfaces.LegacySystemLeavingSort;
import app.domain.model.*;
import app.domain.store.*;
import app.dto.LegacySystemDataDTO;
import app.exception.InvalidLegacySystemException;
import app.exception.InvalidSnsNumberException;
import app.files.FileLegacySystem;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Tiago Oliveira <1211669>
 */

public class LegacySystemDataController {

    private Company company;

    /**
     * Legacy System Store
     */
    private LegacySystemStore str;

    /**
     * Sns user store
     */
    private SNSUserStore snsUserStr;

    /**
     * Vaccine Type controller
     */
    private VaccineTypeController vcTypeCtrl;

    /**
     * Vaccine Type store
     */
    private VaccineTypeStore vcTypeStr;
    /**
     * Vaccine Type store
     */
    private VaccinationCenterStore vcCenterStr;
    /**
     * Vaccine Type store
     */
    private VaccineAdministrationNurseStore vcAdminStr;
    /**
     * Vaccine Type store
     */
    private VacinationCenterController vcCenterCtrl;

    /**
     * Validator
     */
    private Validator vldt;

    /**
     * Constructor instantiates a new Legacy System Data Controller
     */
    public LegacySystemDataController() {
        this.company = App.getInstance().getCompany();
        this.str = company.getLegacySystemStore();
        this.snsUserStr = company.getSNSUserStore();
        this.vcTypeCtrl = new VaccineTypeController();
        this.vcTypeStr = company.getVaccineTypeStore();
        this.vldt= new Validator();

        this.vcCenterStr = company.getVaccinationCenterStore();
        this.vcCenterCtrl = new VacinationCenterController();
        this.vcAdminStr = company.getVaccineAdministrationNurseStore();
    }

    /**
     * Reads a csv file and validates it
     * @param file CSV file
     * @return Legacy System list DTO
     */
    public ArrayList<LegacySystemDataDTO> readCsv(File file) {
        FileLegacySystem fileSer = new FileLegacySystem();

        return fileSer.read(file);
    }

    /**
     * Store new user in list
     * @param user Legacy system user data
     * @return true if user was added, false otherwise
     */
    public boolean addUser(LegacySystemData user) {
        return this.str.addUser(user);
    }

    /**
     * Remove user from store
     * @param user Legacy system user data
     * @return true if user was removed, false otherwise
     */
    public boolean removeUser(LegacySystemData user) {
        return this.str.removeUser(user);
    }

    /**
     * Validates a duplicate user in list
     * @return true if there are no duplicates, false otherwise
     */
    public boolean validateDuplicate(List<LegacySystemData> list) {
        return this.str.validateDuplicate(list);
    }

    /**
     * Validates a duplicate user in store
     * @return true if there are no duplicates, false otherwise
     */
    public boolean validateDuplicate(LegacySystemData userData) {
        return this.str.validateDuplicate(userData);
    }

    /**
     * Validates legacy system data from csv file list
     * @param list Legacy system list
     * @return Verified list until the last valid line
     */
    public ArrayList<LegacySystemData> validateList(ArrayList<LegacySystemData> list) {
        ArrayList<LegacySystemData> newList = new ArrayList<>();
        ListIterator<LegacySystemData> it = list.listIterator();

        long snsNumber;
        int intDose;
        String vaccine, lotNumber, schedule, arrival, administration, leaving, snsUserName, vaccineDesc;
        LegacySystemData line;

        while (it.hasNext()) {
            line = it.next();

            snsNumber = Long.parseLong(line.getSNSNUmber());
            vaccine = line.getVaccine();
            intDose = line.getDose();
            lotNumber = line.getLotNumber();
            schedule = line.getSchedule();
            arrival = line.getArrival();
            administration = line.getAdministration();
            leaving = line.getLeaving();
            snsUserName = getSnsUserName(snsNumber);
            vaccineDesc = getVaccineDescription(vaccine);

            //VALIDATIONS
            snsNumber = vldt.validateSnsNumber(snsNumber);
            vldt.validateNumDoses(intDose);

            if (!snsUserStr.existsSNSUserStore(snsNumber)) {
                throw new InvalidSnsNumberException("\nSNS User '" +snsNumber+ "' not registered");
            } else if (!vcTypeStr.existsVaccineStore(vaccine)) {
                throw new InvalidLegacySystemException("\nVaccine '" +vaccine+ "' not registered");
            } else if (!vldt.checkLotNumber(lotNumber)) {
                throw new InvalidLegacySystemException("\nLot number '" +lotNumber+ "' is invalid");
            } else if (!vldt.validDateTime(schedule) || !vldt.validDateTime(arrival) ||
                    !vldt.validDateTime(administration) || !vldt.validDateTime(leaving)) {
                throw new InvalidLegacySystemException("Invalid date value(s) in line: \n" + it.nextIndex());
            }

            LegacySystemData user = new LegacySystemData(String.valueOf(snsNumber), vaccine, intDose, lotNumber, schedule, arrival, administration, leaving);
            user.setSnsUserName(snsUserName);
            user.setVaccineDesc(vaccineDesc);

            newList.add(user);
        }
        return newList;
    }

    /**
     * Getter
     * @return Legacy System Store
     */
    public LegacySystemStore getLegacySystem() {
        return this.str;
    }

    /**
     * Setter
     * @param list Legacy system data list
     */
    public void setLegacySystem(ArrayList<LegacySystemData> list) {
        this.str.setLegacySysList(list);
    }

    /**
     * Sort legacy system list by Arrival
     * @return sorted list
     */
    public List<LegacySystemData> sortByArrival() {
        List<LegacySystemData> list = company.getLegacySystemStore().getLegacySystemStore();
        Collections.sort(list, new LegacySystemArrivalSort());
        return list;
    }

    /**
     * Sort legacy system list by Leaving
     * @return sorted list
     */
    public List<LegacySystemData> sortByLeaving() {
        List<LegacySystemData> list = company.getLegacySystemStore().getLegacySystemStore();
        Collections.sort(list, new LegacySystemLeavingSort());
        return list;
    }

    /**
     * Convert String Dose to Int
     * @param string Dose string
     * @return dose int
     */
    public int csvDosesStringToInt(String string) {
        int i = 0;
        String[] doses = new String[] {"Primeira", "Segunda", "Terceira", "Quarta", "Quinta", "Sexta", "Setima", "Oitava", "Nona", "Decima"};
        int[] tests = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        for (int j = 0; j < doses.length; j++) {
            if (string.equals(doses[j]))
                return tests[j];
        }
        return 0;
    }

    /**
     * Get SNS User Name
     * @param snsNumber sns user number
     * @return sns user name
     */
    public String getSnsUserName(long snsNumber) {
        String name = "No_Name";
        for (SNSUser user: snsUserStr.getSnsUserList()) {
            if (user.getSnsNumber() == snsNumber) {
                name = user.getName();
            }
        }
        return name;
    }

    /**
     * Get Vaccine Description
     * @param vcName vaccine name
     * @return  vaccine description
     */
    public String getVaccineDescription(String vcName) {
        String desc = "No_Description";
        for (VaccineType vcType: vcTypeStr.getVcTypeList()) {
            if (vcType.getVaccineName().equals(vcName))
                desc = vcType.getVaccineTypeDescription();
        }
        return desc;
    }

    public void saveLegacySystemData(String name) {
        List<LegacySystemData> list = str.getLegacySystemStore();
        VaccinationCenter legacyCenter = new VaccinationCenter(name, "", "", "", new Email(name + "1@legcenter.com"), name + ".pt",
                "9:00", "18:00", "", 1000000000);

        vcCenterStr.add(legacyCenter);
        vcCenterCtrl.addWaitingRoomToVaccinationCenter(legacyCenter.getEmail());

        for (LegacySystemData user: list) {
            long snsNumber = Long.parseLong(user.getSNSNUmber());
            VaccineType vaccineType = vcTypeCtrl.getVaccineByName(user.getVaccine());

            //SCHEDULE
            VaccineBase userSchedule = new VaccineBase(snsUserStr.getSNSUserByNumber(snsNumber), user.getSchedule(), vaccineType, legacyCenter);

            //ARRIVAL
            String arrivalDate = user.getArrival().split(" ")[0];
            String arrivalTime = user.getArrival().split(" ")[1];
            SNSUserArrival userArrival = new SNSUserArrival(snsNumber, arrivalDate, arrivalTime);

            //ADMINISTRATION
            DateTimeFormatter fmtDate= DateTimeFormatter.ofPattern("MM/dd/yyyy");
            DateTimeFormatter fmtTime = DateTimeFormatter.ofPattern("HH:mm");

            String[] adminDateTime = user.getAdministration().split(" ");

            String[] dateParts= adminDateTime[0].split("/");
            if (dateParts[0].length() == 1)
                adminDateTime[0] = "0" + adminDateTime[0];
            LocalDate.parse(adminDateTime[0], fmtDate);

            String[] timeParts= adminDateTime[1].split(":");
            if (timeParts[0].length() == 1)
                adminDateTime[1] = "0" + adminDateTime[1];
            LocalTime.parse(adminDateTime[1], fmtTime);

            LocalDateTime administrationDateTime = LocalDateTime.parse((adminDateTime[0] +" "+ adminDateTime[1]), DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm"));
            VaccineAdministrationNurse userAdministration = new VaccineAdministrationNurse(
                    snsNumber, vaccineType, legacyCenter, administrationDateTime, user.getLotNumber(),
                    user.getDose(), new Email("nurse" + legacyCenter.getEmail()));

            //LEAVING


            //STORE
            vcCenterStr.addVaccine(userSchedule);
            vcCenterCtrl.addSNSUserArrivalToWaitingRoom(legacyCenter.getEmail(), userArrival);
            vcAdminStr.add(userAdministration);
        }
    }
}
