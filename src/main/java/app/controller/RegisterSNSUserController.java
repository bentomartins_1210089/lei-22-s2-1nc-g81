package app.controller;
import app.domain.model.PasswordGenerator;
import app.domain.model.SNSUser;
import app.domain.store.SNSUserStore;
import pt.isep.lei.esoft.auth.domain.model.Password;

import java.util.Date;

/**
 * The Register SNS User Controller.
 *
 * @author Bento Martins <1210089>
 */
public class RegisterSNSUserController {

    private App app;

    /**
     * Instantiates a new Register SNS User controller.
     */
    public RegisterSNSUserController() {
        this.app = App.getInstance();
    }

    /**
     * Creates a SNS User with sex.
     *
     * @param nameUser     the name of SNS User
     * @param birthDate    the birth date of SNS User
     * @param snsNumber    the SNS number of SNS User
     * @param phoneNumber  the Phone number of SNS User
     * @param emailAddress the e-mail address of SNS User
     * @param ccNumber     the Citizen Card number of SNS User
     * @param userAddress  the address of SNS User
     * @param sexUser      the sex of SNS User
     * @return a SNS User
     */
    public SNSUser createSNSUser(String nameUser, Date birthDate, long snsNumber, long phoneNumber, String emailAddress,long ccNumber,String userAddress, String sexUser) {

        SNSUserStore store= app.getCompany().getSNSUserStore();
        SNSUser snsuser = store.createSNSUser(nameUser,birthDate,snsNumber,phoneNumber,emailAddress,ccNumber,userAddress,sexUser);

         if (store.existsSNSUserStore(snsuser)) {

            if (!app.getCompany().existsUser(snsuser)) {
                return snsuser;
            }
        }

        return null;
    }

    /**
     * Creates a SNS User without sex.
     *
     * @param nameUser     the name of SNS User
     * @param birthDate    the birth date of SNS User
     * @param snsNumber    the SNS number of SNS User
     * @param phoneNumber  the Phone number of SNS User
     * @param emailAddress the e-mail address of SNS User
     * @param ccNumber     the Citizen Card number of SNS User
     * @param userAddress  the address of SNS User
     * @return a SNS User
     */
    public SNSUser createSNSUser(String nameUser, Date birthDate, long snsNumber, long phoneNumber, String emailAddress,long ccNumber,String userAddress) {

        SNSUserStore store= app.getCompany().getSNSUserStore();
        SNSUser snsuser = store.createSNSUser(nameUser,birthDate,snsNumber,phoneNumber,emailAddress,ccNumber,userAddress);

        if (store.existsSNSUserStore(snsuser)) {

            if (!app.getCompany().existsUser(snsuser)) {
                return snsuser;
            }
        }

        return null;
    }

    /**
     * Save SNS User.
     *
     * @param snsuser a SNS User
     */
    public void saveSNSUser(SNSUser snsuser){

        PasswordGenerator passGen = new PasswordGenerator();
        String passToUser = passGen.generatePassword();

        SNSUserStore store= app.getCompany().getSNSUserStore();

        snsuser.setPassword(passToUser);

        store.addSNSUserToStore(snsuser);
        app.getCompany().createAuthUser(snsuser);

        System.out.println("Added User: user: "+ snsuser.getEmail() + " password: "+snsuser.getPassword());

    }

    }




