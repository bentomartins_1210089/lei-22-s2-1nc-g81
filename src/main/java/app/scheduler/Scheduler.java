package app.scheduler;

import app.controller.App;
import app.domain.model.Company;

import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static app.controller.VaccinatedCsvController.writeCSVFile;

/**
 * Scheduler class.
 * @Tassio Gomes 1170065
 */

public class Scheduler {

    /**
     * It reads the time from the config.properties file and then it creates a timer that will run at that time. When the timer
     * runs it will get the list of vaccine administrations and then it will create a hashmap with the number of people
     * vaccinated per center. Then it will write the date and the number of vaccinated people per day in each vaccination center in a csv file
     */

    public Scheduler() {
        try(FileReader reader =  new FileReader("config.properties")) {
            Properties properties = new Properties();
            properties.load(reader);
            String runTime = properties.getProperty("time");
            LocalTime time = LocalTime.parse(runTime) ;
            Timer timer = new Timer();
            LocalDateTime currentTime = LocalDateTime.now();
            String filepath= "NumberOfVaccinatedPeoplePerDay";
            if(time.getHour()==currentTime.getHour() && time.getMinute()==currentTime.getMinute() && time.getSecond()<currentTime.getSecond() || time.getHour()==currentTime.getHour() && time.getMinute()<currentTime.getMinute() || time.getHour()<currentTime.getHour() ){
//                System.out.println("Program already saved the number of vaccinated people today");
                timer.cancel();
            }
            else {
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        Company company = App.getInstance().getCompany();
                        LocalDate today = LocalDate.now();
                        HashMap<String,Integer> hashMap = new HashMap<>();
                        company.getVaccinationCenterStore().getAll().forEach(vaccinationCenter -> {
                            String name = vaccinationCenter.getName();
//                            int total = vaccinationCenter.getWaitingRoom().getWaitingRoomList().size();
                            //TODO: A lista com o total de doses deve vir de uma store predefinida aqui
                            int total = company.getVaccineAdministrationNurseStore()
                                    .getVaccineAdministrationList()
                                            .stream()
                                                    .filter(v->v.getVaccinationCenter()
                                                            .getName()
                                                            .equals(vaccinationCenter.getName()))
                                                            .collect(Collectors.toList()).size();
                            hashMap.put(name, total);
                        });
                        writeCSVFile(filepath,today.getDayOfMonth(),today.getMonthValue(),today.getYear(),hashMap);
                        timer.cancel();
                    }
                };
                Calendar date = Calendar.getInstance();
                date.set(Calendar.HOUR_OF_DAY, time.getHour());
                date.set(Calendar.MINUTE, time.getMinute());
                date.set(Calendar.SECOND, time.getSecond());
                timer.schedule(task, date.getTime());
            }}catch (Exception e) {
            e.printStackTrace();
        }
    }
}