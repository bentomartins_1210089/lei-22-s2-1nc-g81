package app.dto;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineType;

import java.time.LocalDateTime;


/**
 * @author Tassio Gomes <1170065>
 * @author Mário Borja <1200586>
 * VaccoineScheduleDto Class
 */
public class VaccineScheduleDto {
    /**
     * Attributes
     */
    private Integer snsNumber;
    private VaccineType vaccineType;
    private VaccinationCenter vaccinationCenter;
    private LocalDateTime localDateTime;
    public VaccineScheduleDto(Integer snsNumber, VaccineType vaccineType, VaccinationCenter vaccinationCenter, LocalDateTime localDateTime) {
        this.snsNumber = snsNumber;
        this.vaccineType = vaccineType;
        this.vaccinationCenter = vaccinationCenter;
        this.localDateTime = localDateTime;
    }


    /**
     * This function returns the SNS number of  the user
     *
     * @return The number of sns.
     */
    public Integer getSnsNumber() {
        return snsNumber;
    }
    /**
     * This function returns the vaccine type of the vaccine
     *
     * @return The vaccineType is being returned.
     */
    public VaccineType getVaccineType() {
        return vaccineType;
    }
    /**
     * This function returns the vaccination center
     *
     * @return The vaccination center.
     */
    public VaccinationCenter getVaccinationCenter() {
        return vaccinationCenter;
    }
    /**
     * > This function returns the local date time
     *
     * @return A LocalDateTime object
     */
    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }
}