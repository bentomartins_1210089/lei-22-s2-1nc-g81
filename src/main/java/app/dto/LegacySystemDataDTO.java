package app.dto;

/**
 * @author Tiago Oliveira <1211669>
 */

public class LegacySystemDataDTO {

    /**
     * Class attributes
     */
    private String snsNumber;
    private String vaccine;
    private String dose;
    private String lotNumber;
    private String schedule;
    private String arrival;
    private String administration;
    private String leaving;

    /**
     * Contructor instantiates an legacy system data DTO
     * @param snsNumber         SNS user number
     * @param vaccine           Vaccine name
     * @param dose              Dose number
     * @param lotNumber         Lot number
     * @param schedule          Schedule date and time
     * @param arrival           Arrival date and time
     * @param administration    Administration date and time
     * @param leaving           Leaving date and time
     */
    public LegacySystemDataDTO(String snsNumber, String vaccine, String dose, String lotNumber, String schedule, String arrival, String administration, String leaving) {
        this.snsNumber = snsNumber;
        this.vaccine = vaccine;
        this.dose = dose;
        this.lotNumber = lotNumber;
        this.schedule = schedule;
        this.arrival = arrival;
        this.administration = administration;
        this.leaving = leaving;
    }

    /**
     * Getters
     */
    public String getSNSNUmber() {
        return this.snsNumber;
    }

    public String getVaccine() {
        return this.vaccine;
    }

    public String getDose() {
        return this.dose;
    }

    public String getLotNumber() {
        return this.lotNumber;
    }

    public String getSchedule() {
        return this.schedule;
    }

    public String getArrival() {
        return this.arrival;
    }

    public String getAdministration() {
        return this.administration;
    }

    public String getLeaving() {
        return this.leaving;
    }

    @Override
    public String toString() {
        return String.format("SNS user number: %s | Vaccine : %s | Dose: %s | " +
                        "Lot number: %s | Schedule: %s | Arrival: %s | " +
                        "Administration: %s | Leaving: %s",
                snsNumber, vaccine, dose, lotNumber, schedule, arrival, administration, leaving);
    }
}
