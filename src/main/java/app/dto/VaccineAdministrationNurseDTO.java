package app.dto;

import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineType;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.time.LocalDateTime;

/**
 * The type Vaccine administration nurse dto.
 * @Mario Borja 1200586
 */
public class VaccineAdministrationNurseDTO {

    private long snsNumber;
    private VaccineType vaccine;
    private VaccinationCenter vaccinationCenter;
    private LocalDateTime localDateTime;
    private String lotNumber;
    private Integer doseNumber;
    private Email email;

    /**
     * Instantiates a new Vaccine administration nurse dto.
     *
     * @param snsNumber         the sns number
     * @param vaccine           the vaccine
     * @param vaccinationCenter the vaccination center
     * @param localDateTime     the local date time
     * @param lotNumber         the lot number
     * @param doseNumber        the dose number
     * @param email             the email
     */
    public VaccineAdministrationNurseDTO(long snsNumber, VaccineType vaccine, VaccinationCenter vaccinationCenter, LocalDateTime localDateTime, String lotNumber, Integer doseNumber, Email email) {
        this.snsNumber = snsNumber;
        this.vaccine = vaccine;
        this.vaccinationCenter = vaccinationCenter;
        this.localDateTime = localDateTime;
        this.lotNumber = lotNumber;
        this.doseNumber = doseNumber;
        this.email = email;
    }

    /**
     * Gets sns number.
     *
     * @return the sns number
     */
    public long getSnsNumber() {
        return snsNumber;
    }


    /**
     * Gets vaccine.
     *
     * @return the vaccine
     */
    public VaccineType getVaccine() {
        return vaccine;
    }


    /**
     * Gets vaccination center.
     *
     * @return the vaccination center
     */
    public VaccinationCenter getVaccinationCenter() {
        return vaccinationCenter;
    }


    /**
     * Gets local date time.
     *
     * @return the local date time
     */
    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }


    /**
     * Gets lot number.
     *
     * @return the lot number
     */
    public String getLotNumber() {
        return lotNumber;
    }


    /**
     * Gets dose number.
     *
     * @return the dose number
     */
    public Integer getDoseNumber() {
        return doseNumber;
    }


    /**
     * Gets email.
     *
     * @return the email
     */
    public Email getEmail() {
        return email;
    }

}
