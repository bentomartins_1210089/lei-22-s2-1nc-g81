package app.dto;
import app.exception.*;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * SNS User DTO Class.
 *
 * @author Bento Martins <1210089>
 */
public class SNSUserDto {

    // Attributes
    private String nameUser;
    private Date birthDate;
    private long snsNumber;
    private long phoneNumber;
    private String emailAddress;
    private long ccNumber;
    private String userAddress;
    private String sexUser;
    private String password;

    /**
     * Gets SNS User DTO name.
     *
     * @return the name of SNS User DTO
     */
    public String getName() {
        return nameUser;
    }

    /**
     * Sets SNS User DTO name.
     *
     * @param nameUser the name of SNS User DTO
     */
    public void setName(String nameUser) {
        this.nameUser = nameUser;
    }

    /**
     * Gets SNS User DTO birth date.
     *
     * @return the birth date of SNS User DTO
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Sets SNS User DTO birth date.
     *
     * @param birthDate the birth date of SNS User DTO
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Gets SNS User DTO SNS number.
     *
     * @return the SNS number of SNS User DTO
     */
    public long getSnsNumber() {
        return snsNumber;
    }

    /**
     * Sets SNS User DTO SNS number.
     *
     * @param snsNumber the SNS number of SNS User DTO
     */
    public void setSnsNumber(long snsNumber) {
        this.snsNumber = snsNumber;
    }

    /**
     * Gets SNS User DTO phone.
     *
     * @return the phone of SNS User DTO
     */
    public long getPhone() {
        return phoneNumber;
    }

    /**
     * Sets SNS User DTO phone.
     *
     * @param phoneNumber the phone number of SNS User DTO
     */
    public void setPhone(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Gets SNS User DTO e-mail.
     *
     * @return the email of SNS User DTO
     */
    public String getEmail() {
        return emailAddress;
    }

    /**
     * Sets SNS User DTO e-mail.
     *
     * @param email the email of SNS User DTO
     */
    public void setEmail(String email) {
        this.emailAddress = email;
    }

    /**
     * Gets SNS User DTO Citizen Card number.
     *
     * @return the Citizan Card number of SNS User DTO
     */
    public long getCcNumber() { return ccNumber;  }

    /**
     * Sets SNS User DTO Citizan Card number.
     *
     * @param ccNumber the Citizan Card number of SNS User DTO
     */
    public void setCcNumber(long ccNumber) {this.ccNumber = ccNumber; }

    /**
     * Gets SNS User DTO sex.
     *
     * @return the sex of SNS User DTO
     */
    public String getSexUser() { return sexUser; }

    /**
     * Sets SNS User DTO sex.
     *
     * @param sexUser the sex of SNS User DTO
     */
    public void setSexUser(String sexUser) { this.sexUser = sexUser; }


    /**
     * Gets SNS User DTO address.
     *
     * @return the user address of SNS User DTO
     */
    public String getUserAddress() { return userAddress; }

    /**
     * Sets SNS User DTO address.
     *
     * @param userAddress the user address of SNS User DTO
     */
    public void setUserAddress(String userAddress) { this.userAddress = userAddress; }

    /**
     * Gets SNS User DTO password.
     *
     * @return the password of SNS User DTO
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets SNS User DTO  password.
     *
     * @param password the password of SNS User DTO
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Constructor of a new SNS User DTO.
     */
    public SNSUserDto() {}

    /**
     * Constructor of a new SNS User DTO.
     *
     * @param nameUser     the name of SNS User
     * @param birthDate    the birth date of SNS User
     * @param snsNumber    the SNS number of SNS User
     * @param phoneNumber  the Phone number of SNS User
     * @param emailAddress the e-mail address of SNS User
     * @param ccNumber     the Citizen Card number of SNS User
     * @param userAddress  the address of SNS User
     * @param sexUser      the sex of SNS User
     */
    // Constructor with Sex
    public SNSUserDto(String nameUser, Date birthDate, long snsNumber, long phoneNumber, String emailAddress, long ccNumber, String userAddress, String sexUser) {
        this.nameUser = nameUser;
        this.birthDate = birthDate;
        this.snsNumber = snsNumber;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.ccNumber = ccNumber;
        this.userAddress = userAddress;
        this.sexUser = sexUser;
    }

    /**
     * Constructor of a new SNS User DTO.
     *
     * @param nameUser     the name of SNS User
     * @param emailAddress the e-mail address of SNS User
     * @param password     the password of SNS User
     */
    // Simple Constructor
    public SNSUserDto(String nameUser, String emailAddress, String password) {
        this.nameUser = nameUser;
        this.emailAddress = emailAddress;
        this.password = password;
    }

    /**
     * Prints to String SNS User DTO.
     *
     * @return string with information about SNS User
     */
    @Override
    public String toString() {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = formatter.format(this.birthDate);

        return  "Name: " + nameUser + "\n"+
                "Birth Date: " + strDate + "\n"+
                "Sex: " + sexUser + "\n"+
                "SNS Number: " + snsNumber + "\n"+
                "CC Number: " + ccNumber + "\n"+
                "Phone: " + phoneNumber + "\n"+
                "E-mail: " + emailAddress + "\n"+
                "Address: " + userAddress;

    }
}
