package app.dto;

import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.util.List;

public class SNSUserArrivalDTO {

    private long snsNumber;
    private String time;
    private String date;

    public SNSUserArrivalDTO(long snsNumber, String time, String date) {
        this.snsNumber = snsNumber;
        this.time = time;
        this.date = date;
    }

    public long getSNSNUmber() {
        return this.snsNumber;
    }

    public String getTime() {
        return this.time;
    }

    public String getDate() {
        return this.date;
    }
}
