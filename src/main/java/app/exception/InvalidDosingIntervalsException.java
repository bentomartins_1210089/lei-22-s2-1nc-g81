package app.exception;

public class InvalidDosingIntervalsException extends IllegalArgumentException {
    public InvalidDosingIntervalsException() {
        System.out.println("\nWrong interval introduced. Please insert all valid interval values greater than 0 (days).");
    }
}
