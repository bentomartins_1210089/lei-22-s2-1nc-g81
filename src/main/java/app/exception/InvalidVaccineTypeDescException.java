package app.exception;

public class InvalidVaccineTypeDescException extends IllegalArgumentException {

    public InvalidVaccineTypeDescException() {

        System.out.println("Vaccine Type Description is not valid. Please insert a valid type description! [2-30] Alphanumeric");
    }
}
