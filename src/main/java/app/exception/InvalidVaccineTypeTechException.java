package app.exception;

public class InvalidVaccineTypeTechException extends IllegalArgumentException {

    public InvalidVaccineTypeTechException() {

        System.out.println("Vaccine Type Technology is not valid. Please insert a valid type value from list");
    }
}
