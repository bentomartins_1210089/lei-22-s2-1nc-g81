package app.exception;

public class InvalidBirthDateException extends IllegalArgumentException {
    public InvalidBirthDateException(String message) {
        System.out.println(message);
    }
}
