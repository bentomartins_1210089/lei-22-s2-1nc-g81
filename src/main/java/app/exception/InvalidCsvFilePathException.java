package app.exception;

import java.io.FileNotFoundException;

public class InvalidCsvFilePathException extends FileNotFoundException {
    public InvalidCsvFilePathException() {
        System.out.println("File not found, please input a correct path for the csv file:\n");
    }
}
