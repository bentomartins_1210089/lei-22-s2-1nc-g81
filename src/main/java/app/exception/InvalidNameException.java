package app.exception;

public class InvalidNameException extends IllegalArgumentException {
    public InvalidNameException(String message) {
        System.out.println(message);
    }
}