package app.exception;

public class InvalidDosagesException extends IllegalArgumentException {
    public InvalidDosagesException() {
        System.out.println("\nWrong dosage introduced. Please insert all valid dosage values greater than 0.0mL.");
    }
}
