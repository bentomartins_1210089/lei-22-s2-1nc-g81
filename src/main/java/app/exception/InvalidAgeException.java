package app.exception;

public class InvalidAgeException extends NumberFormatException {
    public InvalidAgeException() {
        System.out.println("\nWrong age introduced. Please insert a valid age greater than 5.");
    }
}
