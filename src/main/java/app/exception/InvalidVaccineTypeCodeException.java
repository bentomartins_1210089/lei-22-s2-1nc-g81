package app.exception;

public class InvalidVaccineTypeCodeException extends IllegalArgumentException {

    public InvalidVaccineTypeCodeException() {

                System.out.println("Vaccine Type Code is not valid. Please insert a valid type code. [5] Alphanumeric Format:[AA999]");
    }
}