package app.exception;

public class InvalidEmailAddressException extends IllegalArgumentException {
    public InvalidEmailAddressException(String message) {
        System.out.println(message);
    }
}
