package app.exception;

public class InvalidAddressException extends IllegalArgumentException {
    public InvalidAddressException(String message) {
        System.out.println(message);
    }
}