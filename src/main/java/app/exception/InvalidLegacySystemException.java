package app.exception;

/**
 * @author Tiago Oliveira <1211669>
 */
public class InvalidLegacySystemException extends IllegalArgumentException {
    public InvalidLegacySystemException(String message) {
        System.out.println(message);
    }
}