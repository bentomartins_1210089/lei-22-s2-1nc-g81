package app.mappers;

import app.controller.LegacySystemDataController;
import app.domain.model.LegacySystemData;
import app.dto.LegacySystemDataDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tiago Oliveira <1211669>
 */

public class LegacySystemDataMapper {

    private LegacySystemDataController ctrl;

    public LegacySystemDataMapper() {
        ctrl = new LegacySystemDataController();
    }

    /**
     * Legacy user DTO to Legacy user
     * @param dto Legacy System Data DTO
     * @return Legacy System Data User
     */
    public LegacySystemData dtoToLegacyData(LegacySystemDataDTO dto) {
        LegacySystemData user = new LegacySystemData(dto.getSNSNUmber(), dto.getVaccine(),
                ctrl.csvDosesStringToInt(dto.getDose()), dto.getLotNumber(), dto.getSchedule(), dto.getArrival(),
                dto.getAdministration(), dto.getLeaving());

        return user;
    }

    /**
     * Legacy user DTO List to Legacy user List
     * @param listDto Legacy System Data DTO List
     * @return Legacy System Data User List
     */
    public ArrayList<LegacySystemData> dtoToLegacyData(List<LegacySystemDataDTO> listDto) {
        ArrayList<LegacySystemData> list = new ArrayList();

        for (LegacySystemDataDTO dto : listDto) {
            list.add(this.dtoToLegacyData(dto));
        }

        return list;
    }
}
