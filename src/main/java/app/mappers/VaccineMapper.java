package app.mappers;

import app.controller.App;
import app.domain.model.SNSUser;
import app.domain.model.VaccineBase;
import app.dto.VaccineScheduleDto;

import java.util.Objects;
import java.util.Optional;

/**
 * @author Tassio Gomes <1170065>
 * @author Mário Borja <1200586>
 */

public class VaccineMapper {
    /**
     *
     * @param dto
     * @return SnsNumber, LocalDateTime, Vaccine Type, Vaccination Center
     */

    public static VaccineBase dtoToBase(VaccineScheduleDto dto) {
        Integer snsNumber = dto.getSnsNumber();
        Optional<SNSUser> user = Objects.isNull(snsNumber) ? Optional.empty() : App.getInstance()
                .getCompany()
                .getSNSUserStore()
                .getSnsUserList()
                .stream()
                .filter(snsUser -> snsUser.getSnsNumber() == snsNumber)
                .findFirst();

        VaccineBase base = new VaccineBase(null, dto.getLocalDateTime().toString(), dto.getVaccineType(), dto.getVaccinationCenter());
        user.ifPresent(base::setSnsUser);
        return base;

    }
}