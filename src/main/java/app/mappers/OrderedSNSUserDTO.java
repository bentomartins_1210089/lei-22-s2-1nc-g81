package app.mappers;

import app.domain.model.SNSUserArrival;
import app.dto.SNSUserArrivalDTO;
import pt.isep.lei.esoft.auth.domain.model.UserRole;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class OrderedSNSUserDTO {

    public OrderedSNSUserDTO() {
    }
    public SNSUserArrivalDTO toDTO(SNSUserArrival snsUserArrival) {
        return new SNSUserArrivalDTO(snsUserArrival.getSnsNumber(), snsUserArrival.getTime(), snsUserArrival.getTime());
    }

    public List<SNSUserArrivalDTO> toDTO(List<SNSUserArrival> userArrivalList) {
        List<SNSUserArrivalDTO> SNSUserArrivalDTO = new ArrayList();
        Iterator          var3     = userArrivalList.iterator();

        while(var3.hasNext()) {
            SNSUserArrival userArrival = (SNSUserArrival)var3.next();
            SNSUserArrivalDTO.add(this.toDTO(userArrival));
        }

        return SNSUserArrivalDTO;
    }

}
