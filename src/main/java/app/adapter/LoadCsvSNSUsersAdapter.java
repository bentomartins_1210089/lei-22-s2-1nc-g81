package app.adapter;
import app.domain.model.SNSUser;
import app.domain.model.Validator;
import app.dto.SNSUserDto;
import org.apache.commons.lang3.StringUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The Load CSV with SNS Users Adapter.
 *
 * @author Bento Martins <1210089>
 */
public class LoadCsvSNSUsersAdapter {

    Validator validator = new Validator();

    private static final String DATE_FORMAT = "dd/MM/yyyy";

    /**
     * Get SNS Users DTO from CSV.
     *
     * @param path the path of CSV file
     * @return the list of SNS Users DTO
     * @throws FileNotFoundException the file not found exception
     */
    public List<SNSUserDto> GetSNSUsersDtoFromCsv(String path) throws FileNotFoundException {

        List<SNSUserDto> listDto = new ArrayList();

        Scanner sc = new Scanner(new File(path));

        String firstLine = null;

        try {
            firstLine = sc.nextLine();
        }
        catch (NoSuchElementException e) {
            System.out.println("CSV file is empty. Please correct before importing.");
            return listDto;
        }

        int commaSep = StringUtils.countMatches(firstLine, ",");
        int semiSep = StringUtils.countMatches(firstLine, ";");

        sc.close();

        if (commaSep > semiSep) {
            listDto = readCsvWithoutHeaders(path);
        } else {
            listDto = readCsvWithHeaders(path);
        }

        return listDto;
    }

    /**
     * Read CSV with headers.
     *
     * @param path the path of CSV file
     * @return the list of SNS Users DTO
     * @throws FileNotFoundException the file not found exception
     */
    public List<SNSUserDto> readCsvWithHeaders(String path) throws FileNotFoundException {

        Scanner sc = new Scanner(new File(path));

        String firstLine = sc.nextLine();

        List<SNSUserDto> listDto = new ArrayList();

        while (sc.hasNextLine())  //returns a boolean value
        {
            String line = sc.nextLine();

            String[] sepLine = line.split(";");

            SNSUser tmp = new SNSUser();
            SNSUserDto snsUserDto = null;

            try {

                // Name
                String sepLineCorrect = sepLine[0].replace("\u00AD","");
                String nameUser = validator.validateName(sepLineCorrect);

                // Sex
                String sexUser = sepLine[1];

                if (sexUser.equals(""))
                {
                    sexUser="NA";
                }
                else {
                    sexUser= validator.validateSex(sexUser);
                }

                // Birth Date
                Date birthDate;
                SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
                formatter.setLenient(false);

                try {
                    birthDate = validator.validateBirthDate(formatter.parse(sepLine[2]));
                } catch (ParseException e) {
                    throw new IllegalArgumentException();
                }

                // Address
                sepLineCorrect = sepLine[3].replace("\"","");
                sepLineCorrect = sepLineCorrect.replace("\u008D","");
                sepLineCorrect = sepLineCorrect.replace("\u0081","");
                sepLineCorrect = sepLineCorrect.replace("\u008D","");
                sepLineCorrect = sepLineCorrect.replace("Áµ","õe");
                sepLineCorrect = sepLineCorrect.replace("Á“","Ó");
                sepLineCorrect = sepLineCorrect.replace("Á´","ô");
                String userAddress = validator.validateAddress(sepLineCorrect);

                // Phone Number
                long phoneNumber;
                try {
                    phoneNumber = validator.validatePhoneNumber(Long.parseLong(sepLine[4]));
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException();
                }

                // E-mail
                String emailAddress = validator.validateEmailAddress(sepLine[5]);

                // SNS Number
                long snsNumber;
                try {
                    snsNumber = validator.validateSnsNumber(Long.parseLong(sepLine[6]));
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException();
                }

                // CC Number
                long ccNumber;
                try {
                    ccNumber = validator.validateCcNumber(Long.parseLong(sepLine[7]));
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException();
                }

                // CreateDTO
                snsUserDto = new SNSUserDto(nameUser,birthDate,snsNumber,phoneNumber,emailAddress,ccNumber,userAddress,sexUser);
                listDto.add(snsUserDto);

            } catch (IllegalArgumentException e) {
                System.out.print("\nError on line: " + line + "\n");
                System.out.println("CSV file has wrong information. Please correct before importing.");
                break;
            }

            catch (IndexOutOfBoundsException e) {
                System.out.print("\nError on line: " + line + "\n");
                System.out.println("CSV file has wrong information. Please correct before importing.");
                listDto=null;
                break;
            }

        }

        return listDto;

    }

    /**
     * Read CSV without headers.
     *
     * @param path the path of CSV file
     * @return the list of SNS Users DTO
     * @throws FileNotFoundException the file not found exception
     */
    public List<SNSUserDto> readCsvWithoutHeaders(String path) throws FileNotFoundException {

        Scanner sc = new Scanner(new File(path));

        List<SNSUserDto> listDto = new ArrayList();

        while (sc.hasNextLine())  //returns a boolean value
        {
            String line = sc.nextLine();

            String[] sepLine = line.split(",");

            SNSUser tmp = new SNSUser();
            SNSUserDto snsUserDto = null;

            try {

                // Name
                String sepLineCorrect = sepLine[0].replace("\u00AD","");
                String nameUser = validator.validateName(sepLine[0]);

                // Sex
                String sexUser = sepLine[1];

                if (sexUser.equals(""))
                {
                 sexUser="NA";
                }
                else {
                    sexUser= validator.validateSex(sexUser);
                }

                // Birth Date
                Date birthDate;
                SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
                formatter.setLenient(false);

                try {
                    birthDate = validator.validateBirthDate(formatter.parse(sepLine[2]));
                } catch (ParseException e) {
                    throw new IllegalArgumentException();
                }

                // Address
                sepLineCorrect = sepLine[3].replace("\"","");
                sepLineCorrect = sepLineCorrect.replace("\u008D","");
                sepLineCorrect = sepLineCorrect.replace("\u0081","");
                sepLineCorrect = sepLineCorrect.replace("\u008D","");
                sepLineCorrect = sepLineCorrect.replace("Áµ","õe");
                sepLineCorrect = sepLineCorrect.replace("Á“","Ó");
                sepLineCorrect = sepLineCorrect.replace("Á´","ô");
                String userAddress = validator.validateAddress(sepLineCorrect);

                // Phone Number
                long phoneNumber;
                try {
                    phoneNumber = validator.validatePhoneNumber(Long.parseLong(sepLine[4]));
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException();
                }

                // E-mail
                String emailAddress = validator.validateEmailAddress(sepLine[5]);

                // SNS Number
                long snsNumber;
                try {
                    snsNumber = validator.validateSnsNumber(Long.parseLong(sepLine[6]));
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException();
                }

                // CC Number
                long ccNumber;
                try {
                    ccNumber = validator.validateCcNumber(Long.parseLong(sepLine[7]));
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException();
                }

                // CreateDTO
                snsUserDto = new SNSUserDto(nameUser,birthDate,snsNumber,phoneNumber,emailAddress,ccNumber,userAddress,sexUser);
                listDto.add(snsUserDto);

            } catch (IllegalArgumentException e) {
                System.out.print("Error on line: " + line + "\n");
                System.out.println("CSV file has wrong information. Please correct before importing.");
                listDto=null;
                break;
            }

            catch (IndexOutOfBoundsException e) {
                System.out.print("Error on line: " + line + "\n");
                System.out.println("CSV file has wrong information. Please correct before importing.");
                listDto=null;
                break;
            }

        }

        return listDto;

    }

}