package app.files;

import app.domain.model.SNSUser;
import app.domain.model.VaccineBase;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class VaccineBaseFile {
    private static final String FILE_PATH = ".\\src\\main\\serialization_files\\VaccineBaseBinary.bin";

    public void serialize(List<VaccineBase> vaccineBaseList) {
        try {
            FileOutputStream out = new FileOutputStream(FILE_PATH);
            ObjectOutputStream objOut = new ObjectOutputStream(out);
            objOut.writeObject(vaccineBaseList);
            out.close();
            objOut.close();
        } catch (Exception e) {
            System.out.println("Error on serialization of Vaccine Base");
        }
    }

    public ArrayList<VaccineBase> load() {
        ArrayList<VaccineBase> vaccineBaseList = new ArrayList<>();
        try {
            FileInputStream in = new FileInputStream(FILE_PATH);
            ObjectInputStream objIn = new ObjectInputStream(in);
            vaccineBaseList = (ArrayList<VaccineBase>) objIn.readObject();
            in.close();
            objIn.close();
        } catch (Exception e) {
            System.out.println("Error on deserialization of Vaccine Base");
        }
        return vaccineBaseList;
    }

}
