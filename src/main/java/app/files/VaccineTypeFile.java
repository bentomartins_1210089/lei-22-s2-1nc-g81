package app.files;

import app.domain.model.VaccineType;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * @author Tiago Oliveira <1211669>
 */
public class VaccineTypeFile {

    private static final String FILE_PATH = ".\\src\\main\\serialization_files\\VaccineTypeBinary.bin";

    public void serialize(ArrayList<VaccineType> list) {
        try {
            FileOutputStream out = new FileOutputStream(FILE_PATH);
            ObjectOutputStream output = new ObjectOutputStream(out);

            output.writeObject(list);
            out.close(); output.close();
        } catch (Exception e) {
            System.out.println("Error serializing Vaccine Type");
        }
    }

    public ArrayList<VaccineType> load() {
        ArrayList<VaccineType> list = new ArrayList<>();
        try {
            FileInputStream in = new FileInputStream(FILE_PATH);
            ObjectInputStream input = new ObjectInputStream(in);

            list = (ArrayList<VaccineType>) input.readObject();
            in.close(); input.close();
        } catch (Exception e) {
            System.out.println("Error loading previous serialization");
        }
        return list;
    }
}