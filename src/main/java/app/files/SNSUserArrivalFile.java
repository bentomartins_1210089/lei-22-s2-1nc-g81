package app.files;

import app.domain.model.SNSUserArrival;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * @author Tiago Oliveira <1211669>
 */
public class SNSUserArrivalFile {

    private static final String FILE_PATH = ".\\src\\main\\serialization_files\\SNSUserArrivalBinary.bin";

    public void serialize(ArrayList<SNSUserArrival> list) {
        try {
            FileOutputStream out = new FileOutputStream(FILE_PATH);
            ObjectOutputStream output = new ObjectOutputStream(out);

            output.writeObject(list);
            out.close(); output.close();
        } catch (Exception e) {
            System.out.println("Error serializing SNS User Arrival");
        }
    }

    public ArrayList<SNSUserArrival> load() {
        ArrayList<SNSUserArrival> list = new ArrayList<>();
        try {
            FileInputStream in = new FileInputStream(FILE_PATH);
            ObjectInputStream input = new ObjectInputStream(in);

            list = (ArrayList<SNSUserArrival>) input.readObject();
            in.close(); input.close();
        } catch (Exception e) {
            System.out.println("Error loading previous serialization");
        }
        return list;
    }
}
