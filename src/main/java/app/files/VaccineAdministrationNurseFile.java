package app.files;

import app.domain.model.VaccineAdministrationNurse;
import app.domain.model.VaccineBase;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class VaccineAdministrationNurseFile {
    private static final String FILE_PATH = ".\\src\\main\\serialization_files\\VaccineAdministrationNurseBinary.bin";

    public void serialize(List<VaccineAdministrationNurse> vaccineAdministrationNurseList) {
        try {
            FileOutputStream out = new FileOutputStream(FILE_PATH);
            ObjectOutputStream objOut = new ObjectOutputStream(out);
            objOut.writeObject(vaccineAdministrationNurseList);
            out.close();
            objOut.close();
        } catch (Exception e) {
            System.out.println("Error on serialization of Vaccine Administration Nurse List");
        }
    }

    public ArrayList<VaccineAdministrationNurse> load() {
        ArrayList<VaccineAdministrationNurse> vaccineAdministrationNurseList = new ArrayList<>();
        try {
            FileInputStream in = new FileInputStream(FILE_PATH);
            ObjectInputStream objIn = new ObjectInputStream(in);
            vaccineAdministrationNurseList = (ArrayList<VaccineAdministrationNurse>) objIn.readObject();
            in.close();
            objIn.close();
        } catch (Exception e) {
            System.out.println("Error on deserialization of VaccineAdministration");
        }
        return vaccineAdministrationNurseList;
    }

}
