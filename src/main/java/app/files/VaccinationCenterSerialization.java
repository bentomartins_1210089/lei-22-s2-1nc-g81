package app.files;

        import app.domain.model.VaccinationCenter;

        import java.io.FileInputStream;
        import java.io.FileOutputStream;
        import java.io.ObjectInputStream;
        import java.io.ObjectOutputStream;
        import java.util.ArrayList;
        import java.util.List;

public class VaccinationCenterSerialization {

    private static final String FILE_PATH = ".\\src\\main\\serialization_files\\VaccinationCenterBinary.bin";

    public void serialize(List<VaccinationCenter> vaccinationCenterList) {
        try {
            FileOutputStream   out    = new FileOutputStream(FILE_PATH);
            ObjectOutputStream objOut = new ObjectOutputStream(out);
            objOut.writeObject(vaccinationCenterList);
            out.close();
            objOut.close();
        } catch (Exception e) {
            System.out.println("Error on serialization of Vaccination Center");
        }
    }

    public List<VaccinationCenter> load() {
        List<VaccinationCenter> vaccinationCenterList = new ArrayList<>();
        try {
            FileInputStream   in    = new FileInputStream(FILE_PATH);
            ObjectInputStream objIn = new ObjectInputStream(in);
            vaccinationCenterList = (List<VaccinationCenter>) objIn.readObject();
            in.close();
            objIn.close();
        } catch (Exception e) {
            System.out.println("Error on deserialization of Vaccination Center");
        }
        return vaccinationCenterList;
    }


}
