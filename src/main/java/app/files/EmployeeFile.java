package app.files;

import app.domain.model.Employee;
import app.domain.model.SNSUser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class EmployeeFile {

    private static final String FILE_PATH = ".\\src\\main\\serialization_files\\EmployeeBinary.bin";

    public void serialize(List<Employee> employeeList) {
        try {
            FileOutputStream out = new FileOutputStream(FILE_PATH);
            ObjectOutputStream objOut = new ObjectOutputStream(out);
            objOut.writeObject(employeeList);
            out.close();
            objOut.close();
        } catch (Exception e) {
            System.out.println("Error on serialization of Employees");
        }
    }

    public List<Employee> load() {
        List<Employee> employeeList = new ArrayList<>();
        try {
            FileInputStream in = new FileInputStream(FILE_PATH);
            ObjectInputStream objIn = new ObjectInputStream(in);
            employeeList = (List<Employee>) objIn.readObject();
            in.close();
            objIn.close();
        } catch (Exception e) {
            System.out.println("Error on deserialization of Employees");
        }
        return employeeList;
    }


}
