package app.files;

import app.domain.model.VaccineAdministration;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * @author Tiago Oliveira <1211669>
 */
public class VaccineAdministrationFile {

    private static final String FILE_PATH = ".\\src\\main\\serialization_files\\VaccineAdministrationBinary.bin";

    public void serialize(ArrayList<VaccineAdministration> list) {
        try {
            FileOutputStream out = new FileOutputStream(FILE_PATH);
            ObjectOutputStream output = new ObjectOutputStream(out);

            output.writeObject(list);
            out.close(); output.close();
        } catch (Exception e) {
            System.out.println("Error serializing Vaccine Administration");
        }
    }

    public ArrayList<VaccineAdministration> load() {
        ArrayList<VaccineAdministration> list = new ArrayList<>();
        try {
            FileInputStream in = new FileInputStream(FILE_PATH);
            ObjectInputStream input = new ObjectInputStream(in);

            list = (ArrayList<VaccineAdministration>) input.readObject();
            in.close(); input.close();
        } catch (Exception e) {
            System.out.println("Error loading previous serialization");
        }
        return list;
    }
}
