package app.files;

import app.domain.model.LegacySystemData;
import app.dto.LegacySystemDataDTO;

import java.io.*;
import java.util.ArrayList;

/**
 * @author Tiago Oliveira <1211669>
 *
 * File Legacy System class
 */
public class FileLegacySystem {

    public FileLegacySystem() {
    }

    /**
     * Reads a csv file
     * @param filePath File path
     * @return List of Legacy System Data DTO
     */
    public ArrayList<LegacySystemDataDTO> read(String filePath) {
            return read(new File(filePath));
    }

    /**
     * Reads a csv file
     * @param file File to be read
     * @return List of Legacy System Data DTO
     */
    public ArrayList<LegacySystemDataDTO> read(File file) {
        ArrayList<LegacySystemDataDTO> listDTO = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = br.readLine();

            while ((line = br.readLine()) != null) {
                String[] userData = line.split(";");
                listDTO.add(new LegacySystemDataDTO(userData[0], userData[1], userData[2], userData[3], userData[4], userData[5], userData[6], userData[7]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listDTO;

        /*try {
            ObjectInputStream input = new ObjectInputStream(new FileInputStream(file));
            try {
                legacySysList = (ArrayList<LegacySystemDataDTO>) input.readObject();
            } finally {
                input.close();
            }
            return legacySysList;
        } catch (IOException | ClassNotFoundException e) {
            return new ArrayList<LegacySystemDataDTO>();
        }*/
    }

    /*private File removeFirstLine(File inputFile) throws IOException {
        File tempFile = new File("src/test/data/tempFile.csv");

        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

        String removeHeader = "SNSUSerNumber;VaccineName;Dose;LotNumber;ScheduledDateTime;ArrivalDateTime;NurseAdministrationDateTime;LeavingDateTime";
        String currentLine;

        while((currentLine = reader.readLine()) != null) {
            if(currentLine != removeHeader) {
                writer.write(currentLine);
            }
        }
        writer.close();
        reader.close();

        boolean successful = tempFile.renameTo(inputFile);

        return tempFile;
    }*/
}
