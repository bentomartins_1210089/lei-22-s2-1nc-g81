package app.files;

import app.domain.model.SNSUser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Sns user file.
 * @author Bento Martins <1210089>
 */
public class SNSUserFile {

    private static final String FILE_PATH = ".\\src\\main\\serialization_files\\SNSUserBinary.bin";

    /**
     * Serialize.
     *
     * @param snsUserList the sns user list
     */
    public void serialize(ArrayList<SNSUser> snsUserList) {
        try {
            FileOutputStream out = new FileOutputStream(FILE_PATH);
            ObjectOutputStream objOut = new ObjectOutputStream(out);
            objOut.writeObject(snsUserList);
            out.close();
            objOut.close();
        } catch (Exception e) {
            System.out.println("Error on serialization of SNS Users");
        }
    }

    /**
     * Load array list.
     *
     * @return the array list
     */
    public ArrayList<SNSUser> load() {
        ArrayList<SNSUser> snsUserList = new ArrayList<>();
        try {
            FileInputStream in = new FileInputStream(FILE_PATH);
            ObjectInputStream objIn = new ObjectInputStream(in);
            snsUserList = (ArrayList<SNSUser>) objIn.readObject();
            in.close();
            objIn.close();
        } catch (Exception e) {
            System.out.println("Error on deserialization of SNS Users");
        }
        return snsUserList;
    }


}
