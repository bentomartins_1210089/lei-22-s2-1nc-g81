package app.ui.console;

import app.controller.VacinationCenterController;
import app.domain.model.Employee;
import app.ui.console.utils.Utils;

import java.util.Iterator;
import java.util.List;


/**
 * The type Register vaccination center ui.
 */
public class RegisterVaccinationCenterUI implements Runnable{

    private VacinationCenterController vaccinationCenterCtrl;

    /**
     * Instantiates a new Register vaccination center ui.
     */
    public RegisterVaccinationCenterUI()
    {
        vaccinationCenterCtrl = new VacinationCenterController();
    }

    public void run()
    {
        System.out.println("\nEnter Vaccination Center information.");

        if(insertDetails())
        {
            showDetails();

            if (Utils.confirm("Do you confirm the inserted data? (y/n)")) {
                if (vaccinationCenterCtrl.saveVaccinationCenter()) {
                    System.out.println("Vaccination center created with success.");
                } else {
                    System.out.println("It was not possible to create the vaccination center.");
                }
            }
        }
        else
        {
            System.out.println("\nError. Operation cancelled.");
        }
    }

    private boolean insertDetails() {

        String coordinator = "";
        if (showCoordinator()) {
            coordinator = Utils.readLineFromConsole("Coordinator: ");
            String name              = Utils.readLineFromConsole("Name: ");
            String address           = Utils.readLineFromConsole("Address: ");
            String phone             = Utils.readLineFromConsole("Phone: ");
            String fax               = Utils.readLineFromConsole("Fax: ");
            String email             = Utils.readLineFromConsole("Email: ");
            String website           = Utils.readLineFromConsole("Website: ");
            String openingHours      = Utils.readLineFromConsole("Opening Hours: ");
            String closingHours      = Utils.readLineFromConsole("Closing Hours: ");
            String slot              = Utils.readLineFromConsole("slot: ");
            String maxNumberVaccines = Utils.readLineFromConsole("Max Number of Vaccines: ");

            return vaccinationCenterCtrl.newVaccinationCenter(name, address, phone, fax, email, website, openingHours,
                                                              closingHours, slot, maxNumberVaccines, coordinator);
        }

        return false;
    }

    private void showDetails()
    {
        System.out.println("\nNew vaccination center:\n" + vaccinationCenterCtrl.getVaccinationCenterToString());
    }

    private boolean showCoordinator(){

        System.out.println("\nCoordinators list:");

        List<Employee> employeeList = vaccinationCenterCtrl.getCoordinatorsList();
        if (!employeeList.isEmpty()) {
            Employee employee;
            Iterator var2 = employeeList.iterator();
            do {
                employee = (Employee) var2.next();
                System.out.println("id: " + employee.getEmail() + " - name: " + employee.getName());
            } while(var2.hasNext());

            return true;
        }

        System.out.println("\n[there are no coordinators created]");
        return false;
    }

}