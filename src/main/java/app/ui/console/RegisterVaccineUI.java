package app.ui.console;

import app.controller.App;
import app.controller.VaccinationBaseController;
import app.domain.model.*;
import app.dto.VaccineScheduleDto;
import app.mappers.VaccineMapper;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.time.LocalDateTime;
import java.util.List;


/**
 * @author Tassio Gomes <1170065>
 * @author Mário Borja <1200586>
 */
public class RegisterVaccineUI implements Runnable{

    private final VaccinationBaseController ctrl;
    private final AuthFacade auth;
    private final Company company;

    public RegisterVaccineUI() {
        this.company = App.getInstance().getCompany();
        this.ctrl = new VaccinationBaseController();
        this.auth = this.company.getAuthFacade();
    }

    /**
     * Prints to String Vaccination Center
     *
     * @return Vaccination Center
     */
    @Override
    public void run() {
        if(getVaccinationCenter().size() <= 0) {
            System.out.println("No vaccination Centers are currently created");
        } else {
            try {
                ScheduleVaccine();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Validate the SNS Number
     * @throws IllegalAccessException
     */
    public void ScheduleVaccine() throws IllegalAccessException {
        Integer snsNr = null;
        SNSUser snsUser;
        VaccinationCenter vacCenter;
        LocalDateTime localDateTime;
        VaccineType vaccineType;


        snsNr = Utils.readIntegerFromConsole("Insert SNS User Nr");
        snsUser = ctrl.selectClient(snsNr);
        if (snsUser == null) {
            System.out.println("Invalid SNS User Number");
        } else {
            ctrl.showClient(snsUser);
        }


        vacCenter = Utils.showAndSelectVaccinationCenter();


        vaccineType = Utils.showAndSelectVaccinationType();
        localDateTime = Utils.showAndSelectLocalDateTime();

        VaccineScheduleDto vaccineScheduleDto = new VaccineScheduleDto(snsNr, vaccineType, vacCenter,localDateTime);
        VaccineBase base = VaccineMapper.dtoToBase(vaccineScheduleDto);
        if (base != null)
            App.getInstance().getCompany().getVaccinationCenterStore().addVaccine(base);

        System.out.println("Vaccination successfully created");
        System.out.println(base);
    }

    /**
     * Get the VaccinationCenter
     * @return list of Vaccination Centers
     */
    public List<VaccinationCenter> getVaccinationCenter() {
        return this.ctrl.getVacCenters();
    }

}
