package app.ui.console;

import app.controller.SNSUserArrivalController;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Receptionist UI.
 *
 * @author Bento Martins <1210089>
 */
public class ReceptionistUI implements Runnable{

    /**
     * Sns user arrival controller
     */
    private SNSUserArrivalController ctrl;

    /**
     * Instantiates a new Receptionist UI.
     */
    public ReceptionistUI()
    {
        this.ctrl = new SNSUserArrivalController();
    }

    public void run()
    {
        boolean flag = false;
        while (!flag){
            try {
                ctrl.setVaccinationCenter(Utils.showAndSelectVaccinationCenter());
                flag = true;
            } catch (IllegalAccessException e) {}
        }

        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Register SNS User.", new RegisterSNSUserUI()));
        options.add(new MenuItem("Register the arrival of a SNS User.", new RegisterSNSUserArrivalUI()));
        options.add(new MenuItem("Register Vaccine", new RegisterVaccineUI()));

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nReceptionist Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}
