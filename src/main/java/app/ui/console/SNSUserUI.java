package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * The type SNS User UI.
 *
 * @author Bento Martins <1210089>
 */
public class SNSUserUI implements Runnable{
    /**
     * Instantiates a new SNS User UI.
     */
    public SNSUserUI()
    {
    }

    public void run()
    {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Schedule vaccine", new RegisterVaccineUI()));

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nSNS User Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}
