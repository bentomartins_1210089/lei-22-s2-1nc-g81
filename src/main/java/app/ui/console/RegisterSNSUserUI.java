package app.ui.console;

import app.controller.RegisterSNSUserController;
import app.domain.model.SNSUser;
import app.exception.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * The Register SNS User UI.
 *
 * @author Bento Martins <1210089>
 */
public class RegisterSNSUserUI implements Runnable {

    RegisterSNSUserController ctrl = new RegisterSNSUserController();

    private final String AFIRMATIVE_ANSWER = "Y";
    private final String NEGATIVE_ANSWER = "N";
    private static final String DATE_FORMAT = "dd/MM/yyyy";

    Scanner scanner = new Scanner(System.in);

    @Override
    public void run() {

        SNSUser snsuser = createSNSUser();

        if (snsuser!=null) {
            CreateClient(snsuser);
        }

    }

    private void CreateClient(SNSUser snsuser) {

        System.out.printf("\nDo you want to save the following SNS User? Yes (%s) - No (%s): \n\n", AFIRMATIVE_ANSWER, NEGATIVE_ANSWER);
        System.out.println(snsuser.toString());

        boolean validAnswer = false;

        System.out.print("\nAnswer: ");
        String answer = scanner.nextLine();

        while (!validAnswer) {
            if (answer.equals(AFIRMATIVE_ANSWER)) {
                ctrl.saveSNSUser(snsuser);
                validAnswer = true;
            }
            else if (answer.equals(NEGATIVE_ANSWER)) {
                validAnswer = true;
            }
            else {
                System.out.printf("Not a valid answer, please input Yes (%s) - No (%s): ", AFIRMATIVE_ANSWER, NEGATIVE_ANSWER);
                answer = scanner.nextLine();
            }

        }
    }

    private SNSUser createSNSUser(){

            boolean validValues = false;
            SNSUser snsuser = null;

            System.out.println("\nInsert information about the new SNS User: ");

            System.out.print("Name: ");
            String nameUser = scanner.nextLine();

            System.out.printf("Birth Date (DD/MM/YYYY): ");
            Date birthDate = insertBirthDate();

            System.out.print("SNS User Number: ");
            long snsNumber = insertNumber();

            System.out.print("Sex: ");
            String sexUser = scanner.nextLine();

            System.out.print("Citizen Card Number: ");
            long ccNumber = insertNumber();

            System.out.print("Address: ");
            String userAddress = scanner.nextLine();

            System.out.print("Phone Number: ");
            long phoneNumber = insertNumber();

            System.out.print("E-mail: ");
            String emailAddress = scanner.nextLine();

            while (!validValues) {

                try {
                    if (sexUser == "") {
                        snsuser = ctrl.createSNSUser(nameUser, birthDate, snsNumber, phoneNumber, emailAddress, ccNumber, userAddress);

                        if (snsuser == null) {
                            System.out.println("\nPlease review your SNS User data. The SNS User already exists or " +
                                    "the inserted Citizen Card number, SNS number," +
                                    " e-mail address or phone number is already being used by other SNS User.");
                        }

                        validValues=true;
                    } else {
                        snsuser = ctrl.createSNSUser(nameUser, birthDate, snsNumber, phoneNumber, emailAddress, ccNumber, userAddress, sexUser);

                        if (snsuser == null) {
                            System.out.println("\nPlease review your SNS User data. The SNS User already exists or " +
                                    "the inserted Citizen Card number, SNS number," +
                                    " e-mail address or phone number is already being used by other SNS User.");
                        }
                        validValues=true;
                    }

                } catch(InvalidNameException ex){
                    System.out.print("Name: ");
                    nameUser = scanner.nextLine();
                } catch(InvalidEmailAddressException ex){
                    System.out.print("E-mail: ");
                    emailAddress = scanner.nextLine();
                } catch(InvalidBirthDateException ex) {
                    System.out.printf("Birth Date (DD/MM/YYYY): ");
                    birthDate = insertBirthDate();
                } catch(InvalidPhoneNumberException ex){
                    System.out.print("Phone Number: ");
                    phoneNumber = insertNumber();
                } catch(InvalidSnsNumberException ex){
                    System.out.print("SNS Number: ");
                    snsNumber = insertNumber();
                } catch(InvalidCcNumberException ex){
                    System.out.print("Citizen Card Number: ");
                    ccNumber = insertNumber();
                } catch(InvalidAddressException ex){
                    System.out.print("Address: ");
                    userAddress = scanner.nextLine();
                } catch(InvalidSexException ex){
                    System.out.print("Sex: ");
                    sexUser = scanner.nextLine();
                }
            }

        return snsuser;
    }

    private Date insertBirthDate() {
        boolean validDate = false;
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setLenient(false);
        Date birthDate = null;

        while (!validDate) {
            try {
                String stringDate = scanner.nextLine();
                birthDate = formatter.parse(stringDate);

                validDate = true;

            } catch (ParseException ex) {
                System.out.println("The date format is not valid. Please, insert a valid date.");
                System.out.printf("Birth Date (DD/MM/YYYY): ");
            } catch (InvalidBirthDateException ex) {
                System.out.printf("Birth Date (DD/MM/YYYY): ");
            }
        }

        return birthDate;
    }

    private long insertNumber() {
        boolean isValid = false;
        String tester = null;
        long intValue = 0;

        while (!isValid) {
            try {
                tester = scanner.nextLine();
                intValue = Long.parseLong(tester);
                isValid = true;

            } catch (IllegalArgumentException e) {
                System.out.print("The input data is not valid. Please, insert a number: ");
            }
        }
        return intValue;
    }
}



