package app.ui.console;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * JavaFX FileChooser for importing files
 *
 * @author Tiago Oliveira <1211669>
 */

public class FileChooserUI {

    private FileChooser fileChooser;

    /**
     * Empty constructor instantiates a new File Chooser UI
     * with default extension filter attributes
     */
    private FileChooserUI() {
        fileChooser = new FileChooser();
        filter("", "*.csv");
    }

    /**
     * Constructor instantiates a new File Chooser UI
     * with filter extension filter attribute
     */
    private FileChooserUI(String extension) {
        fileChooser = new FileChooser();
        filter("", extension);
    }

    /**
     * Constructor instantiates a new File Chooser UI
     * with filter extension and description attributes
     */
    private FileChooserUI(String description, String extension) {
        fileChooser = new FileChooser();
        filter(description, extension);
    }

    /**
     * Creates a new File Chooser
     * @return FileChooser
     */
    public static FileChooser fileChoose() {
        FileChooserUI fc = new FileChooserUI();
        return fc.fileChooser;
    }

    /**
     * Creates a new File Chooser
     * @param extension File chooser extension
     * @return FileChooser
     */
    public static FileChooser fileChoose(String extension) {
        FileChooserUI fc = new FileChooserUI(extension);
        return fc.fileChooser;
    }

    /**
     * Creates a new File Chooser
     * @param description File chooser description
     * @param extension File chooser extension
     * @return FileChooser
     */
    public static FileChooser fileChoose(String description, String extension) {
        FileChooserUI fc = new FileChooserUI(description, extension);
        return fc.fileChooser;
    }


    /**
     * Adds a new filter to the file chooser
     * @param description File chooser description
     * @param extension File chooser extension
     */
    private void filter(String description, String extension) {
        ExtensionFilter filter = new ExtensionFilter(description, extension);
        fileChooser.getExtensionFilters().add(filter);
    }
}