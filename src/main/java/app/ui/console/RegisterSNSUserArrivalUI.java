package app.ui.console;

import app.controller.App;
import app.controller.SNSUserArrivalController;
import app.controller.VacinationCenterController;
import app.domain.model.SNSUserArrival;
import app.domain.model.VaccinationCenter;
import app.domain.model.Validator;
import app.domain.store.WaitingRoomStore;
import app.exception.InvalidSnsNumberException;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Tiago Oliveira <1211669>
 */

public class RegisterSNSUserArrivalUI implements Runnable {

    /**
     * Sns user arrival controller
     */
    private SNSUserArrivalController ctrl;

    /**
     * Waiting room store
     */
    private WaitingRoomStore str;

    public static VaccinationCenter vaccinationCenter;

    /**
     * Vaccination center controller
     */
    private VacinationCenterController vcCtrl;

    /**
     * Validator
     */
    private Validator validator;

    /**
     * Contructor instantiates an sns user arrival
     */
    public RegisterSNSUserArrivalUI()
    {
        ctrl = new SNSUserArrivalController();
        str = App.getInstance().getCompany().getWaitingRoomStore();
        vcCtrl = new VacinationCenterController();
        validator = new Validator();
        vaccinationCenter = ctrl.getVaccinationCenter();
    }

    @Override
    public void run() {
        boolean running;
        do {
            boolean valid = false;
            boolean validScheduled;
            boolean notDuplicate;
            long snsNumber = 0;
            DateTimeFormatter fmtTime, fmtDate;
            String time, date;
            Email vcCenterEmail;

            vcCenterEmail = null;//vaccinationCenter.getEmail();

            snsNumber = readSNSNumber();

            fmtDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            fmtTime = DateTimeFormatter.ofPattern("HH:mm");
            date = fmtDate.format(LocalDateTime.now().toLocalDate());
            time = fmtTime.format(LocalDateTime.now().toLocalTime());

            while (!valid) {
                try {
                    SNSUserArrival regUserArrival = ctrl.createUserArrival(snsNumber, time, date);

                    validScheduled = ctrl.validateScheduledVaccination(snsNumber, date);
                    notDuplicate = ctrl.validateDuplicateUserArrival(regUserArrival);

                    System.out.println("\n" + regUserArrival.toString());

                    if (Utils.confirm("Confirm the previous SNS user to enter the Waiting Room " +
                            "for vaccine administration? (y/n)")) {
                        if (validScheduled && notDuplicate) {
                            try{
                                if (vcCtrl.addSNSUserArrivalToWaitingRoom(vcCenterEmail, regUserArrival)) {
                                    if (vcCtrl.addWaitingRoomToVaccinationCenter(vcCenterEmail)) {
                                        System.out.println("\nSuccessfully stored!");
                                    } else {
                                        System.err.println("\nError: Could not add a new waiting room");
                                    }
                                } else {
                                    System.err.println("\nError: Could not add a new SNS user arrival");
                                }
                            } catch (IllegalArgumentException e) {
                                System.err.println("An error occured saving the data, please try again.");
                            }
                        } else if (!validScheduled) {
                            System.err.println("\nThe SNS user is not scheduled for the current date and/ or vaccination center.");
                        } else if (!notDuplicate) {
                            System.err.println("\nThe SNS user is already registered in the waiting room.");
                        }
                    } else {
                        System.out.println("\nOperation canceled.");
                    }
                    valid = true;
                } catch (Exception e) {
                    System.err.println("An error occured, please try again");
                }
            }
            running = Utils.confirm("Want to add another SNS user to the Waiting Room? (y/n)");
        } while (running);
    }

    /**
     * Reads a long number
     * @return sns user number
     */
    private long readSNSNumber() {
        boolean valid = false;
        long snsNumber = 0;

        while (!valid) {
            try {
                snsNumber = Long.parseLong(String.valueOf(Utils.readIntegerFromConsole("Insert the SNS User number: ")));

                if (String.valueOf((validator.validateSnsNumber(snsNumber))).equals(String.valueOf(snsNumber))) {
                    valid = true;
                }
            } catch (InvalidSnsNumberException e) {}
        }
        return snsNumber;
    }
}


