package app.ui.console;

import app.controller.VaccineAdministrationController;
import app.domain.model.VaccineAdministration;
import app.domain.model.Validator;
import app.domain.store.VaccineAdministrationStore;
import app.exception.InvalidAgeException;
import app.exception.InvalidDosagesException;
import app.exception.InvalidDosingIntervalsException;
import app.exception.InvalidNumberDosesException;
import app.ui.console.utils.Utils;

import java.util.ArrayList;

/**
 * @author Tiago Oliveira <1211669>
 */

public class VaccineAdministrationUI implements Runnable {

    /**
     * Admin process controller ctrl
     */
    private VaccineAdministrationController ctrl;

    /**
     * Admin process store str
     */
    private VaccineAdministrationStore str;

    /**
     * Validator
     */
    private Validator validator;

    /**
     * Contructor instantiates an admin process UI
     */
    public VaccineAdministrationUI()
    {
        this.ctrl = new VaccineAdministrationController();
        this.str = new VaccineAdministrationStore();
        this.validator = new Validator();
    }

    @Override
    public void run() {
        boolean running = true;
        do {
            boolean valid = false;
            boolean flag = false;
            boolean notDuplicate;
            String vaccine;
            String ageGroup = VaccineAdministration.DEFAULT_AGE_GROUP;
            int numDoses = VaccineAdministration.DEFAULT_NUMBER_DOSES;
            ArrayList<Double> dosages = VaccineAdministration.DEFAULT_DOSAGES;
            ArrayList<Integer> dosingIntervals = VaccineAdministration.DEFAULT_DOSING_INTERVALS;
            dosages.clear();
            dosingIntervals.clear();

            new VaccineChooseUI().run();
            vaccine = ctrl.vaccine;

            do {
                try {
                    ageGroup = readAge();
                    flag = true;
                } catch (InvalidAgeException e) {}
            } while (!flag);
            flag = false;

            do {
                try {
                    numDoses = readNumDoses();
                    flag = true;
                } catch (InvalidNumberDosesException e) {}
            } while (!flag);
            flag = false;

            insertArrayData(dosages, dosingIntervals, numDoses);
            do {
                try {
                    validator.validDosages(dosages);
                    validator.validIntervals(dosingIntervals);
                    flag = true;
                } catch (InvalidDosagesException e) {
                    updateDosages(dosages);
                } catch (InvalidDosingIntervalsException e) {
                    updateIntervals(dosingIntervals);
                }
            } while (!flag);
            flag = false;

            while (!valid) {
                try {
                    VaccineAdministration adminProcessData = ctrl.createVaccineAdmin(String.valueOf(vaccine), ageGroup, numDoses, dosages, dosingIntervals);
                    notDuplicate = ctrl.validateVaccineAdmin(adminProcessData);
                    System.out.println("\n" + adminProcessData.toString());

                    if (Utils.confirm("Want to confirm the previous administration process data? (y/n)")) {
                        if (notDuplicate) {
                            try{
                                str.addAdminProcess(adminProcessData);
                                System.out.println("\nSuccessfully stored!");
                                valid = true;
                            } catch (IllegalArgumentException e) {
                                System.err.println("An error occured saving the data, please try again.");
                                valid = true;
                            }
                        } else {
                            System.err.println("This vaccine administration process already exists.");
                            valid = true;
                        }
                    } else {
                        System.out.println("\nOperation canceled.");
                        valid = true;
                    }
                } catch (IllegalArgumentException e) {
                    System.err.println("An error occured, please try again");
                }
            }
            running = Utils.confirm("Want to add another vaccine administration process? (y/n)");
        } while (running);
    }

    /**
     * Reads the input age
     * @return age group interval
     */
    public String readAge() {
        String ageGroup = VaccineAdministration.DEFAULT_AGE_GROUP;
        boolean validAge = false;

        while (!validAge) {
            int age = 0;

            try {
                age = (Utils.readIntegerFromConsole("Insert the age:"));
            } catch (NumberFormatException e) {
                System.err.println("\nInput data is not valid");
            }

            if (validator.validateAgeGroup(age) != null)
                validAge = true;
        }
        return ageGroup;
    }

    /**
     * Reads the input number of doses
     * @return number of doses
     */
    public int readNumDoses() {
        int numDoses = VaccineAdministration.DEFAULT_NUMBER_DOSES;
        boolean validDose = false;

        while (!validDose) {
            try {
                numDoses = (Utils.readIntegerFromConsole("Insert the number of doses:"));
            } catch (NumberFormatException e) {
                System.err.println("\nInput data is not valid");
            }

            if (validator.validateNumDoses(numDoses))
                validDose = true;
        }
        return numDoses;
    }

    /**
     * Reads the input values for each list
     *
     * @param dosages dosages (mL)
     * @param dosingIntervals recovery intervals for each dose (days)
     * @param numDoses number of doses
     */
    public void insertArrayData(ArrayList<Double> dosages, ArrayList<Integer> dosingIntervals, int numDoses) {
        boolean validInterval;
        int count = 0;

        while (count < numDoses) {
            try {
                dosages.add((Utils.readDoubleFromConsole("Insert dosage (mL) for the number "
                        + (count + 1) + " dose:")));

                validInterval = false;
                if (numDoses >= 2 && count < numDoses - 1) {
                    while (!validInterval) {
                        try {
                            dosingIntervals.add((Utils.readIntegerFromConsole(
                                    "Insert the recovery time (days) for the number "
                                            + (count + 1) +" dose:")));
                            validInterval = true;
                        } catch (NumberFormatException e) {
                            System.err.println("\nInput data is not valid");
                        }
                    }
                }
                count++;
            } catch (NumberFormatException e) {
                System.err.println("\nInput data is not valid");
            }
        }
    }

    /**
     * Replaces wrong values with new ones
     *
     * @param dosages dosages (mL)
     * @return new dosages list
     */
    public ArrayList<Double> updateDosages (ArrayList<Double> dosages) {
        int index = 0;
        double value = 0;

        for (int i = 0; i < dosages.size(); i++) {
            value = dosages.get(i);

            if (value <= 0) {
                dosages.remove(i);
                try {
                    dosages.add(i, (Utils.readDoubleFromConsole(
                            "Enter dosage (mL) for the number "
                                    + (index + 1) + " dose:")));
                } catch (NumberFormatException e) {
                    System.err.println("\nInput data is not valid");
                }
            }
        }
        return dosages;
    }

    /**
     * Replaces wrong values for new ones
     *
     * @param intervals recovery intervals for each dose (days)
     * @return new intervals list
     */
    public ArrayList<Integer> updateIntervals (ArrayList<Integer> intervals) {
        int index = 0, value = 0;

        for (int i = 0; i < intervals.size(); i++) {
            value = intervals.get(i);

            if (value <= 0) {
                intervals.remove(i);
                try {
                    intervals.add(i, (Utils.readIntegerFromConsole(
                            "Insert the recovery time (days) for the number "
                                    + (i + 1) + " dose:")));
                } catch (NumberFormatException e) {
                    System.err.println("\nInput data is not valid");
                }
            }
        }
        return intervals;
    }
}


