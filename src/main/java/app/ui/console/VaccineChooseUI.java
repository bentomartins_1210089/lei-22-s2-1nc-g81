package app.ui.console;

import app.controller.App;
import app.controller.VaccineAdministrationController;
import app.domain.model.Company;
import app.domain.model.VaccineType;
import app.domain.store.VaccineTypeStore;
import app.ui.console.utils.Utils;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tiago Oliveira <1211669>
 */

public class VaccineChooseUI implements Runnable {

    private Company company;
    /**
     * Admin process controller ctrl
     */
    private VaccineAdministrationController ctrl;

    /**
     * Arraylist vaccineTypeList
     */
    private static VaccineTypeStore vcTypeStr;

    /**
     * Contructor instantiates an choose vaccine UI
     */
    //Waiting for US12 Sprint B to be finished...
    public VaccineChooseUI()
    {
        this.company = App.getInstance().getCompany();
        this.ctrl = new VaccineAdministrationController();
        vcTypeStr = company.getVaccineTypeStore();
    }

    //Menu with vaccine types stored in VaccineTypeStore for Administrator to choose from
    @Override
    public void run() {
        List<MenuItem> options = new ArrayList<>();

        List<VaccineType> vaccineTypeList = vcTypeStr.getVcTypeList();

        for (int i = 0; i < vaccineTypeList.size(); i++) {
            options.add(new MenuItem(String.valueOf(vaccineTypeList.get(i)), new ShowTextUI("You have selected option "
                    + (i + 1))));
        }

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nPlease select the Vaccine:");

            if ( (option >= 0) && (option < options.size()))
            {
                String[] vcTypeParts = String.valueOf(vaccineTypeList.get(option)).split(" ");
                ctrl.vaccine = vcTypeParts[3];
                break;
            }
        }
        while (option != -1 );
    }
}
