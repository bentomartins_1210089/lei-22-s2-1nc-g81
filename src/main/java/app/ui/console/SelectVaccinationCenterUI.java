package app.ui.console;

import app.controller.App;
import app.controller.VacinationCenterController;
import app.domain.model.VaccinationCenter;
import app.domain.store.WaitingRoomStore;
import app.dto.SNSUserArrivalDTO;
import app.ui.console.utils.Utils;

import java.util.List;

public class SelectVaccinationCenterUI implements Runnable {

    private List<VaccinationCenter> vaccinationCenterList;
    private VaccinationCenter vaccinationCenter;

    VacinationCenterController vaccinationCenterCtrl;
    WaitingRoomStore waitingRoomStore;

    public SelectVaccinationCenterUI()
    {
        vaccinationCenterCtrl = new VacinationCenterController();
        vaccinationCenterList = App.getInstance().getCompany().getVaccinationCenterStore().getAllVacCenters();
        waitingRoomStore = App.getInstance().getCompany().getWaitingRoomStore();
    }

    public void run()
    {
        showVaccinationCenters();
    }


    private boolean showVaccinationCenters(){
        System.out.println("\nConsult the users in the waiting room:");
        if (!vaccinationCenterList.isEmpty()) {
            int option = Utils.showAndSelectIndex(vaccinationCenterList, "Select your vaccination center:");
            if (option > -1 && option < vaccinationCenterList.size()) {
                VaccinationCenter vaccinationCenter = vaccinationCenterList.get(option);
                vaccinationCenterCtrl.addWaitingRoomToVaccinationCenter(vaccinationCenter.getEmail());
                List<SNSUserArrivalDTO> snsUserArrivalDTOList = vaccinationCenterCtrl.consultUsersInTheWaitingRoom(vaccinationCenter);
                System.out.println(snsUserArrivalDTOList);
            }else {
                System.out.println("Invalid Option");
            }
            return true;
        }

        System.out.println("\n[there are no vaccination centers created]");
        return false;
    }
}
