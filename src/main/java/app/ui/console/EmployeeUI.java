package app.ui.console;

import app.controller.EmployeeRegisterController;
import app.domain.model.Employee;
import app.ui.console.utils.Utils;

public class EmployeeUI implements Runnable {

    @Override
    public void run() {
        boolean response;
        boolean flag = false;
        Employee employee = null;

        boolean created1 = false;
        boolean created2 = false;
        String organizationRole = "";
        String name = "";
        String phoneNumber = "";
        String email = "";
        String cc = "";
        String address = "";

        do {
            EmployeeRegisterController e = new EmployeeRegisterController();
            //List<Employee> employeeList = e.getAllEmployees();
            System.out.println("Employee registration");

            do {
                name = Utils.readLineFromConsole("Name: ");
                try {
                    e.nameValid(name);
                    flag = true;
                } catch (IllegalArgumentException exception) {
                    System.out.printf("%s\nPlease insert a valid name.\n", exception.getMessage());
                }
            } while (!flag);
            flag = false;

            do {
                address = Utils.readLineFromConsole("Address: ");
                try {
                    e.addressValid(address);
                    flag = true;
                } catch (IllegalArgumentException exception) {
                    System.out.printf("%s\nPlease insert a valid address.\n", exception.getMessage());
                }
            } while (!flag);
            flag = false;

            do {
                phoneNumber = Utils.readLineFromConsole("Phone Number: ");
                try {
                    e.phoneNumberValid(phoneNumber);
                    flag = true;
                } catch (IllegalArgumentException exception) {
                    System.out.printf("%s\nPlease insert a valid phone number.\n", exception.getMessage());
                }
            } while (!flag);
            flag = false;

            do {
                email = Utils.readLineFromConsole("Email: ");
                try {
                    e.emailValid(email);
                    flag = true;
                } catch (IllegalArgumentException exception) {
                    System.out.printf("%s\nPlease insert a valid email.\n", exception.getMessage());
                }
            } while (!flag);
            flag = false;

            do {
                cc = Utils.readLineFromConsole("CC number: ");
                try {
                    e.ccValid(cc);
                    flag = true;
                } catch (IllegalArgumentException exception) {
                    System.out.printf("%s\nPlease insert a valid cc.\n", exception.getMessage());
                }
            } while (!flag);
            flag = false;

            do {
                organizationRole = Utils.readLineFromConsole("Role: ");
                try {
                    e.organizationRoleValid(organizationRole);
                    flag = true;
                } catch (IllegalArgumentException exception) {
                    System.out.printf("%s\nPlease insert a valid cc.\n", exception.getMessage());
                }
            } while (!flag);
            flag = false;

            //New User
            employee = new Employee(name, address, phoneNumber, email, cc, organizationRole);
            created1 = e.validateEmployee(employee);

            System.out.println(employee.toString());
            if (Utils.confirm("Want to confirm employee's data? (y/n)")) {
                if (created1) {
                    e.registerEmployee(phoneNumber, address, organizationRole, name, cc, email);
                    System.out.println("Employee saved!");
                    // Utils.showList(employeeList, "Registered employees:");
                }
            }
            response = Utils.confirm("Do you want to register another employee? (y/n)");
        } while (response);

    }
}



