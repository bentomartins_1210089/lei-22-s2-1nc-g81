package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class AdminUI implements Runnable {
    public AdminUI()
    {
    }

    public void run()
    {
        List<MenuItem> options = new ArrayList<>();
        options.add(new MenuItem("Register Employee", new EmployeeUI()));
        options.add(new MenuItem("Search Employees by function", new SearchEmployeeUI()));
        options.add(new MenuItem("Vaccine Type Menu", new VaccineTypeMenuUI()));
        options.add(new MenuItem("Create New Administration Process", new VaccineAdministrationUI()));
        options.add(new MenuItem("Load csv file with SNS Users", new LoadCsvSNSUsersUI()));
        options.add(new MenuItem("Vaccination Center Options", new VaccinationCenterUI()));
        options.add(new MenuItem("Schedule vaccine", new RegisterVaccineUI()));
        options.add(new MenuItem("Nurse Menu", new RegisterVaccineUI()));
//        options.add(new MenuItem("Go to GUI", new RegisterVaccineUI()));
        //options.add(new MenuItem("Vaccinated Users", new Scheduler()));

        int option;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nAdmin Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}
