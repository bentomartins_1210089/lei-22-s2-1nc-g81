package app.ui.console;

import java.util.Arrays;
import java.util.List;


import app.controller.EmployeeSearchController;
import app.domain.shared.Enums.EmployeeRole;
import app.ui.console.utils.Utils;

/**
 * The type SNS User UI.
 *
 * @author Tassio Gomes <1170065>
 */

public class SearchEmployeeUI implements Runnable {

    /**
     * Instantiates a Search Employee by Role.
     */

    @Override
    public void run() {

        EmployeeSearchController searchController = new EmployeeSearchController();

        List<EmployeeRole> rolesList = Arrays.asList(EmployeeRole.values());

        int option = Utils.showAndSelectIndex(rolesList, "Select employee role:");
        if (option > -1 && option < rolesList.size()) {

            System.out.println(rolesList.get(option).value());
            Utils.showList(searchController.getListEmployeesByRole(rolesList.get(option).value()), "Search result:");


        }else {
            System.out.println("Invalid Option");

        }
    }

}