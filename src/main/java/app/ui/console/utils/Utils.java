package app.ui.console.utils;

import app.controller.App;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineType;
import app.domain.model.Validator;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Utils {

    
    static public String readLineFromConsole(String prompt) {
        try {
            System.out.println("\n" + prompt);

            InputStreamReader converter = new InputStreamReader(System.in);
            BufferedReader in = new BufferedReader(converter);

            return in.readLine();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public int readIntegerFromConsole(String prompt) {
        do {
            try {
                String input = readLineFromConsole(prompt);

                int value = Integer.parseInt(input);

                return value;
            } catch (NumberFormatException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }

    static public double readDoubleFromConsole(String prompt) {
        do {
            try {
                String input = readLineFromConsole(prompt);

                double value = Double.parseDouble(input);

                return value;
            } catch (NumberFormatException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }

    static public Date readDateFromConsole(String prompt) {
        do {
            try {
                String strDate = readLineFromConsole(prompt);

                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

                Date date = df.parse(strDate);

                return date;
            } catch (ParseException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }

    static public boolean confirm(String message) {
        String input;
        do {
            input = Utils.readLineFromConsole("\n" + message + "\n");
        } while (!input.equalsIgnoreCase("y") && !input.equalsIgnoreCase("n"));

        return input.equalsIgnoreCase("y");
    }

    static public Object showAndSelectOne(List list, String header) {
        showList(list, header);
        return selectsObject(list);
    }

    static public int showAndSelectIndex(List list, String header) {
        showList(list, header);
        return selectsIndex(list);
    }

    static public void showList(List list, String header) {
        System.out.println(header);

        int index = 0;
        for (Object o : list) {
            index++;

            System.out.println(index + ". " + o.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancel");
    }

    static public Object selectsObject(List list) {
        String input;
        Integer value;
        do {
            input = Utils.readLineFromConsole("Type your option: ");
            value = Integer.valueOf(input);
        } while (value < 0 || value > list.size());

        if (value == 0) {
            return null;
        } else {
            return list.get(value - 1);
        }
    }

    static public int selectsIndex(List list) {
        String input;
        Integer value;
        do {
            input = Utils.readLineFromConsole("Type your option: ");
            value = Integer.valueOf(input);
        } while (value < 0 || value > list.size());

        return value - 1;
    }

    public static VaccinationCenter showAndSelectVaccinationCenter() throws IllegalAccessException {

        //Choose Vaccination Center

        List<VaccinationCenter> vaccinationCenterList = App.getInstance().getCompany().getVaccinationCenterStore().getAllVacCenters();


        if (vaccinationCenterList.size() != 0) {

            Object o = Utils.showAndSelectOption(vaccinationCenterList, "\n\nVaccination Centers: ");

            return (VaccinationCenter) o;

        } else {

            throw new IllegalAccessException("Empty List of Vaccination Centers");

        }

    }

    static public Object showAndSelectOption(List list, String message) {

        int i = 1;

        System.out.println(message);


        for (Object p : list) {
            if (p instanceof VaccineType)

                System.out.printf("%d: %s\n", i, ((VaccineType) p).getVaccineTypeCode());


            else if (p instanceof VaccinationCenter) {

                System.out.printf("%d: %s\n", i, ((VaccinationCenter) p).getName());

            }


            i++;

        }

        return selectsObject(list);

    }

    public static VaccineType showAndSelectVaccinationType() {
        Object o;
        boolean error;
        do {
            List<VaccineType> vaccineTypeList = App.getInstance().getCompany().getVaccineTypeStore().getVcTypeList();
            o = Utils.showAndSelectOption(vaccineTypeList, "\n\nVaccine type: ");
            if (o == null)
                error = true;
            else
                error = false;
        } while (error);
        return (VaccineType) o;
    }

    public static LocalDateTime showAndSelectLocalDateTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        boolean error;
        String strDate;
        LocalDateTime date = null;
        do {
            try {
                strDate = Utils.readLineFromConsole("\n\nDate (dd/mm/yyyy HH:mm): ");
                error = false;
                date = LocalDateTime.parse(strDate, formatter);
            } catch (Exception e) {
                System.out.println("Invalidate date format");
                error = true;
            }
        } while (error);
        return date;
    }
    
    public static String readLotNumber(String number) {
        Validator validator = new Validator();
        String lotNumber = null;
        boolean check = false;
        do {
            try {
                lotNumber = Utils.readLineFromConsole(number);
                assert lotNumber != null;
                check = validator.checkLotNumber(lotNumber);
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        } while (!check);
        return lotNumber;
    }
}
