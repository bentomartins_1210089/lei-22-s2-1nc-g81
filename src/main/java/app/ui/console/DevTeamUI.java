package app.ui.console;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DevTeamUI implements Runnable{

    public DevTeamUI()
    {

    }
    public void run()
    {
        System.out.println("\n");
        System.out.printf("Development Team:\n");
        System.out.printf("\t Bento Martins - 1210089@isep.ipp.pt \n");
        System.out.printf("\t Delfim Costa - 1081348@isep.ipp.pt \n");
        System.out.printf("\t Mário Borja - 1200586@isep.ipp.pt \n");
        System.out.printf("\t Sérgio Soares - 1980292@isep.ipp.pt \n");
        System.out.printf("\t Tássio Gomes - 1170065@isep.ipp.pt \n");
        System.out.printf("\t Tiago Oliveira - 1211669@isep.ipp.pt \n");
        System.out.println("\n");
    }
}
