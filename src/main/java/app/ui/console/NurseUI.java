package app.ui.console;

import app.domain.model.VaccinationCenter;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class    NurseUI implements Runnable{
    public NurseUI()
    {
    }
    private VaccinationCenter vaccinationCenter;

    public NurseUI(VaccinationCenter vaccinationCenter) {
        this.vaccinationCenter = vaccinationCenter;
    }

    public void run()
    {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Consult the users in the waiting room.", new SelectVaccinationCenterUI()));
        options.add(new MenuItem("Register vaccine administration", new RegisterVaccineAdministrationNurseUI(vaccinationCenter))); //Mário Borja 1200586


        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nNurse Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}
