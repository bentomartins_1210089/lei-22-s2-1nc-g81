
package app.ui.console;
import java.util.Scanner;

import app.controller.VaccineTypeController;
import app.domain.model.VaccineType;
import app.domain.shared.Enums.VaccineTypeTechnology;
import app.exception.InvalidVaccineTypeCodeException;
import app.exception.InvalidVaccineTypeDescException;
import app.exception.InvalidVaccineTypeTechException;

/**
 * US12
 * Vaccine Type UI
 *
 * @author Delfim Costa <1081348>
 */

public class VaccineTypeUI implements Runnable {

    VaccineTypeController ctrl = new VaccineTypeController();

    /**
    private VaccineTypeController ctrl;

    public VaccineTypeUI() {
        ctrl = new VaccineTypeController();
    }
    */

    Scanner scanner = new Scanner(System.in);


    @Override
    public void run() {

        VaccineType vcType = null;

        boolean validValues = false;

        System.out.println("Create New Vaccine Type\n");

        System.out.println("Vaccine Name: ");
        String vcName = scanner.nextLine();

        System.out.println("Select a Vaccine Brand: ");
        String vcBrand = selectVaccineTechnology();

        System.out.println("Vaccine Type Code: ");
        String vcTCode = scanner.nextLine();

        System.out.println("Vaccine Type Descritpion: ");
        String vcTDesc = scanner.nextLine();

        System.out.println("Select a Vaccine Type Technology: ");
        String vcTTech = selectVaccineTechnology();

        while (!validValues) {

            try {
                if (vcTDesc == "") {

                    vcType = ctrl.createVaccineType(vcTCode, vcTTech.toUpperCase(), vcName, vcBrand);
                    System.out.println(vcType.toString());
                    validValues = true;

                } else {

                    vcType = ctrl.createVaccineType(vcTCode, vcTDesc, vcTTech.toUpperCase(), vcName, vcBrand);
                    System.out.println(vcType.toString());
                    validValues = true;
                }
            } catch(InvalidVaccineTypeCodeException ex) {
                System.out.println("Vaccine Type Code: ");
                vcTCode = scanner.nextLine();
            } catch(InvalidVaccineTypeDescException ex) {
                System.out.println("Vaccine Type Descritpion: ");
                vcTDesc = scanner.nextLine();
            }
        }

    }

    private String selectVaccineTechnology() {

        boolean isValid = false;
        String vctTemp = null;
        String vctTempDesc = null;

        System.out.println("\nVaccine Type Technology List:\n ");

        for (VaccineTypeTechnology vTechChoices : VaccineTypeTechnology.values()) {
            System.out.println(vTechChoices.name() + " - " + vTechChoices.getVaccineTypeTechnology());
        }

        while (!isValid) {

            try {
                System.out.println("Insert Vaccine Type Technology Code: ");
                vctTemp = scanner.nextLine();

                for (VaccineTypeTechnology vTechChoices : VaccineTypeTechnology.values()) {

                    if (vTechChoices.name().equals(vctTemp.toUpperCase())) {

                        vctTempDesc = vTechChoices.valueOf((vctTemp).toUpperCase()).getVaccineTypeTechnology();
                        isValid = true;
                    }
                }
                } catch (InvalidVaccineTypeTechException ex){}
        }
            return vctTempDesc;
    }

} // End class




