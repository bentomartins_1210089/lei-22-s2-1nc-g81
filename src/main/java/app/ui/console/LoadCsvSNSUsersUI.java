package app.ui.console;
import app.controller.LoadCsvSNSUsersController;
import app.dto.SNSUserDto;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

/**
 * The Load CSV with SNS Users UI.
 *
 * @author Bento Martins <1210089>
 */
public class LoadCsvSNSUsersUI implements Runnable {

    /**
     * Starts Scanner.
     */
    Scanner scanner = new Scanner(System.in);

    /**
     * Initiates Class LoadCsvSNSUsersController.
     */
    LoadCsvSNSUsersController ctrl = new LoadCsvSNSUsersController();

    public void run() {

        System.out.print("\nPlease input CSV file path:\n");

        Boolean fileExists = false;
        String answer = null;

        while (!fileExists) {
            answer = scanner.nextLine();
            fileExists=ctrl.CsvFileExists(answer);
        }

        try {
            List<SNSUserDto> listDtoFinal = null;
            listDtoFinal = ctrl.readCsv(answer);

            if (listDtoFinal==null) {
                return;
            }

            int usersToAdd = listDtoFinal.size();

            if (usersToAdd>0) {
                System.out.println("\n"+usersToAdd + " SNS Users available to load. Please confirm? Yes (Y) or No (N)");
                String confirm = scanner.nextLine();

                while (!(confirm.equals("Y") || confirm.equals("N")))
                {
                    System.out.println("Please write Y or N.");
                    confirm = scanner.nextLine();
                }

                if (confirm.equals("Y")){

                    List<SNSUserDto> listDtoAdded = ctrl.addSNSUsersDtoToStore(listDtoFinal);

                    System.out.println("\nUsers added:");
                    System.out.println("\nName | E-mail | Password");

                    for (SNSUserDto snsUserDto : listDtoAdded) {

                        System.out.println(snsUserDto.getName()+" | "+snsUserDto.getEmail()+" | "+snsUserDto.getPassword());

                    }
                }

                if (confirm.equals("N")) {

                    new AdminUI();

                }

            }

            if (usersToAdd ==0) {
                System.out.println("\nNo SNS Users available to load.");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
