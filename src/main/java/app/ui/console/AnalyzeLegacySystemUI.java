package app.ui.console;

import app.controller.LegacySystemDataController;

/**
 * @author Tiago Oliveira <1211669>
 */

public class AnalyzeLegacySystemUI implements Runnable {

    /**
     * Legacy system data controller
     */
    private LegacySystemDataController ctrl;

    /**
     * Constructor instantiates a Analyze Legacy System UI
     */
    public AnalyzeLegacySystemUI() {
        this.ctrl = new LegacySystemDataController();
    }

    @Override
    public void run() {
        System.out.println("\nLegacy System Data:");
        for (int i = 0; i < ctrl.getLegacySystem().getLegacySystemStore().size(); i++) {
            System.out.println(ctrl.getLegacySystem().getLegacySystemStore().get(i));
        }
    }
}
