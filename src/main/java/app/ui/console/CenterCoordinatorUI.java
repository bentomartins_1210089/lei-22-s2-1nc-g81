package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class CenterCoordinatorUI implements Runnable{
    public CenterCoordinatorUI()
    {
    }

    public void run()
    {

        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Get list of all vaccines (not implemented)", new CenterCoordinatorUI()));
        options.add(new MenuItem("Check and export vaccination statistics", new SelectVaccinationCenterUI()));

        int option = 2;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nCenter Coordinator Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}
