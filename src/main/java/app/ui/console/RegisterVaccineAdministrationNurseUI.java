package app.ui.console;

import app.controller.App;
import app.controller.RegisterVaccineAdministrationController;
import app.controller.VaccinationBaseController;
import app.controller.VacinationCenterController;
import app.domain.model.*;
import app.domain.store.SNSUserStore;
import app.domain.store.VaccinationCenterStore;
import app.dto.SNSUserArrivalDTO;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Mario Borja 1200586
 */
public class RegisterVaccineAdministrationNurseUI implements Runnable{
    private Company company;
    private RegisterVaccineAdministrationController controller;
    private VaccinationCenter vaccinationCenter;
    private VacinationCenterController vacCenterController;
    private SNSUserStore snsUserStore;
    private VaccineBase vaccineBase;
    private VaccinationCenterStore vaccinationCenterStore;


    /**
     * Instantiates a new Register vaccine administration nurse ui.
     */
    public RegisterVaccineAdministrationNurseUI() {
        this.company = App.getInstance().getCompany();
        this.controller = new RegisterVaccineAdministrationController();
    }

    /**
     * Instantiates a new Register vaccine administration nurse ui.
     *
     * @param vaccinationCenter the vaccination center
     */
    public RegisterVaccineAdministrationNurseUI(VaccinationCenter vaccinationCenter) {
        controller = new RegisterVaccineAdministrationController();
        this.vaccinationCenter = vaccinationCenter;
    }

    public void run() {
        snsUserStore = App.getInstance().getCompany().getSNSUserStore();
        vaccineBase = new VaccineBase();
        vaccineBase.bootstrapVaccineBase();
        //System.out.println("List of patients waiting for vaccine administration: ");

        //List<SNSUserArrival> userWaitingRoomList = vacCenterController.getUsersInTheWaitingRoom(vaccinationCenter.getEmail());

        //Bootstrap for the waiting room
        List<SNSUserArrival> userWaitingRoomList = new ArrayList<>();
        SNSUserArrival user1 = new SNSUserArrival(123456789, "14:00", "19/06/2022");
        SNSUserArrival user2 = new SNSUserArrival(987654321, "15:00", "19/06/2022");
        SNSUserArrival user3 = new SNSUserArrival(123123123, "16:00", "19/06/2022");
        SNSUserArrival user4 = new SNSUserArrival(123333333, "17:00", "19/06/2022");
        userWaitingRoomList.add(user1);
        userWaitingRoomList.add(user2);
        userWaitingRoomList.add(user3);
        userWaitingRoomList.add(user4);

        int userNr = Utils.readIntegerFromConsole("Write user number to administer vaccine");

        for(SNSUserArrival selectedUser : userWaitingRoomList) {
            if(selectedUser.getSnsNumber() == userNr) {
                long snsNumber = controller.getSnsNumberArrival(selectedUser);
                if(snsUserStore.existsSNSUserStore(snsNumber)) {
                    //get name do user
                    String name = controller.getNameBySnsNumber(snsNumber);
                    System.out.println("Name of the SNS User: " + name);
                    //List<VaccineBase> vaccineBaseList = controller.returnVaccinesFromUser(snsNumber);

                    List<VaccineBase> vaccineBootstrap = vaccineBase.bootstrapVaccineBase();

                    //VaccineType vaccineType = vaccineBaseList.get(0).getVaccineType();
                    VaccineType vaccineType = vaccineBootstrap.get(0).getVaccineType();

                    System.out.println("List of Selected User Vaccines");
                    VaccineAdministrationNurse vaccineAdministrationNurse = controller.getVaccineAdministrationNurseBySnsNumberAndVaccine(snsNumber, vaccineType);
                    int dosesTaken = 0;
                    if(vaccineAdministrationNurse == null) {
                        System.out.println("Impossible to register. There are no vaccines available");
                        NurseUI nurseUI = new NurseUI(vaccinationCenter);
                        nurseUI.run();
                    } else {
                        dosesTaken = vaccineAdministrationNurse.getDoseNumber();
                        System.out.println("Doses taken: " + dosesTaken +
                                "\n Vaccine name/brand: " + vaccineType.getVaccineName() +
                                "\n" + vaccineType.getVaccineBrand());
                    }

                    System.out.println("Dose number administered: " + dosesTaken + 1);
                    String lotNumber = Utils.readLotNumber("Lot number: ");

                    System.out.println("Vaccine Administration: \n");
                    System.out.println(controller.registerVaccineNurseAdministration(snsNumber, vaccineType, vaccinationCenter, lotNumber, dosesTaken + 1));


                    if(Utils.confirm("Confirm administration? (Y/N)")) {
                        controller.saveVaccine();
                        System.out.println("Registered successfully");
                        controller.sendSmsToUserWithTimeToLeave();
                    }

                }
            } else {
                System.out.println("invalid user number, please try again");
            }
        }


    }

}
