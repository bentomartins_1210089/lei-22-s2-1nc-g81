package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Vaccination center ui.
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class VaccinationCenterUI implements Runnable{
    /**
     * Instantiates a new Vaccination center ui.
     */
    public VaccinationCenterUI()
    {
    }

    public void run()
    {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Register vaccination Center.", new RegisterVaccinationCenterUI()));

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nVaccination Center Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}
