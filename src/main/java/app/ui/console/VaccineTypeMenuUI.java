package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Delfim Costa <1081348@isep.ipp.pt>
 */


public class VaccineTypeMenuUI implements Runnable {

    public VaccineTypeMenuUI() {}

    public void run()
    {
        List<MenuItem> options = new ArrayList<>();
        options.add(new MenuItem("Create New Vaccine Type", new VaccineTypeUI()));
        //options.add(new MenuItem("List Vaccines Type", new ...()));
        int option;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nVaccine Type Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}