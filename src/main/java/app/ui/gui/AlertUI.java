package app.ui.gui;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

/**
 * JavaFX Alerts
 */
public class AlertUI {

    private static Alert alert;

    public static Alert showAlert(Alert.AlertType type, String title, String header, String description) {
        alert = new Alert(type);
        
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(description);
        
        if (type.equals(Alert.AlertType.CONFIRMATION)) {
            ((Button) alert.getDialogPane().lookupButton(ButtonType.OK)).setText("Yes");
            ((Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL)).setText("No");
        }

        return alert;
    }
}
