package app.ui.gui.ccoordinator_menu;

import app.controller.App;
import app.controller.LegacySystemDataController;
import app.controller.SerializationOnClose;
import app.domain.model.Company;
import app.domain.model.LegacySystemData;
import app.domain.shared.Enums.SortingType;
import app.dto.LegacySystemDataDTO;
import app.exception.InvalidLegacySystemException;
import app.exception.InvalidSnsNumberException;
import app.mappers.LegacySystemDataMapper;
import app.ui.console.*;
import app.ui.gui.AlertUI;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.input.DragEvent;
import javafx.scene.control.Alert;
import javafx.stage.WindowEvent;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.ResourceBundle;

/**
 * @author Tiago Oliveira <1211669>
 */

public class ImportLegacySystemController implements Initializable {

    public static final String LEGACY_TITLE = "Import Legacy System";
    public static String LEGACY_SYSTEM_NAME = "";
    private static final String ARRIVAL = SortingType.ARRIVAL.getSortingType();
    private static final String LEAVING = SortingType.LEAVING.getSortingType();

    private Company company;

    /**
     * Controllers
     */
    private LegacySystemDataController legCtrl;
    private CenterCoordinatorMenuController ccCtrl;

    /**
     * JavaFX Stages
     */
    private Stage stage;

    /**
     * FXML components
     */
    @FXML
    private TableView<LegacySystemData> table;
    @FXML
    private TableColumn<LegacySystemData, String> userNumberID;
    @FXML
    private TableColumn<LegacySystemData, String> userID;
    @FXML
    private TableColumn<LegacySystemData, String> vaccineID;
    @FXML
    private TableColumn<LegacySystemData, String> descID;
    @FXML
    private TableColumn<LegacySystemData, Integer> doseID;
    @FXML
    private TableColumn<LegacySystemData, String> lotID;
    @FXML
    private TableColumn<LegacySystemData, String> scheduleID;
    @FXML
    private TableColumn<LegacySystemData, String> arrivalID;
    @FXML
    private TableColumn<LegacySystemData, String> adminID;
    @FXML
    private TableColumn<LegacySystemData, String> leaveID;

    @FXML
    private Button addButton;
    @FXML
    private MenuBar menuBar;
    @FXML
    private Button removeButton;
    @FXML
    private Button sortButton;
    @FXML
    private ChoiceBox<String> sortOptions;
    @FXML
    private Label txtLStatus;
    @FXML
    private Label txtRStatus;
    @FXML
    private Font x1;
    @FXML
    private Color x2;
    @FXML
    private Font x3;
    @FXML
    private Color x4;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.company = App.getInstance().getCompany();
        this.legCtrl = new LegacySystemDataController();
        this.ccCtrl = new CenterCoordinatorMenuController();

        // Fixed options
        this.sortOptions.getItems().addAll(ARRIVAL, LEAVING);
        //Left status
        this.txtLStatus.setText("LEI PPROG");
        //Right status
        ccCtrl.initClock(txtRStatus);

        updateView();
    }

    public void updateView() {
        setTable();

        List<LegacySystemData> list = company.getLegacySystemStore().getLegacySystemStore();
        ObservableList<LegacySystemData> legSysTable = FXCollections.observableArrayList();

        for (LegacySystemData user : list) {
            System.out.println(user);
            legSysTable.add(user);
        }

        table.setItems(legSysTable);
    }

    public void updateView(List<LegacySystemData> list) {
        setTable();

        ObservableList<LegacySystemData> legSysTable = FXCollections.observableArrayList();

        for (LegacySystemData user : list) {
            System.out.println(user);
            legSysTable.add(user);
        }

        table.setItems(legSysTable);
    }

    private void setTable() {
        userNumberID.setCellValueFactory(new PropertyValueFactory<LegacySystemData, String>("snsNumber"));
        userID.setCellValueFactory(new PropertyValueFactory<LegacySystemData, String>("snsUserName"));
        vaccineID.setCellValueFactory(new PropertyValueFactory<LegacySystemData, String>("vaccine"));
        descID.setCellValueFactory(new PropertyValueFactory<LegacySystemData, String>("vaccineDesc"));
        doseID.setCellValueFactory(new PropertyValueFactory<LegacySystemData, Integer>("dose"));
        lotID.setCellValueFactory(new PropertyValueFactory<LegacySystemData, String>("lotNumber"));
        scheduleID.setCellValueFactory(new PropertyValueFactory<LegacySystemData, String>("schedule"));
        arrivalID.setCellValueFactory(new PropertyValueFactory<LegacySystemData, String>("arrival"));
        adminID.setCellValueFactory(new PropertyValueFactory<LegacySystemData, String>("administration"));
        leaveID.setCellValueFactory(new PropertyValueFactory<LegacySystemData, String>("leaving"));

        userNumberID.setStyle("-fx-alignment: CENTER;"); descID.setStyle("-fx-alignment: CENTER;");
        vaccineID.setStyle("-fx-alignment: CENTER;"); descID.setStyle("-fx-alignment: CENTER;");
        doseID.setStyle("-fx-alignment: CENTER;"); lotID.setStyle("-fx-alignment: CENTER;");
        scheduleID.setStyle("-fx-alignment: CENTER;"); arrivalID.setStyle("-fx-alignment: CENTER;");
        adminID.setStyle("-fx-alignment: CENTER;"); leaveID.setStyle("-fx-alignment: CENTER;");
    }
    @FXML
    void menuImportAction(ActionEvent event) {
        FileChooser flChooser = FileChooserUI.fileChoose("Legacy System Files", "*.csv");
        File flImport = flChooser.showOpenDialog(menuBar.getScene().getWindow());

        importFile(flImport);

        LEGACY_SYSTEM_NAME = flImport.getName();
        //legCtrl.saveLegacySystemData(LEGACY_SYSTEM_NAME);
    }

    @FXML
    void menuDragAction(DragEvent event) {
        Dragboard db = event.getDragboard();
        File flImport = db.getFiles().get(0);

        importFile(flImport);
    }

    @FXML
    void menuLogoutAction(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/LoginUI.fxml"));

        Stage stage = (Stage) menuBar.getScene().getWindow();
        stage.setScene(new Scene(root));
        stage.show();
    }

    @FXML
    void menuExitAction(ActionEvent event) {
        Stage stage = (Stage) menuBar.getScene().getWindow();
        stage.close();
    }

    @FXML
    void menuGoBackAction(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/CenterCoordinatorUI.fxml"));

        Stage stage = (Stage) menuBar.getScene().getWindow();
        stage.setScene(new Scene(root));
        stage.show();
    }

    @FXML
    void menuHelpUseAction(ActionEvent event) {
        AlertUI.showAlert(Alert.AlertType.INFORMATION, LEGACY_TITLE, "Help",
                "Start Importing the Legacy System .csv file in \n<File> -> <Import>." +
                        "\n\nThe .csv file must have the following format:" +
                        "\n     SNS Number;Vaccine Name;Dose;Lot Number;\n     Scheduled;Arrival;Administration;Leaving" +
                        "\n\nYou can Add, Remove or Sort users from the legacy system by using the left buttons." +
                        "\n\nPress the top-right X or <File> -> <Exit> to close the app.").show();
    }

    @FXML
    void menuAboutAction(ActionEvent event) {
        AlertUI.showAlert(Alert.AlertType.INFORMATION, "About",
                "Development Team", "Bento Martins - 1210089@isep.ipp.pt\n" +
                        "Delfim Costa - 1081348@isep.ipp.pt\n" +
                        "Mário Borja - 1200586@isep.ipp.pt\n" +
                        "Sérgio Soares - 1980292@isep.ipp.pt\n" +
                        "Tássio Gomes - 1170065@isep.ipp.pt\n" +
                        "Tiago Oliveira - 1211669@isep.ipp.pt").show();
    }

    @FXML
    void addUser(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AddUserLegacySystem.fxml"));
            Parent root = loader.load();

            stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Add New User");
            stage.setResizable(false);
            stage.setScene(new Scene(root));

            AddNewLegacyUserController addNewUser = loader.getController();
            addNewUser.mainUI(this);

            stage.show();
        } catch (IOException e) {
            AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, "Error", e.getMessage());
        }
    }

    @FXML
    void removeUser(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/RemoveUserLegacySystem.fxml"));
            Parent root = loader.load();

            stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Remove User");
            stage.setResizable(false);
            stage.setScene(new Scene(root));

            RemoveLegacyUserController remUser = loader.getController();
            remUser.mainUI(this);

            stage.show();
        } catch (IOException e) {
            AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, "Error", e.getMessage());
        }
    }

    @FXML
    void sortLegacyList(MouseEvent event) {
        String sortOption = sortOptions.getSelectionModel().getSelectedItem();
        List<LegacySystemData> sortedList = new ArrayList<>();

            if (sortOption.equals(ARRIVAL))
                sortedList = legCtrl.sortByArrival();
            if (sortOption.equals(LEAVING))
                sortedList = legCtrl.sortByLeaving();

        updateView(sortedList);
    }

    private void importFile(File flImport) {
        ArrayList<LegacySystemData> list, validatedList = new ArrayList<>();

        try {
            if (flImport != null) {
                ArrayList<LegacySystemDataDTO> importList = legCtrl.readCsv(flImport);

                LegacySystemDataMapper mapper = new LegacySystemDataMapper();
                list = mapper.dtoToLegacyData(importList);

                if (list.size() > 0) {
                    validatedList = legCtrl.validateList(list);
                    if (validatedList.size() > 0) {

                        Alert confirmAlert = AlertUI.showAlert(Alert.AlertType.CONFIRMATION, LEGACY_TITLE, "Confirmation",
                                "Import " + validatedList.size() + " users from Legacy System?");

                        if (confirmAlert.showAndWait().get() == ButtonType.OK) {
                            if (legCtrl.validateDuplicate(validatedList)) {
                                legCtrl.setLegacySystem(validatedList);
                                updateView();

                                AlertUI.showAlert(Alert.AlertType.INFORMATION, LEGACY_TITLE, "Imported Legacy System",
                                        String.format("%d users imported successfully!", list.size())).show();
                            } else {
                                AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, "Duplicate SNS user",
                                        "Please remove the duplicate user found in database.");
                            }
                        } else {
                            AlertUI.showAlert(Alert.AlertType.INFORMATION, LEGACY_TITLE, "Cancelled",
                                    "Import operation cancelled.").show();
                        }
                    } else {
                        AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, "No data found",
                                "Please correct the .csv file with valid values and try again.").show();
                    }
                } else {
                    AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, "No data found",
                            "Please check the .csv file and try again.").show();
                }
            } else {
                AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, "File not found",
                        "Please check the .csv file path and try again.").show();
            }
        } catch (InvalidSnsNumberException e) {
            AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, e.getMessage(),
                    "Please add the missing SNS users to database" +
                            "\nPlease check the users after line: " + (validatedList.size() + 1)).show();

            Alert confirmAlert = AlertUI.showAlert(Alert.AlertType.CONFIRMATION, LEGACY_TITLE, "Confirmation",
                    "Would you like to import the missing SNS users now?");

            if (confirmAlert.showAndWait().get() == ButtonType.OK)
                new LoadCsvSNSUsersUI().run();
        } catch (InvalidLegacySystemException e) {
            AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, e.getMessage(),
                    "Please correct the file data after line: " + (validatedList.size() + 1)).show();
        } catch (Exception e) {
            AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, "Error", e.getMessage()).show();
        }
    }
}