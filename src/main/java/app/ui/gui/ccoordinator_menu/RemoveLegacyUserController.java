package app.ui.gui.ccoordinator_menu;

import app.controller.App;
import app.controller.LegacySystemDataController;
import app.domain.model.LegacySystemData;
import app.domain.model.Validator;
import app.domain.store.LegacySystemStore;
import app.domain.store.VaccineTypeStore;
import app.exception.InvalidLegacySystemException;
import app.ui.gui.AlertUI;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static app.ui.gui.ccoordinator_menu.ImportLegacySystemController.LEGACY_TITLE;

/**
 * @author Tiago Oliveira <1211669>
 */
public class RemoveLegacyUserController implements Initializable {

    private ImportLegacySystemController mainUI;
    private LegacySystemDataController ctrl;
    private LegacySystemStore str;
    private Validator vldt;
    private VaccineTypeStore vcTypeStr;

    @FXML
    private Button removeButton;
    @FXML
    private DatePicker datePicker;
    @FXML
    private TextField txtSnsNum;
    @FXML
    private TextField txtVaccine;
    @FXML
    private TextField txtDose;
    @FXML
    private TextField txtLotNum;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.str = App.getInstance().getCompany().getLegacySystemStore();
        this.ctrl = new LegacySystemDataController();
        this.vcTypeStr = App.getInstance().getCompany().getVaccineTypeStore();
        this.vldt = new Validator();
    }

    public void mainUI(ImportLegacySystemController mainUI) {
        this.mainUI = mainUI;
    }

    /**
     * Remove User Button
     * @param event MouseEvent
     */
    @FXML
    private void btnRemUser(ActionEvent event) {
        try {
            ArrayList<LegacySystemData> listValidated = validateUser();

            if (listValidated != null) {
                if (!ctrl.validateDuplicate(listValidated.get(0))) {

                    boolean rmv = ctrl.removeUser(listValidated.get(0));

                    if (rmv) {
                        mainUI.updateView();
                        AlertUI.showAlert(Alert.AlertType.INFORMATION, LEGACY_TITLE, "Remove user",
                                "Removed user successfully!").show();
                        clearAndClose(event);
                    } else {
                        AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, "Error removing user",
                                "Please verify input data.").show();
                    }
                } else {
                    AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, "Error removing user",
                            "User doesn't exist in database.").show();
                }
            } else {
                AlertUI.showAlert(Alert.AlertType.INFORMATION, LEGACY_TITLE, "Remove user",
                        "Please fill all text fields with valid values.").show();
            }
        } catch (InvalidLegacySystemException e) {
            AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, "Data error",
                    "Fill all the fields with valid values\n").show();
        }
    }

    /**
     * Cancel Button
     * @param event MouseEvent
     */
    @FXML
    private void btnCancel(ActionEvent event) {
        clearAndClose(event);
    }

    /**
     * Validates the user data
     * @return user data if valid, null otherwise
     */
    private ArrayList<LegacySystemData> validateUser() {
        DateTimeFormatter fmtDate = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        String date = fmtDate.format(datePicker.getValue());
        int dose = ctrl.csvDosesStringToInt(txtDose.getText());

        //VALIDATIONS
        long snsNumber = vldt.validateSnsNumber(Long.parseLong(txtSnsNum.getText()));
        vldt.validateNumDoses(dose);

        if (!vcTypeStr.existsVaccineStore(txtVaccine.getText())) {
            throw new InvalidLegacySystemException("\nVaccine '" +txtVaccine.getText()+ "' not registered");
        } else if (!vldt.checkLotNumber(txtLotNum.getText())) {
            throw new InvalidLegacySystemException("\nLot number '" +txtLotNum.getText()+ "' is invalid");
        } else if (!vldt.validDate(String.valueOf(datePicker.getValue()))) {
            throw new InvalidLegacySystemException("\nDate value '" +datePicker.getValue()+ "' is invalid");
        }

        List<LegacySystemData> listStr = str.getLegacySystemStore();
        ArrayList<LegacySystemData> list = new ArrayList<>();
        for (LegacySystemData user: listStr) {
            String[] scheduleDate = user.getSchedule().split(" ");

            boolean isEqual = user.getSNSNUmber().equals(String.valueOf(snsNumber));
            boolean isEqualLot = user.getLotNumber().equals(txtLotNum.getText());
            boolean isEqualVaccine = user.getVaccine().equals(txtVaccine.getText());
            boolean isEqualDose = user.getDose() == dose;
            boolean isEqualDate = scheduleDate[0].equals(date);

            if (isEqual && isEqualLot && isEqualVaccine && isEqualDose && isEqualDate)
                list.add(user);
        }
        return list;
    }

    /**
     * Clears all the fields and closes the window
     * @param event MouseEvent
     */
    private void clearAndClose(ActionEvent event) {
        this.txtSnsNum.clear();
        this.txtVaccine.clear();
        this.txtDose.clear();
        this.txtLotNum.clear();

        ((Node) event.getSource()).getScene().getWindow().hide();
    }
}