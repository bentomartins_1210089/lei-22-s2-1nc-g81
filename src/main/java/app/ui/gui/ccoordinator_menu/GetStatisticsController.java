package app.ui.gui.ccoordinator_menu;

import app.controller.App;
import app.domain.model.Employee;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineAdministrationNurse;
import app.domain.store.EmployeeStore;
import app.domain.store.VaccineAdministrationNurseStore;
import app.ui.gui.AlertUI;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * The type Get statistics controller.
 * @author Bento Martins <1210089>
 */
public class GetStatisticsController implements Initializable {

    /**
     * The type Data 4 table.
     */
    public class Data4table {

        /**
         * The Date.
         */
        String date;
        /**
         * The Number.
         */
        String number;

        /**
         * Instantiates a new Data 4 table.
         *
         * @param date   the date
         * @param number the number
         */
        public Data4table(String date, String number) {
            this.date = date;
            this.number = number;
        }

        /**
         * Gets date.
         *
         * @return the date
         */
        public String getDate() {
            return date;
        }

        /**
         * Sets date.
         *
         * @param date the date
         */
        public void setDate(String date) {
            this.date = date;
        }

        /**
         * Gets number.
         *
         * @return the number
         */
        public String getNumber() {
            return number;
        }

        /**
         * Sets number.
         *
         * @param number the number
         */
        public void setNumber(String number) {
            this.number = number;
        }


    }

    private Stage stage;
    private Scene scene;
    private Parent root;
    private App app = App.getInstance();

    @FXML
    private TableView table;

    @FXML
    private TableColumn<Data4table, String> dateId;

    @FXML
    private TableColumn<Data4table, String> fullyId;

    @FXML
    private GridPane gridPane;

    @FXML
    private Label txtRStatus;

    @FXML
    private PasswordField password;

    @FXML
    private Font x3;

    @FXML
    private Color x4;

    @FXML
    private TextField email;

    @FXML
    private MenuBar menuBar;


    @FXML
    private DatePicker iniDate;

    @FXML
    private DatePicker endDate;

    @FXML
    private Text texttest;

    /**
     * Menu drag action.
     *
     * @param event the event
     */
    @FXML
    void menuDragAction(ActionEvent event) {
        ;
    }

    /**
     * The File chooser.
     */
    FileChooser fileChooser = new FileChooser();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        fileChooser.setInitialDirectory(new File("C:"));
        fileChooser.setTitle("Save Statistics");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Csv file", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);

        initClock();

        LocalDate timeYesterday = LocalDate.now().minusDays(1);
        LocalDate timePreviousWeek = timeYesterday.minusDays(7);

        iniDate.setValue(timePreviousWeek);
        endDate.setValue(timeYesterday);

    }

    private void initClock() {

        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd    HH:mm:ss ");
            txtRStatus.setText(LocalDateTime.now().format(formatter));
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

    /**
     * Save data.
     *
     * @param event the event
     * @throws FileNotFoundException the file not found exception
     */
    @FXML
    void saveData(ActionEvent event) throws FileNotFoundException {

        LocalDate timeIni = iniDate.getValue();
        LocalDate timeEnd = endDate.getValue();

        Boolean valid = validateChosenDates();

        if (valid) {

            ArrayList<String> stringsToAdd = getStatisticsFromGivenInterval(timeIni, timeEnd);

            String content = "";

            for (String line : stringsToAdd) {

                content = content + line;

            }

            String fileName = "FullyVac_" + timeIni + "_" + timeEnd;
            fileName = fileName.replace("-", "");
            try {
            fileChooser.setInitialFileName(fileName);

            File file = fileChooser.showSaveDialog(new Stage());
            fileChooser.setInitialDirectory((file.getParentFile()));

            PrintWriter printWriter = new PrintWriter(file);
            printWriter.write(content);
            printWriter.close();

            Alert alert = AlertUI.showAlert(Alert.AlertType.INFORMATION, "Data export",
                    "Data was exported successfully", "");
            alert.show();
            password.clear();

            } catch (Exception ex) {

            }

        }

        }


    /**
     * About us.
     *
     * @param event the event
     */
    @FXML
        void aboutUs (ActionEvent event){
            Alert alert = AlertUI.showAlert(Alert.AlertType.INFORMATION, "About",
                    "Development Team:", "Bento Martins - 1210089@isep.ipp.pt\n" +
                            "Delfim Costa - 1081348@isep.ipp.pt\n" +
                            "Mário Borja - 1200586@isep.ipp.pt\n" +
                            "Sérgio Soares - 1980292@isep.ipp.pt\n" +
                            "Tássio Gomes - 1170065@isep.ipp.pt\n" +
                            "Tiago Oliveira - 1211669@isep.ipp.pt");
            alert.show();

        }

    /**
     * Logout.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    @FXML
        void logout (ActionEvent event) throws IOException {

            root = FXMLLoader.load(getClass().getResource("/fxml/LoginUI.fxml"));
            Stage stage = (Stage) menuBar.getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.show();

        }


    /**
     * Exit.
     *
     * @param event the event
     */
    @FXML
        void exit (ActionEvent event){

            Stage stage = (Stage) menuBar.getScene().getWindow();
            stage.close();

        }

    /**
     * Get statistics.
     *
     * @param event the event
     */
    @FXML
        void getStatistics (ActionEvent event){

            LocalDate timeIni = iniDate.getValue();
            LocalDate timeEnd = endDate.getValue();

            Boolean valid = validateChosenDates();

            if (valid) {
                dateId.setCellValueFactory(new PropertyValueFactory<Data4table, String>("date"));
                fullyId.setCellValueFactory(new PropertyValueFactory<Data4table, String>("number"));

                dateId.setStyle("-fx-alignment: CENTER;");
                fullyId.setStyle("-fx-alignment: CENTER;");


                ArrayList<String> stringsToAdd = getStatisticsFromGivenInterval(timeIni, timeEnd);

                ObservableList<Data4table> data4table = FXCollections.observableArrayList();

                for (String string : stringsToAdd) {

                    String day = string.split(",")[0];
                    String fullVac = string.split(",")[1];

                    data4table.add(new Data4table(day, fullVac));
                }

                table.setItems(data4table);
            }
        }

    /**
     * Gets back.
     *
     * @param actionEvent the action event
     * @throws IOException the io exception
     */
    public void getBack (ActionEvent actionEvent) throws IOException {

            root = FXMLLoader.load(getClass().getResource("/fxml/CenterCoordinatorUI.fxml"));
            Stage stage = (Stage) menuBar.getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.show();

        }


    /**
     * Get statistics from given interval array list.
     *
     * @param timeIni the time ini
     * @param timeEnd the time end
     * @return the array list
     */
    public ArrayList<String> getStatisticsFromGivenInterval (LocalDate timeIni, LocalDate timeEnd){

            VaccinationCenter vaccinationCenter = getVaccinationCenterOfCoordinator();

            ArrayList<VaccineAdministrationNurse> administrationsBetweenDatesInCenter = getAdministratedVaccinesFromIniToEndDateByCenter(vaccinationCenter, timeIni, timeEnd);

            VaccineAdministrationNurseStore vacAdminStore = app.getCompany().getVaccineAdministrationNurseStore();

            ArrayList<String> dataToExport = vacAdminStore.getFullDosesByDateFromAdministrations(administrationsBetweenDatesInCenter, timeIni, timeEnd);

            return dataToExport;

        }


    /**
     * Gets vaccination center of coordinator.
     *
     * @return the vaccination center of coordinator
     */
    public VaccinationCenter getVaccinationCenterOfCoordinator () {

            Email emailTemp = app.getCompany().getAuthFacade().getCurrentUserSession().getUserId();
            String email = emailTemp.toString();

            Employee employee = app.getCompany().getEmployeeStore().findEmployee(email);

            VaccinationCenter vaccinationCenter = employee.getVaccinationCenter();

            return vaccinationCenter;

        }


    /**
     * Get administrated vaccines from ini to end date by center array list.
     *
     * @param vaccinationCenter the vaccination center
     * @param timeIni           the time ini
     * @param timeEnd           the time end
     * @return the array list
     */
    public ArrayList<VaccineAdministrationNurse> getAdministratedVaccinesFromIniToEndDateByCenter (VaccinationCenter
        vaccinationCenter, LocalDate timeIni, LocalDate timeEnd){

            VaccineAdministrationNurseStore vacAdminStore = app.getCompany().getVaccineAdministrationNurseStore();

            ArrayList<VaccineAdministrationNurse> administrationsBetweenDatesInCenter = vacAdminStore.getVaccinatedByDateIntervalByCenter(vaccinationCenter, timeIni, timeEnd);

            return administrationsBetweenDatesInCenter;
        }


    /**
     * Validate chosen dates boolean.
     *
     * @return the boolean
     */
    public Boolean validateChosenDates() {

            LocalDate timeIni = iniDate.getValue();
            LocalDate timeEnd = endDate.getValue();

            if (iniDate.getValue() == null || endDate.getValue() == null) {
                Alert alert = AlertUI.showAlert(Alert.AlertType.WARNING, "Date error",
                        "Please verify your date intervals", "");
                alert.show();

                return false;
            }

            LocalDate timeNow = LocalDate.now();

            boolean nowIsBeforeIni = timeNow.isAfter(timeEnd);
            boolean nowIsBeforeEnd = timeNow.isAfter(timeIni);

            if (!nowIsBeforeIni || !nowIsBeforeEnd) {
                Alert alert = AlertUI.showAlert(Alert.AlertType.WARNING, "Date error",
                        "Please verify your date intervals", "Final or initial date can't be in the future");
                alert.show();

                return false;
            }

            boolean isBefore = timeEnd.isAfter(timeIni);

            if (!isBefore && !(timeEnd.equals(timeIni))) {
                Alert alert = AlertUI.showAlert(Alert.AlertType.WARNING, "Date error",
                        "Please verify your date intervals", "Final date should be after initial date");
                alert.show();

                return false;
            }

            return true;
        }


    }


