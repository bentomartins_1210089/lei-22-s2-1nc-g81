package app.ui.gui.ccoordinator_menu;

import app.controller.App;
import app.domain.model.Employee;
import app.ui.gui.AlertUI;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import static app.ui.gui.ccoordinator_menu.ImportLegacySystemController.LEGACY_TITLE;

/**
 * The type Center coordinator menu controller.
 * @author Bento Martins <1210089>
 */
public class CenterCoordinatorMenuController implements Initializable {

    private Stage stage;
    private Scene scene;
    private Parent root;
    private app.controller.App app = App.getInstance();

    @FXML
    private Label txtLStatus;

    @FXML
    private Label txtRStatus;

    @FXML
    private PasswordField password;

    @FXML
    private Font x3;

    @FXML
    private Color x4;

    @FXML
    private TextField email;

    @FXML
    private Button exportButton;

    @FXML
    private Button importButton;

    @FXML
    private MenuBar menuBar;

    /**
     * Menu drag action.
     *
     * @param event the event
     */
    @FXML
    void menuDragAction(ActionEvent event) {;
    }

    /**
     * Go to statistics.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    @FXML
    void goToStatistics(ActionEvent event) throws IOException {

        root = FXMLLoader.load(getClass().getResource("/fxml/GetStatisticsUI.fxml"));

        stage = (Stage) exportButton.getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();


    }


    /**
     * Go to legacy system.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    @FXML
    void goToLegacySystem(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/ImportLegacySystemMenu.fxml"));
        stage = (Stage) exportButton.getScene().getWindow();

        stage.setTitle(LEGACY_TITLE);
        stage.setResizable(false);
        stage.setScene(new Scene(root));

        stage.show();
    }

    /**
     * Show center performance.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    @FXML
    void showCenterPerformance(ActionEvent event) throws IOException {
        String fxmlPath="/fxml/CenterPerformanceUI.fxml";
        root = FXMLLoader.load(getClass().getResource(fxmlPath));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        initClock(txtRStatus);

    }

    /**
     * Init clock.
     *
     * @param txtRStatus the txt r status
     */
    public void initClock(Label txtRStatus) {

        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd    HH:mm:ss ");
            txtRStatus.setText(LocalDateTime.now().format(formatter));
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();

    }

    /**
     * About us.
     *
     * @param event the event
     */
    @FXML
    void aboutUs(ActionEvent event) {
        Alert alert = AlertUI.showAlert(Alert.AlertType.INFORMATION, "About",
                "Development Team:", "Bento Martins - 1210089@isep.ipp.pt\n" +
                        "Delfim Costa - 1081348@isep.ipp.pt\n" +
                        "Mário Borja - 1200586@isep.ipp.pt\n" +
                        "Sérgio Soares - 1980292@isep.ipp.pt\n" +
                        "Tássio Gomes - 1170065@isep.ipp.pt\n" +
                        "Tiago Oliveira - 1211669@isep.ipp.pt");
        alert.show();
    }

    /**
     * Logout.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    @FXML
    void logout(ActionEvent event) throws IOException {

        root = FXMLLoader.load(getClass().getResource("/fxml/LoginUI.fxml"));
        Stage stage = (Stage) menuBar.getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

        }


    /**
     * Exit.
     *
     * @param event the event
     */
    @FXML
    void exit(ActionEvent event) {

        Stage stage = (Stage) menuBar.getScene().getWindow();
        stage.close();

    }
}
