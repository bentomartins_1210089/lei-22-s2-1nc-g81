package app.ui.gui.ccoordinator_menu;

        import app.ui.console.FileChooserUI;
        import com.isep.mdis.Sum;
        import app.controller.App;
        import app.controller.VacinationCenterController;
        import app.domain.model.VaccinationCenter;
        import app.ui.gui.AlertUI;
        import javafx.animation.Animation;
        import javafx.animation.KeyFrame;
        import javafx.animation.Timeline;
        import javafx.event.ActionEvent;
        import javafx.fxml.FXML;
        import javafx.fxml.FXMLLoader;
        import javafx.fxml.Initializable;
        import javafx.scene.Parent;
        import javafx.scene.Scene;
        import javafx.scene.control.*;
        import javafx.stage.FileChooser;
        import javafx.stage.Stage;
        import javafx.util.Duration;
        import pt.isep.lei.esoft.auth.domain.model.Email;

        import java.io.*;
        import java.net.URL;
        import java.time.LocalDate;
        import java.time.LocalDateTime;
        import java.time.format.DateTimeFormatter;
        import java.util.*;
        import java.util.logging.Logger;

public class CenterPerformanceController implements Initializable {

    private Scene  scene;
    private Parent root;
    private App    app = App.getInstance();

    @FXML
    private Label txtRStatus;

    @FXML
    private MenuBar menuBar;


    private VacinationCenterController ctrlVacinationCenter;
    private VaccinationCenter vaccinationCenter;
    private String    interval;
    private LocalDate date;

    @FXML
    private ComboBox cmbCenterList;
    @FXML
    private ComboBox cmbIntervalList;
    @FXML
    private DatePicker selDate;
    @FXML
    private TableView tblViewData;
    @FXML
    private TextArea txtLoadedData;
    @FXML
    private TextField txtResult;
    @FXML
    void menuDragAction(ActionEvent event) {;
    }

    //FileChooser fileChooser = new FileChooser();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        initClock();

        this.ctrlVacinationCenter = new VacinationCenterController();
        try {
            this.cmbCenterList.getItems().addAll(this.getVaccinationCenterList());
        }catch (Exception e){
            cmbCenterList.setPromptText(e.getMessage());
        }

        this.cmbIntervalList.getItems().addAll( "1",
                                            "5",
                                            "10",
                                            "20",
                                            "30");
    }

    private void initClock() {

        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd    HH:mm:ss ");
            txtRStatus.setText(LocalDateTime.now().format(formatter));
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

    @FXML
    private List<Email> getVaccinationCenterList() {
        List<VaccinationCenter> vaccinationCenterList = this.ctrlVacinationCenter.getVaccinationCenterList();
        List<Email> centerEmailList = new ArrayList<>();
        for (VaccinationCenter vaccinationCenter: vaccinationCenterList) {
            centerEmailList.add(vaccinationCenter.getEmail());
        }
        return centerEmailList;
    }

    @FXML
    private void setVaccinationCenter(ActionEvent event){
        Optional <VaccinationCenter> optionalVaccinationCenter = ctrlVacinationCenter.getById(this.cmbCenterList.getValue().toString());
        if(optionalVaccinationCenter.isPresent()) {
            this.vaccinationCenter = optionalVaccinationCenter.get();
        }
    }

    @FXML
    private void setInterval(ActionEvent event){
        this.interval = cmbIntervalList.getValue().toString();
    }

    @FXML
    private void setDate(ActionEvent event) {
        this.date = this.selDate.getValue();
    }

    @FXML
    private void loadData(ActionEvent event) {
        FileChooser fileChooser = FileChooserUI.fileChoose("Performance Data File", "*.csv");
        File        file  = fileChooser.showOpenDialog(menuBar.getScene().getWindow());

        readCSV(file);
    }

    private void readCSV(File file) {

        BufferedReader br;

        try {
            br = new BufferedReader(new FileReader(file));

            String line;
            while ((line = br.readLine()) != null) {
                txtLoadedData.appendText(line+"\n");
            }
        } catch (FileNotFoundException ex) {
            txtLoadedData.setText(ex.toString());
        } catch (IOException ex) {
            txtLoadedData.setText(ex.toString());
        }
    }

    @FXML
    private void calculatePerformance(ActionEvent event) {
        int[] example = new int[]{29, -32, -9, -25, 44, 12, -61, 51, -9, 44, 74, 4};
        int[] result = Sum.Max(example);
        txtResult.setText(Arrays.toString(result));
    }

    @FXML
    void aboutUs(ActionEvent event) {
        Alert alert = AlertUI.showAlert(Alert.AlertType.INFORMATION, "About",
                                        "Development Team:", "Bento Martins - 1210089@isep.ipp.pt\n" +
                                                "Delfim Costa - 1081348@isep.ipp.pt\n" +
                                                "Mário Borja - 1200586@isep.ipp.pt\n" +
                                                "Sérgio Soares - 1980292@isep.ipp.pt\n" +
                                                "Tássio Gomes - 1170065@isep.ipp.pt\n" +
                                                "Tiago Oliveira - 1211669@isep.ipp.pt");
        alert.show();

    }

    @FXML
    void logout(ActionEvent event) throws IOException {

        root = FXMLLoader.load(getClass().getResource("/fxml/LoginUI.fxml"));
        Stage stage = (Stage) menuBar.getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }


    @FXML
    void exit(ActionEvent event) {

        Stage stage = (Stage) menuBar.getScene().getWindow();
        stage.close();

    }

    public void getBack(ActionEvent actionEvent) throws IOException {

        root = FXMLLoader.load(getClass().getResource("/fxml/CenterCoordinatorUI.fxml"));
        Stage stage = (Stage) menuBar.getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
