package app.ui.gui.ccoordinator_menu;

import app.controller.LegacySystemDataController;
import app.domain.model.LegacySystemData;
import app.domain.store.SNSUserStore;
import app.exception.InvalidLegacySystemException;
import app.exception.InvalidSnsNumberException;
import app.ui.console.LoadCsvSNSUsersUI;
import app.ui.gui.AlertUI;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;

import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static app.ui.gui.ccoordinator_menu.ImportLegacySystemController.LEGACY_TITLE;

/**
 * @author Tiago Oliveira <1211669>
 */

public class AddNewLegacyUserController implements Initializable {

    private ImportLegacySystemController mainUI;
    private LegacySystemDataController ctrl;

    @FXML
    private Button addButton;
    @FXML
    private DatePicker datePicker;
    @FXML
    private TextField txtSnsNum;
    @FXML
    private TextField txtVaccine;
    @FXML
    private TextField txtDose;
    @FXML
    private TextField txtLotNum;
    @FXML
    private TextField txtScheduleTime;
    @FXML
    private TextField txtArrivalTime;
    @FXML
    private TextField txtAdministrationTime;
    @FXML
    private TextField txtLeavingTime;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.ctrl = new LegacySystemDataController();
    }

    public void mainUI(ImportLegacySystemController mainUI) {
        this.mainUI = mainUI;
    }

    /**
     * Add User Button
     * @param event MouseEvent
     */
    @FXML
    private void btnAddUser(ActionEvent event) {
        try {
            ArrayList<LegacySystemData> listValidated = validateUser();

            if (listValidated != null) {
                if (ctrl.validateDuplicate(listValidated)) {

                    boolean stored = ctrl.addUser(listValidated.get(0));

                    if (stored) {
                        mainUI.updateView();
                        AlertUI.showAlert(Alert.AlertType.INFORMATION, LEGACY_TITLE, "Add new user",
                                "Added new user successfully!").show();
                        clearAndClose(event);
                    } else {
                        AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, "Error adding user",
                                "Please verify input data.").show();
                    }
                } else {
                    AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, "Error adding user",
                            "User already exists in database.").show();
                }
            } else {
                AlertUI.showAlert(Alert.AlertType.INFORMATION, LEGACY_TITLE, "Add new user",
                        "Please fill all the text fields correctly before addind a new user").show();
            }
        } catch (InvalidSnsNumberException e) {
            AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, e.getMessage(),
                    "Please add the missing SNS users to database").show();

            Alert confirmAlert = AlertUI.showAlert(Alert.AlertType.CONFIRMATION, LEGACY_TITLE, "Confirmation",
                    "Would you like to import the missing SNS users now?");

            if (confirmAlert.showAndWait().get() == ButtonType.OK)
                new LoadCsvSNSUsersUI().run();
        } catch (InvalidLegacySystemException e) {
            AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, e.getMessage(),
                    "Please fill all the fields with valid values").show();
        } catch (Exception e) {
            AlertUI.showAlert(Alert.AlertType.ERROR, LEGACY_TITLE, "Error", "Invalid values").show();
        }
    }

    /**
     * Cancel Button
     * @param event MouseEvent
     */
    @FXML
    private void btnCancel(ActionEvent event) {
        clearAndClose(event);
    }

    /**
     * Validates the user data
     * @return user data if valid, null otherwise
     */
    private ArrayList<LegacySystemData> validateUser() {
        DateTimeFormatter fmtDate = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        String date = fmtDate.format(datePicker.getValue());

        LegacySystemData user = new LegacySystemData(
                txtSnsNum.getText(),
                txtVaccine.getText(),
                ctrl.csvDosesStringToInt(txtDose.getText()),
                txtLotNum.getText(),
                String.format(date +" "+ txtScheduleTime.getText()),
                String.format(date +" "+ txtArrivalTime.getText()),
                String.format(date +" "+ txtAdministrationTime.getText()),
                String.format(date +" "+ txtLeavingTime.getText())
        );

        ArrayList<LegacySystemData> list = new ArrayList<>();
        list.add(user);

        return ctrl.validateList(list);
    }

    /**
     * Clears all the fields and closes the window
     * @param event ActionEvent
     */
    private void clearAndClose(ActionEvent event) {
        this.txtSnsNum.clear();
        this.txtVaccine.clear();
        this.txtDose.clear();
        this.txtLotNum.clear();
        this.txtScheduleTime.clear();
        this.txtArrivalTime.clear();
        this.txtAdministrationTime.clear();
        this.txtLeavingTime.clear();

        ((Node) event.getSource()).getScene().getWindow().hide();
    }
}