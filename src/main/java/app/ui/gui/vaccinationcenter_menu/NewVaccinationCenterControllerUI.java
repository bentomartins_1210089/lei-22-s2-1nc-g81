package app.ui.gui.vaccinationcenter_menu;

import app.controller.VacinationCenterController;
import app.domain.model.Employee;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.*;

public class NewVaccinationCenterControllerUI implements Initializable {

    private VacinationCenterController ctrlVacinationCenter;

    @FXML
    private Button     addButton;
    @FXML
    private TextField  txtName;
    @FXML
    private TextField  txtAddress;
    @FXML
    private TextField txtPhone;
    @FXML
    private TextField txtFax;
    @FXML
    private TextField txtEmail;
    @FXML
    private TextField txtWebsite;
    @FXML
    private TextField txtOpenningHours;
    @FXML
    private TextField txtClosingHours;
    @FXML
    private TextField txtSlot;
    @FXML
    private TextField              txtMaximumNumberVaccines;
    @FXML
    private ComboBox cmbCoordinator;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.ctrlVacinationCenter = new VacinationCenterController();
        if(this.getCoordinatorsList().isEmpty()) addButton.setDisable(true);
            else{
            this.cmbCoordinator.getItems().addAll(this.getCoordinatorsList());
        }
    }

    @FXML
    private void addCenter(ActionEvent event) {
        if(ctrlVacinationCenter.newVaccinationCenter(this.txtName.getText(), this.txtAddress.getText(), this.txtPhone.getText(), this.txtFax.getText(), this.txtEmail.getText(), this.txtWebsite.getText(), this.txtClosingHours.getText(), this.txtClosingHours.getText(), this.txtSlot.getText(), this.txtMaximumNumberVaccines.getText(), this.cmbCoordinator.getValue().toString())) {
            ctrlVacinationCenter.saveVaccinationCenter();
        }
    }
    /**
     * Clears all the fields and closes the window
     * @param event ActionEvent
     */
    @FXML
    private void clearAndClose(ActionEvent event) { //soaressf
        this.txtName.clear();
        this.txtAddress.clear();
        this.txtPhone.clear();
        this.txtFax.clear();
        this.txtEmail.clear();
        this.txtWebsite.clear();
        this.txtOpenningHours.clear();
        this.txtClosingHours.clear();
        this.txtSlot.clear();
        this.txtMaximumNumberVaccines.clear();

        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    @FXML
    private List<String> getCoordinatorsList() {
        List<Employee> employeeList = ctrlVacinationCenter.getCoordinatorsList();
        List<String> employeeEmailList = new ArrayList<>();
        for (Employee employee: employeeList) {
            employeeEmailList.add(employee.getEmail());
        }
        return employeeEmailList;
    }
}
