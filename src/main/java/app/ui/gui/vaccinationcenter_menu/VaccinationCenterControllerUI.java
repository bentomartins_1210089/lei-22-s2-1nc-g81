package app.ui.gui.vaccinationcenter_menu;

import app.ui.gui.AlertUI;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class VaccinationCenterControllerUI implements Initializable {

    private Stage  stage;
    private Scene  scene;
    private Parent root;
    @FXML
    private Label txtRStatus;
    @FXML
    private MenuBar menuBar;
    @FXML
    void menuDragAction(ActionEvent event) {;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        initClock();

    }

    private void initClock() {

        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd    HH:mm:ss ");
            txtRStatus.setText(LocalDateTime.now().format(formatter));
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

    @FXML
    void newVaccinationCenter(ActionEvent event) throws IOException {
        String fxmlPath="/fxml/newVaccinationCenterUI.fxml";
        root = FXMLLoader.load(getClass().getResource(fxmlPath));
        scene = new Scene(root);
        stage = new Stage();
        stage.setScene(scene);
        stage.show();
    }
    @FXML
    void aboutUs(ActionEvent event) {
        Alert alert = AlertUI.showAlert(Alert.AlertType.INFORMATION, "About",
                                        "Development Team:", "Bento Martins - 1210089@isep.ipp.pt\n" +
                                                "Delfim Costa - 1081348@isep.ipp.pt\n" +
                                                "Mário Borja - 1200586@isep.ipp.pt\n" +
                                                "Sérgio Soares - 1980292@isep.ipp.pt\n" +
                                                "Tássio Gomes - 1170065@isep.ipp.pt\n" +
                                                "Tiago Oliveira - 1211669@isep.ipp.pt");
        alert.show();
    }


    @FXML
    void logout(ActionEvent event) throws IOException {

        root = FXMLLoader.load(getClass().getResource("/fxml/LoginUI.fxml"));
        Stage stage = (Stage) menuBar.getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

    @FXML
    void exit(ActionEvent event) {

        Stage stage = (Stage) menuBar.getScene().getWindow();
        stage.close();

    }

}
