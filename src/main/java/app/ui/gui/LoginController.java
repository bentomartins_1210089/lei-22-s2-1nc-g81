package app.ui.gui;

import app.controller.App;
import app.controller.AuthController;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import pt.isep.lei.esoft.auth.domain.model.User;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

/**
 * The type Login controller.
 * @author Bento Martins <1210089>
 */
public class LoginController implements Initializable {

    private app.controller.App app = App.getInstance();

    private AuthController authCtrl = new AuthController();
    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    private Label txtRStatus;

    @FXML
    private PasswordField password;

    @FXML
    private Button btnLogin;

    @FXML
    private Font x3;

    @FXML
    private Color x4;

    @FXML
    private TextField email;

    @FXML
    private MenuItem dothis;

    /**
     * Menu drag action.
     *
     * @param event the event
     */
    @FXML
    void menuDragAction(ActionEvent event) {;
    }

    @FXML
    private MenuBar menuBar;

    /**
     * Do login.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    @FXML
    void doLogin(ActionEvent event) throws IOException {
        boolean success = authCtrl.doLogin(email.getText(), password.getText());

        if(success){

            String userole = app.getCurrentUserSession().getUserRoles().get(0).getId();

            if (userole=="ADMINISTRATOR") {
                switchScene(event,"Administrator");
            }

            if (userole=="CENTER COORDINATOR") {
                switchScene(event,"CenterCoordinator");
            }

            if (userole=="NURSE") {
                switchScene(event,"Nurse");
            }

            if (userole=="RECEPTIONIST") {
                switchScene(event,"Receptionist");
            }

            if (userole=="USER") {
                switchScene(event,"User");
            }

        }else{
            Alert alert = AlertUI.showAlert(Alert.AlertType.ERROR, "Login Error",
                    "Login credentials error", "Please correct values.");
            alert.show();
            password.clear();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        initClock();

    }

    private void initClock() {

        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd    HH:mm:ss ");
            txtRStatus.setText(LocalDateTime.now().format(formatter));
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

    /**
     * About us.
     *
     * @param event the event
     */
    @FXML
    void aboutUs(ActionEvent event) {
        Alert alert = AlertUI.showAlert(Alert.AlertType.INFORMATION, "About",
                "Development Team:", "Bento Martins - 1210089@isep.ipp.pt\n" +
                        "Delfim Costa - 1081348@isep.ipp.pt\n" +
                        "Mário Borja - 1200586@isep.ipp.pt\n" +
                        "Sérgio Soares - 1980292@isep.ipp.pt\n" +
                        "Tássio Gomes - 1170065@isep.ipp.pt\n" +
                        "Tiago Oliveira - 1211669@isep.ipp.pt");
        alert.show();
    }


    /**
     * Switch scene.
     *
     * @param event the event
     * @param role  the role
     * @throws IOException the io exception
     */
    public void switchScene(ActionEvent event, String role) throws IOException {

        String fxmlPath="/fxml/"+role+"UI.fxml";

        root = FXMLLoader.load(getClass().getResource(fxmlPath));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

    /**
     * Exit.
     *
     * @param event the event
     */
    @FXML
    void exit(ActionEvent event) {

        Stage stage = (Stage) menuBar.getScene().getWindow();
        stage.close();

    }

}
