package app.ui.gui.user_menu;

import app.ui.console.NurseUI;
import app.ui.console.SNSUserUI;
import app.ui.gui.AlertUI;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

/**
 * The type User menu controller.
 * @author Bento Martins <1210089>
 */
public class UserMenuController implements Initializable {

    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    private Label txtLStatus;

    @FXML
    private Label txtRStatus;

    @FXML
    private PasswordField password;

    @FXML
    private Font x3;

    @FXML
    private Color x4;

    @FXML
    private TextField email;

    /**
     * Menu drag action.
     *
     * @param event the event
     */
    @FXML
    void menuDragAction(ActionEvent event) {;
    }

    @FXML
    private MenuBar menuBar;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        initClock();

    }

    /**
     * Go to console ui.
     *
     * @param event the event
     */
    @FXML
    void goToConsoleUI(ActionEvent event) {
        Stage stage = (Stage) menuBar.getScene().getWindow();
        stage.setIconified(true);

        new SNSUserUI().run();

    }

    private void initClock() {

        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd    HH:mm:ss ");
            txtRStatus.setText(LocalDateTime.now().format(formatter));
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

    /**
     * About us.
     *
     * @param event the event
     */
    @FXML
    void aboutUs(ActionEvent event) {
        Alert alert = AlertUI.showAlert(Alert.AlertType.INFORMATION, "About",
                "Development Team:", "Bento Martins - 1210089@isep.ipp.pt\n" +
                        "Delfim Costa - 1081348@isep.ipp.pt\n" +
                        "Mário Borja - 1200586@isep.ipp.pt\n" +
                        "Sérgio Soares - 1980292@isep.ipp.pt\n" +
                        "Tássio Gomes - 1170065@isep.ipp.pt\n" +
                        "Tiago Oliveira - 1211669@isep.ipp.pt");
        alert.show();
    }


    /**
     * Logout.
     *
     * @param event the event
     * @throws IOException the io exception
     */
    @FXML
    void logout(ActionEvent event) throws IOException {

        root = FXMLLoader.load(getClass().getResource("/fxml/LoginUI.fxml"));
        Stage stage = (Stage) menuBar.getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }


    /**
     * Exit.
     *
     * @param event the event
     */
    @FXML
    void exit(ActionEvent event) {

        Stage stage = (Stage) menuBar.getScene().getWindow();
        stage.close();

    }

}
