package app.ui.gui;

import app.controller.SerializationOnClose;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

/**
 * JavaFX App
 *
 * @author Bento Martins <1210089>
 */
public class App extends Application {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * The constant APP_TITLE.
     */
    public static final String APP_TITLE = "Pandemic Vaccination Management System";

    @Override
    public void start(Stage stage) throws IOException {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/LoginUI.fxml"));
            stage.setTitle(APP_TITLE);
            Scene scene = new Scene(root);
            stage.setScene(scene);

            stage.setOnCloseRequest(new EventHandler<WindowEvent>()
            {
                @Override
                public void handle (WindowEvent event){
                    Alert alert = AlertUI.showAlert(Alert.AlertType.CONFIRMATION, APP_TITLE,
                            "Confirm", "Exit application?");

                    if (alert.showAndWait().get() == ButtonType.CANCEL) {
                        event.consume();
                        SerializationOnClose serializationOnClose = new SerializationOnClose();
                    }
                }
            });
            stage.show();
        } catch(IOException ex){
            AlertUI.showAlert(Alert.AlertType.ERROR, APP_TITLE,
                    "Error running app.", ex.getMessage()).show();
        }
    }

    }



