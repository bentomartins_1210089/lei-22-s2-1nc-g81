package app.domain.store;

import app.domain.model.SNSUserArrival;
import app.domain.model.VaccinationCenter;

import java.util.ArrayList;

/**
 * @author Tiago Oliveira <1211669>
 */

public class WaitingRoomStore {

    /**
     * List of users waiting to be vaccinated at a given vaccination center
     */
    private ArrayList<SNSUserArrival> waitingRoomList;

    /**
     * Vaccination center
     */
    public VaccinationCenter vaccinationCenter;

    /**
     * Empty contructor initializes an waiting room store list
     */
    public WaitingRoomStore() {
        this.waitingRoomList = new ArrayList<>();
    }

    /**
     * Contructor initializes an waiting room store list
     * @param waitingRoomList List of users waiting to be vaccinated
     */
    public WaitingRoomStore(ArrayList<SNSUserArrival> waitingRoomList) {
        this.waitingRoomList = waitingRoomList;
    }

    /**
     * Store in list
     * @param registerUser Model information of the SNS user arrival
     */
    public void addWaitingUser(SNSUserArrival registerUser) {
        waitingRoomList.add(registerUser);
    }

    /**
     * Creates the arrival of a user to be vaccinated in store
     * @param snsNumber SNS user number
     * @param time      User arrival time
     * @param date      User arrival date
     * @return user arrival model
     */
    public SNSUserArrival createUserArrival(long snsNumber, String time, String date) {
        return new SNSUserArrival(snsNumber, time, date);
    }

    /**
     * Validates a duplicate in store
     * @param user Arrived SNS user
     * @return true if there are no duplicates, false otherwise
     */
    public boolean validateDuplicateUserArrival(SNSUserArrival user) {
        for (SNSUserArrival snsUser : waitingRoomList) {
            if (snsUser.getSnsNumber() == user.getSnsNumber())
                return false;
        }
        return true;
    }

    /**
     * Getter
     * @return List of users to be vaccinated
     */
    public ArrayList<SNSUserArrival> getWaitingRoomList() {
        return waitingRoomList;
    }

    public VaccinationCenter getVaccinationCenter() {
        return vaccinationCenter;
    }

}