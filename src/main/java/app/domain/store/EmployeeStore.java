package app.domain.store;

import app.domain.model.Employee;
import app.domain.model.SNSUser;

import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The type Employee store.
 */
public class EmployeeStore implements Serializable {

    /**
     * List of employees
     */
    private List<Employee> employeeList;

    /**
     * Constructor of the employee store
     *
     * @param list list
     */
    public EmployeeStore(List<Employee> list) {
        employeeList = list;
    }

    /**
     * Creates an employee
     *
     * @param phoneNumber      phone number
     * @param address          address
     * @param organizationRole organization role
     * @param name             name
     * @param email            email
     * @return an employee
     * @throws IllegalArgumentException illegal argument exception
     */
    public static Employee registerEmployee(String phoneNumber, String address, String cc, String organizationRole, String name, String email) throws IllegalArgumentException {
        return new Employee(phoneNumber, address, cc, organizationRole, name, email);
    }

    /**
     * Validates an employee checking if its null or is equal to any other employee
     *
     * @param employee employee
     * @return result boolean
     */
    public boolean validateEmployee(Employee employee) {
        if (employee == null || employee.getAddress() == null || employee.getPhoneNumber() == null || employee.getEmail() == null || employee.getName() == null || employee.getRole() == null) {
            return false;
        }
        for (Employee employee1 : employeeList) {
            if (employee1.equals(employee)) {
                return false;
            }
        }
        return !this.employeeList.contains(employee);
    }

    public List<Employee> getEmployeeListByRole(String role) {
        return this.employeeList
                .stream()
                .filter(employee -> Objects.equals(employee.getRole(), role))
                .collect(Collectors.toList());
    }

    public Employee createEmployee(String name, String address, String phoneNumber, String email, String cc, String role) {
        Employee employee = new Employee(name, address, phoneNumber, email, cc, role);
        this.addEmployee(employee);
        return employee;
    }

    /**
     * Exists boolean.
     *
     * @param employee employee
     * @return result boolean
     */
    public boolean existsEmployee(Employee employee) {
        return this.employeeList.contains(employee);
    }


    /**
     * Number of employees int.
     *
     * @return int
     */
    public int numberOfEmployees() {
        return employeeList.size();
    }


    /**
     * Add employee boolean.
     *
     * @param employee employee
     * @return boolean
     */
    public boolean addEmployee(Employee employee) {
        return this.employeeList.add(employee);
    }

    public List<Employee> getEmployeeList() {
        return this.employeeList;
    }

    public Employee getEmployeeByEmail(String email) {
        for (Employee employee : this.employeeList) {
            if (email == employee.getEmail())
                return employee;
        }
        return null;
    }

    public Employee findEmployee(String email) {

        for (Employee employee : employeeList ) {

            if (employee.getEmail().equals(email)) {
                return employee;
            }

        }
        return null;
    }
}
