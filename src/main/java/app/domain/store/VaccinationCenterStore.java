package app.domain.store;

import app.domain.model.*;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.util.*;

/**
 * The type Vaccination center store.
 */
public class VaccinationCenterStore {

    private final List<VaccineBase> vaccineBaseList = new ArrayList<>(); //US 1 and 2
    private final Set<VaccinationCenter> vaccinationCenterList = new HashSet();

    private final ArrayList<VaccinationCenter> vaccinationCenterList2 = new ArrayList<>();



    /**
     * Instantiates a new Vaccination center store.
     */
    public VaccinationCenterStore() {}

    /**
     * New vaccination center vaccination center.
     *
     * @param name              the name
     * @param address           the address
     * @param phone             the phone
     * @param fax               the fax
     * @param email             the email
     * @param website           the website
     * @param openingHours      the opening hours
     * @param closingHours      the closing hours
     * @param slot              the slot
     * @param maxNumberVaccines the max number vaccines
     * @param coordinator       the coordinator
     * @return the vaccination center
     */
    public VaccinationCenter newVaccinationCenter(String name, String address, String phone, String fax, Email email,
                                                  String website, String openingHours, String closingHours, String slot,
                                                  int maxNumberVaccines, Employee coordinator) {

        return new VaccinationCenter(name, address, phone, fax, email, website, openingHours, closingHours, slot, maxNumberVaccines, coordinator);
    }

    /**
     * New vaccination center vaccination center.
     *
     * @param vaccinationCenter the vaccination center
     * @return the vaccination center
     */
    public VaccinationCenter newVaccinationCenter(VaccinationCenter vaccinationCenter) {return new VaccinationCenter(vaccinationCenter);}

    /**
     * Add boolean.
     *
     * @param vaccinationCenter the vaccination center
     * @return the boolean
     */
    public boolean add2(VaccinationCenter vaccinationCenter) {
        if (vaccinationCenter != null && !this.exists(vaccinationCenter)) {
            this.vaccinationCenterList2.add(vaccinationCenter);
            return true;
        }
        return false;
    }

    /**
     * Add boolean.
     *
     * @param vaccinationCenter the vaccination center
     * @return the boolean
     */
    public boolean add(VaccinationCenter vaccinationCenter) {
        if (vaccinationCenter != null && !this.exists(vaccinationCenter)) {
            this.vaccinationCenterList.add(vaccinationCenter);
            return true;
        }
        return false;
    }

    /**
     * Remove boolean.
     *
     * @param vaccinationCenter the vaccination center
     * @return the boolean
     */
    public boolean remove(VaccinationCenter vaccinationCenter) {
        return vaccinationCenter != null && this.vaccinationCenterList.remove(vaccinationCenter);
    }

    /**
     * Gets all.
     *
     * @return the all
     */
    public Set<VaccinationCenter> getAll() {
        return Collections.unmodifiableSet(this.vaccinationCenterList);
    }

    /**
     * Gets by id.
     *
     * @param email the email
     * @return the by id
     */
    public Optional<VaccinationCenter> getById(String email) {
        return this.getById(new Email(email));
    }

    /**
     * Gets by id.
     *
     * @param email the email
     * @return the by id
     */
    public Optional<VaccinationCenter> getById(Email email) {
        Iterator var2 = this.vaccinationCenterList.iterator();

        VaccinationCenter vaccinationCenter;
        do {
            if (!var2.hasNext()) {
                return Optional.empty();
            }

            vaccinationCenter = (VaccinationCenter)var2.next();
        } while(!vaccinationCenter.hasId(email));

        return Optional.of(vaccinationCenter);
    }

    /**
     * Exists boolean.
     *
     * @param email the email
     * @return the boolean
     */
    public boolean exists(String email) {
        Optional<VaccinationCenter> result = this.getById(email);
        return result.isPresent();
    }

    /**
     * Exists boolean.
     *
     * @param email the email
     * @return the boolean
     */
    public boolean exists(Email email) {
        Optional<VaccinationCenter> result = this.getById(email);
        return result.isPresent();
    }

    /**
     * Exists boolean.
     *
     * @param vaccinationCenter the vaccination center
     * @return the boolean
     */
    public boolean exists(VaccinationCenter vaccinationCenter) {
        return this.vaccinationCenterList.contains(vaccinationCenter);
    }

    /**
     * Instantiates a new Vaccination center store.
     *
     * @param vaccineBaseList the vaccine base list
     */
    public VaccinationCenterStore(List<VaccineBase> vaccineBaseList) {
        this.vaccineBaseList.clear(); 
        this.vaccineBaseList.addAll(vaccineBaseList);
    }

    /**
     * @author Tassio Gomes <1170065>
     * @author Mário Borja <1200586>
     */

    /**
     * Get list of all vaccines.
     *
     * @return the list
     */
    public List<VaccineBase> getAllVacs(){
        return vaccineBaseList;
    }

    /**
     * Create vaccine base model.
     *
     * @param snsUser           the sns user
     * @param vaccineType       the vaccine type
     * @param vaccinationCenter the vaccination center
     * @return the vaccine base
     */
    public VaccineBase createVaccine(SNSUser snsUser, VaccineType vaccineType, VaccinationCenter vaccinationCenter){
        return new VaccineBase(snsUser, vaccineType, vaccinationCenter);
    }

    /**
     * Add vaccine.
     *
     * @param vaccineBase the vaccine base
     */
    public void addVaccine(VaccineBase vaccineBase){
        if(validateVaccine(vaccineBase)){
            vaccineBaseList.add(vaccineBase);
        }
    }

    /**
     * Validate vaccine boolean.
     *
     * @param vaccine the vaccine
     * @return the boolean
     */
    public boolean validateVaccine(VaccineBase vaccine)
    {
        assert vaccine != null;
        if(vaccine.getSnsUser() == null || vaccine.getVaccineType() == null || vaccine.getVaccinationCenter() == null
                || vaccine.getInternalCode() == null)
        {
            return false;
        }
        for (VaccineBase vaccine1 : vaccineBaseList)
        {
            if (vaccine.equals(vaccine1))
            {
                return false;
            }
        }
        return !this.vaccineBaseList.contains(vaccine);
    }

    /**
     * Select user sns user.
     *
     * @param userNr      the user nr
     * @param snsUserList the sns user list
     * @return the sns user
     */
    public SNSUser selectUser(long userNr, List<SNSUser> snsUserList )
    {
        for (SNSUser user1 : snsUserList)
        {
            if (user1.getSnsNumber() == (userNr))
            {
                return user1;
            }
        }
        return null;
    }

    /**
     * Show client string.
     *
     * @param snsUser the sns user
     * @return the string
     */
    public String showClient(SNSUser snsUser){
        return snsUser.toString();
    }

    /**
     * Get all vac centers list.
     *
     * @return the list
     */
    public List<VaccinationCenter> getAllVacCenters(){
        return new ArrayList<>(vaccinationCenterList);
    }

}