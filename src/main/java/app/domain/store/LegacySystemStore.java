package app.domain.store;

import app.domain.model.LegacySystemData;

import java.util.*;

/**
 * @author Tiago Oliveira <1211669>
 */

public class LegacySystemStore{

    /**
     * Legacy system vaccinated users list
     */
    private ArrayList<LegacySystemData> legacySysList;

    /**
     * Empty constructor initializes an legacy system store
     */
    public LegacySystemStore() {
        this.legacySysList = new ArrayList<>();
    }

    /**
     * Constructor initializes an legacy system store
     * @param legacySystemList Legacy system vaccinated users list
     */
    public LegacySystemStore (ArrayList<LegacySystemData> legacySystemList) {
        this.legacySysList = legacySystemList;
    }

    /**
     * Store new user in list
     * @param user Legacy system user data
     * @return true if user was added, false otherwise
     */
    public boolean addUser(LegacySystemData user) {
        return legacySysList.add(user);
    }

    /**
     * Remove user from store
     * @param user Legacy system user data
     * @return true if user was removed, false otherwise
     */
    public boolean removeUser(LegacySystemData user) {
        return legacySysList.remove(user);
    }

    /**
     *
     * Validates a duplicate user in list
     * @param list Legacy system list
     * @return true if there are no duplicates, false otherwise
     */
    public boolean validateDuplicate(List<LegacySystemData> list) {
        Set<String> set = new HashSet<>();

        for (LegacySystemData user: list) {
            String userInfo = String.format("%s %s %s", user.getSNSNUmber(), user.getVaccine(), user.getDose());
            if (!set.add(userInfo))
                return false;
        }
        return true;
    }

    /**
     * Validates a duplicate user in store
     * @param checkUser Legacy system user
     * @return true if there are no duplicates, false otherwise
     */
    public boolean validateDuplicate(LegacySystemData checkUser) {
        for (LegacySystemData user: legacySysList) {
            String userInfo = String.format("%s %s %s", user.getSNSNUmber(), user.getVaccine(), user.getDose());
            String checkUserInfo = String.format("%s %s %s", checkUser.getSNSNUmber(), checkUser.getVaccine(), checkUser.getDose());

            if (userInfo.equals(checkUserInfo))
                return false;
        }
        return true;
    }

    /**
     * Getter
     * @return Legacy system list
     */
    public ArrayList<LegacySystemData> getLegacySystemStore() {
        return legacySysList;
    }

    /**
     * Setter
     * @param legacySysList Legacy system vaccinated users list
     */
    public void setLegacySysList(ArrayList<LegacySystemData> legacySysList) {
        this.legacySysList = legacySysList;
    }
}