package app.domain.store;

import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineAdministrationNurse;
import app.domain.model.VaccineType;
import app.dto.VaccineAdministrationNurseDTO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Vaccine administration nurse store.
 * @Mario Borja 1200586
 */
public class VaccineAdministrationNurseStore {
    private List<VaccineAdministrationNurse> vaccineAdministrationNurseList;

    /**
     * Instantiates a new Vaccine administration nurse store.
     */
    public VaccineAdministrationNurseStore() {
        this.vaccineAdministrationNurseList = new ArrayList<>();
    }

    /**
     * Gets vaccine administration by sns number.
     *
     * @param snsNumber   the sns number
     * @param vaccineType the vaccine type
     * @return the vaccine administration by sns number
     */
    public VaccineAdministrationNurse getVaccineAdministrationBySnsNumber(long snsNumber, VaccineType vaccineType) {
        for (int i = 0; i < vaccineAdministrationNurseList.size(); i++) {
            if (Objects.equals(snsNumber, vaccineAdministrationNurseList.get(i).getSnsNumber()) && vaccineType.equals(vaccineAdministrationNurseList.get(i).getVaccineBase().getVaccineType())) {
                return vaccineAdministrationNurseList.get(i);
            }
        }
        return null;
    }

    /**
     * Create vaccine administration nurse.
     *
     * @param vaccineAdministrationNurseDTO the vaccine administration nurse dto
     * @return the vaccine administration nurse
     */
    public VaccineAdministrationNurse create(VaccineAdministrationNurseDTO vaccineAdministrationNurseDTO) {
        return new VaccineAdministrationNurse(vaccineAdministrationNurseDTO.getSnsNumber(), vaccineAdministrationNurseDTO.getVaccine(), vaccineAdministrationNurseDTO.getVaccinationCenter(), vaccineAdministrationNurseDTO.getLocalDateTime(), vaccineAdministrationNurseDTO.getLotNumber(), vaccineAdministrationNurseDTO.getDoseNumber(), vaccineAdministrationNurseDTO.getEmail());
    }

    /**
     * Validate boolean.
     *
     * @param vaccineAdministrationNurse the vaccine administration nurse
     * @return the boolean
     */
    public boolean validate(VaccineAdministrationNurse vaccineAdministrationNurse) {
        if (vaccineAdministrationNurse == null) {
            return false;
        }
        return !vaccineAdministrationNurseList.contains(vaccineAdministrationNurse);
    }

    /**
     * Save boolean.
     *
     * @param vaccineAdministrationNurse the vaccine administration nurse
     * @return the boolean
     */
    public boolean save(VaccineAdministrationNurse vaccineAdministrationNurse) {
        if (validate(vaccineAdministrationNurse)) {
            return add(vaccineAdministrationNurse);
        } else {
            throw new IllegalArgumentException("Vaccine administration nurse invalid");
        }
    }

    /**
     * Add boolean.
     *
     * @param vaccineAdministration the vaccine administration
     * @return the boolean
     */
    public boolean add(VaccineAdministrationNurse vaccineAdministration) {
        if (vaccineAdministration == null) {
            return false;
        }
        return vaccineAdministrationNurseList.add(vaccineAdministration);
    }

    /**
     * Gets dose details list.
     *
     * @param dosesTaken the doses taken
     * @param dose       the dose
     * @return the dose details list
     */
    public List<Integer> getDoseDetailsList(int dosesTaken, int dose) {
        List<Integer> doseNumbersList = new ArrayList<>();
        for (int i = dosesTaken + 1; i <= dose; i++) {
            doseNumbersList.add(i);
        }
        return doseNumbersList;
    }

    /**
     * Gets vaccine administration list.
     *
     * @return the vaccine administration list
     */
    public List<VaccineAdministrationNurse> getVaccineAdministrationList() {
        return this.vaccineAdministrationNurseList;
    }

    /**
     * Gets vaccinated by date interval by center.
     *
     * @param vaccinationCenter the vaccination center
     * @param timeIni           the time ini
     * @param timeEnd           the time end
     * @return the vaccinated by date interval by center
     */
    public ArrayList<VaccineAdministrationNurse> getVaccinatedByDateIntervalByCenter(VaccinationCenter vaccinationCenter, LocalDate timeIni, LocalDate timeEnd)
    {

        ArrayList<VaccineAdministrationNurse> administratedBetweenDatesInCenter = new ArrayList<>();

        for (VaccineAdministrationNurse vacAdmin : vaccineAdministrationNurseList) {

            LocalDate adminTime = vacAdmin.getLocalDateTime().toLocalDate();

            if (adminTime.isAfter(timeIni.minusDays(1)) && adminTime.isBefore(timeEnd.plusDays(1)) && vaccinationCenter.getName().equals(vacAdmin.getVaccinationCenter().getName())) {

                administratedBetweenDatesInCenter.add(vacAdmin);

            }

        }

        return administratedBetweenDatesInCenter;
    }

    /**
     * Get full doses by date from administrations array list.
     *
     * @param administrationsBetweenDatesInCenter the administrations between dates in center
     * @param timeIni                             the time ini
     * @param timeEnd                             the time end
     * @return the array list
     */
    public ArrayList<String> getFullDosesByDateFromAdministrations
            (ArrayList < VaccineAdministrationNurse > administrationsBetweenDatesInCenter, LocalDate timeIni, LocalDate
                    timeEnd){

        ArrayList<String> content = new ArrayList<>();

        LocalDate next = timeIni.minusDays(1);
        while ((next = next.plusDays(1)).isBefore(timeEnd.plusDays(1))) {
            int count = 0;

            for (VaccineAdministrationNurse vacAdmin : administrationsBetweenDatesInCenter) {

                if (vacAdmin.getLocalDateTime().toLocalDate().equals(next)) {

                    if (vacAdmin.getDoseNumber().equals(vacAdmin.getVaccineAdministration().getNumDoses())) {

                        count++;
                    }

                }

            }

            content.add(next.toString() + "," + count + "\n");

        }

        return content;
    }


}
