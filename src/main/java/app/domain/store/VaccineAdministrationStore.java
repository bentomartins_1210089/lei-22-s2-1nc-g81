
package app.domain.store;

import app.domain.model.VaccineAdministration;

import java.util.ArrayList;

/**
 * @author Tiago Oliveira <1211669>
 */

public class VaccineAdministrationStore {

    /**
     * List of admin processes
     */
    private ArrayList<VaccineAdministration> adminProcessList;

    /**
     * Contructor initializes an admin process store list
     */
    public VaccineAdministrationStore()
    {
        adminProcessList = new ArrayList<>();
    }

    /**
     * Contructor initializes an admin process store list
     * @param adminProcessList List of admin processes
     */
    public VaccineAdministrationStore(ArrayList<VaccineAdministration> adminProcessList)
    {
        this.adminProcessList = adminProcessList;
    }


    /**
     * Store in list
     * @param adminProcessData Object with all information provided for the administration process
     */
    public void addAdminProcess(VaccineAdministration adminProcessData) {
        adminProcessList.add(adminProcessData);
    }

    /**
     * Creates a vaccine administration process in store
     *
     * @param vaccine           vaccine
     * @param ageGroup          age group
     * @param numDoses          number of doses
     * @param dosages           dosages (mL)
     * @param dosingIntervals   recovery intervals for each dose (days)
     * @return model
     */
    public VaccineAdministration createVaccineAdmin(String vaccine, String ageGroup, int numDoses, ArrayList<Double> dosages, ArrayList<Integer> dosingIntervals) {
        VaccineAdministration vaccineAdmin = new VaccineAdministration(vaccine, ageGroup, numDoses, dosages, dosingIntervals);
        return vaccineAdmin;
    }

    /**
     * Validates a duplicate in store
     * @param vaccineAdmin administration process
     * @return true if there are no duplicates, false otherwise
     */
    public boolean validateVaccineAdmin(VaccineAdministration vaccineAdmin) {
        for (VaccineAdministration vcAdmin : adminProcessList) {
            if (vcAdmin.equals(vaccineAdmin)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Getter
     * @return All administration processes list
     */
    public ArrayList<VaccineAdministration> getAdminProcessList() {
        return adminProcessList;
    }
}