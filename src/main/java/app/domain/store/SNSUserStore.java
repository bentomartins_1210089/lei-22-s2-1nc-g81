package app.domain.store;
import app.domain.model.SNSUser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * The SNS User store.
 *
 * @author Bento Martins <1210089>
 */
public class SNSUserStore implements Serializable {

    /**
     * The SNS User list.
     */
    private ArrayList<SNSUser> snsUserList;

    /**
     * Instantiates a new SNS User store.
     */
    public SNSUserStore() {
        this.snsUserList = new ArrayList<>();
    }

    /**
     * Gets SNS User list.
     *
     * @return the SNS user list
     */
    public ArrayList<SNSUser> getSnsUserList() {
        return snsUserList;
    }

    /**
     * Sets SNS User list.
     *
     * @param snsUserList the SNS User list
     */
    public void setSnsUserList(ArrayList<SNSUser> snsUserList) {
        this.snsUserList = snsUserList;
    }


    /**
     * Add SNS User.
     *
     * @param snsuser an SNS User
     */
    public void addSNSUserToStore(SNSUser snsuser){
        snsUserList.add(snsuser);
    }

    /**
     * Creates a SNS User with sex.
     *
     * @param nameUser     the name of SNS User
     * @param birthDate    the birth date of SNS User
     * @param snsNumber    the SNS number of SNS User
     * @param phoneNumber  the Phone number of SNS User
     * @param emailAddress the e-mail address of SNS User
     * @param ccNumber     the Citizen Card number of SNS User
     * @param userAddress  the address of SNS User
     * @param sexUser      the sex of SNS User
     * @return a SNS User
     */
    public SNSUser createSNSUser(String nameUser, Date birthDate, long snsNumber, long phoneNumber, String emailAddress, long ccNumber, String userAddress, String sexUser) {

        SNSUser snsuser = new SNSUser(nameUser,birthDate,snsNumber,phoneNumber,emailAddress,ccNumber,userAddress,sexUser);

        return snsuser;
    }

    /**
     * Creates a SNS User without sex.
     *
     * @param nameUser     the name of SNS User
     * @param birthDate    the birth date of SNS User
     * @param snsNumber    the SNS number of SNS User
     * @param phoneNumber  the Phone number of SNS User
     * @param emailAddress the e-mail address of SNS User
     * @param ccNumber     the Citizen Card number of SNS User
     * @param userAddress  the address of SNS User
     * @return a SNS User
     */
    public SNSUser createSNSUser(String nameUser, Date birthDate, long snsNumber, long phoneNumber, String emailAddress, long ccNumber, String userAddress) {

        SNSUser snsuser = new SNSUser(nameUser,birthDate,snsNumber,phoneNumber,emailAddress,ccNumber,userAddress);

        return snsuser;
    }

    /**
     * Validate if a SNS User already exists in store.
     *
     * @param snsuser an SNS User
     * @return a boolean. True if a SNS User doesn't exist, False if a SNS User already exists
     */
    public boolean existsSNSUserStore(SNSUser snsuser) {
        for (SNSUser newuser : snsUserList ) {
            if (newuser.getEmail().equals(snsuser.getEmail()) && newuser.getPhone() == snsuser.getPhone() && newuser.getCcNumber() == snsuser.getCcNumber() && newuser.getSnsNumber() == snsuser.getSnsNumber()) {
                return false;
            }
        }
        return true;
    }

    //Mário Borja 1200586

    /**
     * Checks SNS user number existance in Store
     *
     * @param snsNumber SNS user number
     * @return True if user exists in store. False otherwise
     */
    public boolean existsSNSUserStore(long snsNumber) {
        for (SNSUser user : snsUserList) {
            if (user.getSnsNumber() == snsNumber)
                return true;
        }
        return false;
    }


    /**
     * Get sns user name name by sns number.
     *
     * @param snsNumber sns user number
     * @return name of user
     */
    public String getNameBySnsNumber(long snsNumber){
        String name = null;
        for (SNSUser user : snsUserList) {
            if (Objects.equals(user.getSnsNumber(), snsNumber)){
                name = user.getName();
            }
            return name;
        }

        return name;
    }

    /**
     * Get SNSUser by sns number.
     * @param snsNumber sns number
     * @return SNSUser
     */
    public SNSUser getSNSUserByNumber(long snsNumber) {
        for (SNSUser snsUser: snsUserList) {
            if (snsUser.getSnsNumber() == snsNumber)
                return snsUser;
        }
        return null;
    }
}
