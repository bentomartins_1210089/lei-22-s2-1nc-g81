
package app.domain.store;

import app.domain.model.VaccineType;

import java.util.ArrayList;

/**
 * @author Tiago Oliveira <1211669>
 */

public class VaccineTypeStore {

    private ArrayList<VaccineType> vcTypeList;

    public VaccineTypeStore()
    {
        vcTypeList = new ArrayList<>();
    }

    public VaccineTypeStore(ArrayList<VaccineType> vcTypeList)
    {
        this.vcTypeList = vcTypeList;
    }

    public void addVcType(VaccineType vcType) {
        vcTypeList.add(vcType);
    }

    public boolean validateVcType(VaccineType vcType) {
        for (VaccineType vcTypeValidate : vcTypeList) {
            if (vcTypeValidate.equals(vcType)) {
                return false;
            }
        }
        return true;
    }

    public ArrayList<VaccineType> getVcTypeList() {
        return vcTypeList;
    }

    /**
     * Checks vaccine existance in Store
     *
     * @param vcName Vaccine name
     * @return True if vaccine exists in store. False otherwise
     */
    public boolean existsVaccineStore(String vcName) {
        for (VaccineType vcType : vcTypeList) {
            if (vcType.getVaccineName().equals(vcName))
                return true;
        }
        return false;
    }
}