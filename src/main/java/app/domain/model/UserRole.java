package app.domain.model;


/**
 * @author Tassio Gomes <1170065>
 */

public class UserRole {

    /**
     * Class attributes
     */

    private int idRole;
    private String description;

    /**
     * Default attributes
     */

    private static final int DEFAULT_ID_ROLE = 0;
    public static final String DEFAULT_DESCRIPTION_ROLE = "No_Description";


    /**
     * Constructors
     */
    public UserRole() {
        this.idRole = DEFAULT_ID_ROLE;
        this.description = DEFAULT_DESCRIPTION_ROLE;
    }


    public UserRole(int idRole, String description) {
        this.idRole = idRole;
        this.description = description;
    }

    /**
     * Getters ans Setters
     */

    public int getIdRole() {
        return idRole;
    }

    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "idRole=" + idRole +
                ", description='" + description + '\'' +
                '}';
    }
}