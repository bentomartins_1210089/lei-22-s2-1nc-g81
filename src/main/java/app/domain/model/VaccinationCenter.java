package app.domain.model;

import app.controller.App;
import app.domain.shared.Enums.EmployeeRole;
import app.domain.store.WaitingRoomStore;
import org.apache.commons.lang3.StringUtils;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.Serializable;

/**
 * The type Vaccination center.
 *
 * @author aluno nº 1980292
 */
public class VaccinationCenter implements Serializable {

    private int id;
    private String name;
    private String address;
    private String phone;
    private String fax;
    private String website;
    private String openingHours;
    private String closingHours;
    private String slot;
    private int maxNumberVaccines;
    transient Email email;
    private Employee coordinator;

    public VaccinationCenter(String name, String address, String phone, String openingHours, String closingHours, String slot, int maxNumberVaccines, Email email) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.openingHours = openingHours;
        this.closingHours = closingHours;
        this.slot = slot;
        this.maxNumberVaccines = maxNumberVaccines;
        this.email = email;
    }

    private WaitingRoomStore waitingRoom;

    /**
     * Total number of vaccination centers.
     */
    private static int totalCenters = 1;

    /**
     * The constant NAME_BY_OMISSION.
     */
    protected static final String NAME_BY_OMISSION = "no name";
    /**
     * The constant ADDRESS_BY_OMISSION.
     */
    protected static final String ADDRESS_BY_OMISSION = "no address";
    /**
     * The constant PHONE_BY_OMISSION.
     */
    protected static final String PHONE_BY_OMISSION = "no phone";
    /**
     * The constant EMAIL_BY_OMISSION.
     */
    protected static final Email EMAIL_BY_OMISSION = null;
    /**
     * The constant FAX_BY_OMISSION.
     */
    protected static final String FAX_BY_OMISSION = "no fax";
    /**
     * The constant WEBSITE_BY_OMISSION.
     */
    protected static final String WEBSITE_BY_OMISSION = "no website";
    /**
     * The constant OPENING_HOURS_BY_OMISSION.
     */
    protected static final String OPENING_HOURS_BY_OMISSION = "no opening hours";
    /**
     * The constant CLOSING_HOURS_BY_OMISSION.
     */
    protected static final String CLOSING_HOURS_BY_OMISSION = "no closing hours";
    /**
     * The constant SLOT_BY_OMISSION.
     */
    protected static final String SLOT_BY_OMISSION        = "no slot";
    /**
     * The constant MAX_SLOTS_BY_OMISSION.
     */
    protected static final int MAX_VACINES_BY_OMISSION = 0;
    /**
     * The constant COORDINATOR_BY_OMISSION.
     */
    protected static final Employee COORDINATOR_BY_OMISSION = null;

    /**
     * Instantiates a new Vaccination center.
     *
     * @param name              the name
     * @param address           the address
     * @param phone             the phone
     * @param fax               the fax
     * @param email             the email
     * @param website           the website
     * @param openingHours      the opening hours
     * @param closingHours      the closing hours
     * @param slot              the slot
     * @param maxNumberVaccines the max number vaccines
     */
    public VaccinationCenter(String name, String address, String phone, String fax, Email email, String website,
                             String openingHours, String closingHours, String slot, int maxNumberVaccines) {
        setID(totalCenters);
        setName(name);
        setAddress(address);
        setPhone(phone);
        setFax(fax);
        setEmail(email);
        setWebsite(website);
        setOpeningHours(openingHours);
        setClosingHours(closingHours);
        setSlot(slot);
        setMaxNumberVaccines(maxNumberVaccines);

        waitingRoom = App.getInstance().getCompany().getWaitingRoomStore();

        totalCenters++;
    }

    /**
     * Instantiates a new Vaccination center.
     *
     * @param name              the name
     * @param address           the address
     * @param phone             the phone
     * @param fax               the fax
     * @param email             the email
     * @param website           the website
     * @param openingHours      the opening hours
     * @param closingHours      the closing hours
     * @param slot              the slot
     * @param maxNumberVaccines the max number vaccines
     * @param coordinator       the coordinator
     */
    public VaccinationCenter(String name, String address, String phone, String fax, Email email, String website,
                             String openingHours, String closingHours, String slot, int maxNumberVaccines,
                             Employee coordinator) {
        this(name, address, phone, fax, email, website, openingHours, closingHours, slot, maxNumberVaccines);
        setCoordinator(coordinator);

        waitingRoom = App.getInstance().getCompany().getWaitingRoomStore();
    }

    /**
     * Instantiates a new Vaccination center.
     */
    public VaccinationCenter() {

        this(VaccinationCenter.NAME_BY_OMISSION,
             VaccinationCenter.ADDRESS_BY_OMISSION,
             VaccinationCenter.PHONE_BY_OMISSION,
             VaccinationCenter.FAX_BY_OMISSION,
             VaccinationCenter.EMAIL_BY_OMISSION,
             VaccinationCenter.WEBSITE_BY_OMISSION,
             VaccinationCenter.OPENING_HOURS_BY_OMISSION,
             VaccinationCenter.CLOSING_HOURS_BY_OMISSION,
             VaccinationCenter.SLOT_BY_OMISSION,
             VaccinationCenter.MAX_VACINES_BY_OMISSION,
             VaccinationCenter.COORDINATOR_BY_OMISSION);

        waitingRoom = App.getInstance().getCompany().getWaitingRoomStore();
    }

    /**
     * Instantiates a new Vaccination center.
     *
     * @param vaccinationCenter the vaccination center
     */
    public VaccinationCenter(VaccinationCenter vaccinationCenter) {
        this(vaccinationCenter.getName(),
             vaccinationCenter.getAddress(),
             vaccinationCenter.getPhone(),
             vaccinationCenter.getFax(),
             vaccinationCenter.getEmail(),
             vaccinationCenter.getWebsite(),
             vaccinationCenter.getOpeningHours(),
             vaccinationCenter.getClosingHours(),
             vaccinationCenter.getSlot(),
             vaccinationCenter.getMaxNumberVaccines(),
             vaccinationCenter.getCoordinator());

        waitingRoom = App.getInstance().getCompany().getWaitingRoomStore();
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Gets phone.
     *
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public Email getEmail() {
        return email;
    }

    /**
     * Gets fax.
     *
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * Gets website.
     *
     * @return the website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * Gets opening hours.
     *
     * @return the opening hours
     */
    public String getOpeningHours() {
        return openingHours;
    }

    /**
     * Gets closing hours.
     *
     * @return the closing hours
     */
    public String getClosingHours() {
        return closingHours;
    }

    /**
     * Gets slot.
     *
     * @return the slot
     */
    public String getSlot() {
        return slot;
    }

    /**
     * Gets max number vaccines.
     *
     * @return the max number vaccines
     */
    public int getMaxNumberVaccines() {
        return maxNumberVaccines;
    }

    /**
     * Gets coordinator.
     *
     * @return the coordinator
     */
    public Employee getCoordinator() {
        return coordinator;
    }

    /**
     * Gets waiting room.
     *
     * @return the waiting room
     */
    public WaitingRoomStore getWaitingRoom() {
        return this.waitingRoom;
    }

    /**
     * Gets total centers.
     *
     * @return the total centers
     */
    public static int getTotalCenters() {
        return totalCenters - 1;
    }

    /**
     * Has id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    public boolean hasId(Email id) {
        return this.email.equals(id);
    }

    private void setID(int id) {
        this.id = this.totalCenters;
    }

    /**
     * Sets name.
     *
     * @param name the name
     * @return the name
     */
    public boolean setName(String name) {
        if (StringUtils.isBlank(name)) {
            this.name = VaccinationCenter.NAME_BY_OMISSION;
            return false;
        }

        this.name = name.trim();
        return true;
    }

    /**
     * Sets address.
     *
     * @param address the address
     * @return the address
     */
    public boolean setAddress(String address) {
        if (StringUtils.isBlank(address)) {
            this.address = VaccinationCenter.ADDRESS_BY_OMISSION;
            return false;
        }

        this.address = address.trim();
        return true;

    }

    /**
     * Sets phone.
     *
     * @param phone the phone
     * @return the phone
     */
    public boolean setPhone(String phone) {
        if (StringUtils.isBlank(phone)) {
            this.phone = VaccinationCenter.PHONE_BY_OMISSION;
            return false;
        }

        this.phone = phone.trim();
        return true;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(Email email) {
        this.email = email;
    }

    /**
     * Sets fax.
     *
     * @param fax the fax
     * @return the fax
     */
    public boolean setFax(String fax) {
        if (StringUtils.isBlank(fax)) {
            this.fax = VaccinationCenter.FAX_BY_OMISSION;
            return false;
        }

        this.fax = fax.trim();
        return true;
    }

    /**
     * Sets website.
     *
     * @param website the website
     * @return the website
     */
    public boolean setWebsite(String website) {
        if (StringUtils.isBlank(website)) {
            this.website = VaccinationCenter.WEBSITE_BY_OMISSION;
            return false;
        }

        this.website = website.trim();
        return true;
    }

    /**
     * Sets opening hours.
     *
     * @param openingHours the opening hours
     * @return the opening hours
     */
    public boolean setOpeningHours(String openingHours) {
        if (StringUtils.isBlank(openingHours)) {
            this.openingHours = VaccinationCenter.OPENING_HOURS_BY_OMISSION;
            return false;
        }

        this.openingHours = openingHours.trim();
        return true;
    }

    /**
     * Sets closing hours.
     *
     * @param closingHours the closing hours
     * @return the closing hours
     */
    public boolean setClosingHours(String closingHours) {
        if (StringUtils.isBlank(closingHours)) {
            this.closingHours = VaccinationCenter.CLOSING_HOURS_BY_OMISSION;
            return false;
        }

        this.closingHours = closingHours.trim();
        return true;

    }

    /**
     * Sets slot.
     *
     * @param slot the slot
     * @return the slot
     */
    public boolean setSlot(String slot) {
        if (StringUtils.isBlank(slot)) {
            this.slot = VaccinationCenter.CLOSING_HOURS_BY_OMISSION;
            return false;
        }

        this.name = name.trim();
        return true;
    }

    /**
     * Sets max number vaccines.
     *
     * @param maxNumberVaccines the max number vaccines
     * @return the max number vaccines
     */
    public boolean setMaxNumberVaccines(int maxNumberVaccines) {
        if (maxNumberVaccines <= 0) {
            this.maxNumberVaccines = VaccinationCenter.MAX_VACINES_BY_OMISSION;
            return false;
        }

        this.maxNumberVaccines = maxNumberVaccines;
        return true;
    }

    /**
     * Sets coordinator.
     *
     * @param coordinator the coordinator
     * @return the coordinator
     */
    public boolean setCoordinator(Employee coordinator) {

        if(coordinator != null && coordinator.getRole().equals(EmployeeRole.ROLE_CENTER_COORDINATOR.value())) {
            this.coordinator = coordinator;
            return true;
        }

        this.coordinator = null;
        return false;
    }

    /**
     * Sets waiting room.
     *
     * @param waitingRoom the waiting room
     * @return the waiting room
     */
    public boolean setWaitingRoom(WaitingRoomStore waitingRoom) {

        if(waitingRoom != null) {
            this.waitingRoom = waitingRoom;
            return true;
        }

        this.waitingRoom = null;
        return false;
    }

    /**
     * Sets waiting room user.
     *
     * @param snsUserArrival a new SNS user arrivel
     * @return the SNSUserArrival
     */
    public boolean setNewSNSUserArrival(SNSUserArrival snsUserArrival) {

        if(snsUserArrival != null) {
            this.waitingRoom.addWaitingUser(snsUserArrival);
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return "VacinationCenter{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", fax='" + fax + '\'' +
                ", website='" + website + '\'' +
                ", openingHours='" + openingHours + '\'' +
                ", closingHours='" + closingHours + '\'' +
                ", slot='" + slot + '\'' +
                ", maxNumberVaccines=" + maxNumberVaccines +
                '}';
    }

    @Override
    public VaccinationCenter clone() {
        return new VaccinationCenter(this);
    }

    @Override
    public boolean equals(Object vaccinationCenterObject) {
        if (this == vaccinationCenterObject) return true;
        if (vaccinationCenterObject == null || getClass() != vaccinationCenterObject.getClass()) return false;
        VaccinationCenter otherVaccinationCenter = (VaccinationCenter) vaccinationCenterObject;
        return email == otherVaccinationCenter.email;
    }
}
