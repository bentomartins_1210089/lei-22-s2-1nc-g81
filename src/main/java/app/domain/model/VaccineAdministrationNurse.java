package app.domain.model;

import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;


/**
 * @Mario Borja 1200586
 */
public class VaccineAdministrationNurse implements Serializable {
    private long snsNumber;
    private VaccineType vaccine;
    private VaccineBase vaccineBase;
    private VaccinationCenter vaccinationCenter;
    private LocalDateTime localDateTime;
    private String lotNumber;
    private Integer doseNumber;
    private Email email;
    private ArrayList<Double> dosages;


    private VaccineAdministration vaccineAdministration;
    /**
     * The Validator class instantiation
     */
    Validator validator = new Validator();


    /**
     * Instantiates a new Vaccine administration nurse.
     *
     * @param snsNumber         the sns number
     * @param vaccine           the vaccine
     * @param vaccinationCenter the vaccination center
     * @param localDateTime     the local date time
     * @param lotNumber         the lot number
     * @param doseNumber        the dose number
     * @param email             the email
     */
    public VaccineAdministrationNurse(long snsNumber, VaccineType vaccine, VaccinationCenter vaccinationCenter, LocalDateTime localDateTime, String lotNumber, Integer doseNumber, Email email) {
        setSnsNumber(snsNumber);
        setVaccine(vaccine);
        setVaccinationCenter(vaccinationCenter);
        setLocalDateTime(localDateTime);
        setLotNumber(lotNumber);
        setDoseNumber(doseNumber);

    }

    /**
     * Instantiates a new Vaccine administration nurse.
     *
     * @param snsNumber             the sns number
     * @param vaccinationCenter     the vaccination center
     * @param localDateTime         the local date time
     * @param doseNumber            the dose number
     * @param vaccineAdministration the vaccine administration
     */
    public VaccineAdministrationNurse(Integer snsNumber, VaccinationCenter vaccinationCenter, LocalDateTime localDateTime, Integer doseNumber, VaccineAdministration vaccineAdministration) {
        setSnsNumber(snsNumber);
        setVaccinationCenter(vaccinationCenter);
        setLocalDateTime(localDateTime);
        setDoseNumber(doseNumber);
        setVaccineAdministration(vaccineAdministration);

    }

    /**
     * Instantiates a new Vaccine administration nurse with dosage values
     *
     * @param snsNumber         the sns number
     * @param vaccine           the vaccine
     * @param vaccinationCenter the vaccination center
     * @param lotNumber         the lot number
     * @param doseNumber        the dose number
     * @param dosages           the dosages
     */
    public VaccineAdministrationNurse(long snsNumber, VaccineType vaccine, VaccinationCenter vaccinationCenter, String lotNumber, Integer doseNumber, ArrayList<Double> dosages) {
        this.snsNumber = snsNumber;
        this.vaccine = vaccine;
        this.vaccinationCenter = vaccinationCenter;
        this.lotNumber = lotNumber;
        this.doseNumber = doseNumber;
        this.dosages = dosages;
    }

    /**
     * Sets sns number.
     *
     * @param snsNumber the sns number
     */
    public void setSnsNumber(long snsNumber) {
        validator.validateSnsNumber(snsNumber);
        this.snsNumber = snsNumber;
    }

    /**
     * Gets sns number.
     *
     * @return the sns number
     */
    public long getSnsNumber() {
        return snsNumber;
    }

    /**
     * Sets vaccine.
     *
     * @param vaccine the vaccine
     */
    public void setVaccine(VaccineType vaccine) {
        validator.validateVaccine(vaccine);
        this.vaccine = vaccine;
    }

    /**
     * Gets vaccine.
     *
     * @return the vaccine
     */
    public VaccineType getVaccine() {
        return vaccine;
    }

    /**
     * Gets vaccine base.
     *
     * @return the vaccine base
     */
    public VaccineBase getVaccineBase() {
        return vaccineBase;
    }

    /**
     * Sets vaccination center.
     *
     * @param vaccinationCenter the vaccination center
     */
    public void setVaccinationCenter(VaccinationCenter vaccinationCenter) {
        validator.validateVaccinationCenter(vaccinationCenter);
        this.vaccinationCenter = vaccinationCenter;
    }

    /**
     * Gets vaccination center.
     *
     * @return the vaccination center
     */
    public VaccinationCenter getVaccinationCenter() {
        return vaccinationCenter;
    }

    /**
     * Sets local date time.
     *
     * @param localDateTime the local date time
     */
    public void setLocalDateTime(LocalDateTime localDateTime) {
        validator.checkLocalDateTimeRules(localDateTime);
        this.localDateTime = localDateTime;
    }

    /**
     * Gets local date time.
     *
     * @return the local date time
     */
    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    /**
     * Sets lot number.
     *
     * @param lotNumber the lot number
     */
    public void setLotNumber(String lotNumber) {
        validator.checkLotNumber(lotNumber);
        this.lotNumber = lotNumber;
    }

    /**
     * Gets lot number.
     *
     * @return the lot number
     */
    public String getLotNumber() {
        return lotNumber;
    }

    /**
     * Sets dose number.
     *
     * @param doseNumber the dose number
     */
    public void setDoseNumber(Integer doseNumber) {
        this.doseNumber = doseNumber;
    }

    /**
     * Gets vaccine administration.
     *
     * @return the vaccine administration
     */
    public VaccineAdministration getVaccineAdministration() {
        return vaccineAdministration;
    }

    /**
     * Sets vaccine administration.
     *
     * @param vaccineAdministration the vaccine administration
     */
    public void setVaccineAdministration(VaccineAdministration vaccineAdministration) {
        this.vaccineAdministration = vaccineAdministration;
    }


    /**
     * Gets dose number.
     *
     * @return the dose number
     */
    public Integer getDoseNumber() {
        return doseNumber;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(Email email) {
        validator.validateEmail(email);
        this.email = email;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public Email getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return  "SNS number: " + snsNumber +
                "\nVaccine code: " + vaccine.getVaccineTypeCode() +
                "\nVaccination Center: " + vaccinationCenter.getName() +
                "\nDate: " + localDateTime.getDayOfMonth() + "-" + localDateTime.getMonth() + "-" + localDateTime.getYear() +
                "\nTime: " + localDateTime.getHour() + ":" + localDateTime.getMinute() +
                "\nLot number: " + lotNumber +
                "\nDose number: " + doseNumber +
                "\nNurse email: " + email;
    }
}
