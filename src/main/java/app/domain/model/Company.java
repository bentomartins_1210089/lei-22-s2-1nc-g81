package app.domain.model;

import app.controller.DeserializationOnBoot;
import app.controller.SerializationOnClose;
import app.domain.shared.Constants;
import app.domain.store.*;
import javafx.util.converter.LocalDateTimeStringConverter;
import pt.isep.lei.esoft.auth.AuthFacade;
import org.apache.commons.lang3.StringUtils;
import app.domain.store.EmployeeStore;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Company {

    private String designation;
    private AuthFacade authFacade;
    private EmployeeStore employeeStore;
    private SNSUserStore snsUserStore;
    private VaccineAdministrationNurseStore vaccineAdministrationNurseStore;
    private VaccineAdministrationStore vaccineAdministrationStore;
    private WaitingRoomStore waitingRoomStore;
    private VaccinationCenterStore vaccinationCenterStore;
    private LegacySystemStore legacySystemStore;
    private VaccineTypeStore vaccineTypeStore;

    public Company(String designation)
    {
        if (StringUtils.isBlank(designation))
            throw new IllegalArgumentException("Designation cannot be blank.");

        this.designation = designation;
        this.authFacade = new AuthFacade();
        this.snsUserStore = new SNSUserStore();
        this.vaccineAdministrationStore = new VaccineAdministrationStore();
        this.waitingRoomStore = new WaitingRoomStore();
        this.vaccinationCenterStore = new VaccinationCenterStore();
        this.legacySystemStore = new LegacySystemStore();
        this.vaccineTypeStore = new VaccineTypeStore();

        this.employeeStore = new EmployeeStore(new ArrayList<Employee>());
        this.vaccineAdministrationNurseStore = new VaccineAdministrationNurseStore();

        //Tassio Gomes
        bootstrap();

        DeserializationOnBoot deserializationOnBoot = new DeserializationOnBoot();
        deserializationOnBoot.DeserializationOnBootSNSUsers(this.snsUserStore, this.authFacade);
        deserializationOnBoot.DeserializationOnBootEmployees(this.employeeStore, this.authFacade);
        deserializationOnBoot.DeserializationOnBootVaccineType(this.vaccineTypeStore);
        deserializationOnBoot.DeserializationOnBootVaccineAdministration(this.vaccineAdministrationStore);
        deserializationOnBoot.DeserializationOnBootRegisterSNSUserArrival(this.waitingRoomStore);
        deserializationOnBoot.DeserializationOnBootVaccinationCenter(this.vaccinationCenterStore);
        deserializationOnBoot.DeserializationOnBootVaccineBase(this.vaccinationCenterStore);
        deserializationOnBoot.DeserializationOnBootVaccineAdministrationNurse(this.vaccineAdministrationNurseStore);
    }

    public EmployeeStore getEmployeeStore() { return employeeStore; }

    public void setEmployeeStore(EmployeeStore employeeStore) {
        this.employeeStore = employeeStore;
    }

    public SNSUserStore getSNSUserStore() { return snsUserStore; }

    public VaccineAdministrationStore getAdminProcessStore() {return vaccineAdministrationStore; }

    public WaitingRoomStore getWaitingRoomStore() { return waitingRoomStore; }

    public VaccinationCenterStore getVaccinationCenterStore() {return vaccinationCenterStore;}

    public LegacySystemStore getLegacySystemStore() {return legacySystemStore;}

    public VaccineTypeStore getVaccineTypeStore() {return vaccineTypeStore;}

    public String getDesignation() {
        return designation;
    }

    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    /**
     * Validate if a SNS User already exists in AuthFacade.
     *
     * @param snsuser an snsuser
     * @return a boolean. True if a SNS User exists, False if a SNS User doesn't exist
     */
    public boolean existsUser(SNSUser snsuser) {

        String email = snsuser.getEmail();

        Boolean exists = getAuthFacade().existsUser(email);

        return exists;

    }

    /**
     * Create Auth User.
     *
     * @param snsuser an SNS User
     */
    public void createAuthUser(SNSUser snsuser){

        String email = snsuser.getEmail();
        String name = snsuser.getName();
        String password = snsuser.getPassword();

        getAuthFacade().addUserWithRole(name,email,password, Constants.ROLE_USER);

    }

    public VaccineAdministrationNurseStore getVaccineAdministrationNurseStore() {
        return vaccineAdministrationNurseStore;
    }

    public void setVaccineAdministrationNurseStore(VaccineAdministrationNurseStore vaccineAdministrationNurseStore) {
        this.vaccineAdministrationNurseStore = vaccineAdministrationNurseStore;
    }

    private void bootstrap()
    {
        this.vaccineTypeStore.addVcType(new VaccineType("Ab123","No Type specified", "No Name specified", "No Brand specified"));
        this.vaccineTypeStore.addVcType(new VaccineType("Bc456","Covid-19 Moderna","Messenger RNA (mRNA) vaccines","Spikevax","Moderna"));
        this.vaccineTypeStore.addVcType(new VaccineType("Cd789","Covid-19 Pfizer","Viral vector vaccines", "Cominarty", "Pfizer" ));
        this.vaccineTypeStore.addVcType(new VaccineType("De298","Smallpox","Live-attenuated vaccines", "No Name specified", "No Brand specified"));
        this.vaccineTypeStore.addVcType(new VaccineType("Ee999","Flu","No Type specified", "No Name specified", "No Brand specified"));
        this.vaccineTypeStore.addVcType(new VaccineType("Zb123","Inactivated vaccines", "No Name specified", "No Brand specified"));

        this.authFacade.addUserRole(Constants.ROLE_ADMIN,Constants.ROLE_ADMIN);
        this.authFacade.addUserRole(Constants.ROLE_RECEPTIONIST,Constants.ROLE_RECEPTIONIST);
        this.authFacade.addUserRole(Constants.ROLE_USER,Constants.ROLE_USER);
        this.authFacade.addUserRole(Constants.ROLE_NURSE,Constants.ROLE_NURSE);
        this.authFacade.addUserRole(Constants.ROLE_CENTER_COORDINATOR,Constants.ROLE_CENTER_COORDINATOR);

        this.authFacade.addUserWithRole("Main Administrator", "admin@lei.sem2.pt", "123456",Constants.ROLE_ADMIN);
        this.authFacade.addUserWithRole("Receptionist", "recep@lei.sem2.pt", "123456",Constants.ROLE_RECEPTIONIST);
        this.authFacade.addUserWithRole("Nurse", "nurse@lei.sem2.pt", "123456",Constants.ROLE_NURSE);
//        this.authFacade.addUserWithRole("Center Coordinator", "ccord@lei.sem2.pt", "123456",Constants.ROLE_CENTER_COORDINATOR);

        VaccinationCenter vaccinationCenter = new VaccinationCenter("Centro de Vacinação COVID-19 - ACeS Porto Ocidental","Quartel Regimento de Transmissões "+
                " | Rua 14 de Agosto, 4250-120 Porto","961402216","8","18","5",10,new Email("cv.pt-ocidental@arsnorte.min-saude.pt"));

        vaccinationCenterStore.add(vaccinationCenter);

        Date date = Date.from(Instant.now());
        vaccinationCenterStore.addVaccine(new VaccineBase(new SNSUser("John Doe", date, 161593120, 900000000L, "test1@test.com", 1000001L, "Av Liberdade 1", "MASCULINO")
        ,"19/06/2022 19:00",vaccineTypeStore.getVcTypeList().get(0),vaccinationCenter ));

        ArrayList<Double> dosages= new ArrayList<Double>(3);
        ArrayList<Integer> intervals= new ArrayList<Integer>(2);

        VaccineAdministration vaccineAdminProc = new VaccineAdministration("Pfizer", "18+", 3, dosages, intervals);
        VaccineAdministration vaccineAdminProc2 = new VaccineAdministration("Spikevax", "18+", 3, dosages, intervals);
        vaccineAdministrationStore.addAdminProcess(vaccineAdminProc);
        vaccineAdministrationStore.addAdminProcess(vaccineAdminProc2);

        DateTimeFormatter fmtDate = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

        VaccineAdministrationNurse vac01 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("31-05-2022 10:00", fmtDate),3,vaccineAdminProc2);
        VaccineAdministrationNurse vac02 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("01-06-2022 10:00", fmtDate),1,vaccineAdminProc2);
        VaccineAdministrationNurse vac03 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("02-06-2022 10:00", fmtDate),3,vaccineAdminProc2);
        VaccineAdministrationNurse vac04 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("03-06-2022 10:00", fmtDate),1,vaccineAdminProc2);
        VaccineAdministrationNurse vac05 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("04-06-2022 10:00", fmtDate),3,vaccineAdminProc2);
        VaccineAdministrationNurse vac06 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("05-06-2022 10:00", fmtDate),1,vaccineAdminProc2);
        VaccineAdministrationNurse vac07 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("06-06-2022 10:00", fmtDate),3,vaccineAdminProc2);
        VaccineAdministrationNurse vac08 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("07-06-2022 10:00", fmtDate),1,vaccineAdminProc2);
        VaccineAdministrationNurse vac09 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("08-06-2022 10:00", fmtDate),3,vaccineAdminProc2);
        VaccineAdministrationNurse vac09a = new VaccineAdministrationNurse(161593121,vaccinationCenter, LocalDateTime.parse("08-06-2022 10:00", fmtDate),3,vaccineAdminProc2);
        VaccineAdministrationNurse vac10 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("09-06-2022 10:00", fmtDate),2,vaccineAdminProc2);
        VaccineAdministrationNurse vac11a = new VaccineAdministrationNurse(161593121,vaccinationCenter, LocalDateTime.parse("10-06-2022 10:00", fmtDate),3,vaccineAdminProc2);
        VaccineAdministrationNurse vac11 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("10-06-2022 10:00", fmtDate),3,vaccineAdminProc2);
        VaccineAdministrationNurse vac12 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("11-06-2022 10:00", fmtDate),1,vaccineAdminProc2);
        VaccineAdministrationNurse vac13 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("12-06-2022 10:00", fmtDate),3,vaccineAdminProc2);
        VaccineAdministrationNurse vac14 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("13-06-2022 10:00", fmtDate),1,vaccineAdminProc2);
        VaccineAdministrationNurse vac15 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("14-06-2022 10:00", fmtDate),3,vaccineAdminProc2);
        VaccineAdministrationNurse vac16 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("15-06-2022 10:00", fmtDate),1,vaccineAdminProc2);
        VaccineAdministrationNurse vac17 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("16-06-2022 10:00", fmtDate),3,vaccineAdminProc2);
        VaccineAdministrationNurse vac18 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("17-06-2022 10:00", fmtDate),1,vaccineAdminProc2);
        VaccineAdministrationNurse vac19 = new VaccineAdministrationNurse(161593120,vaccinationCenter, LocalDateTime.parse("18-06-2022 10:00", fmtDate),3,vaccineAdminProc2);

        vaccineAdministrationNurseStore.add(vac01);
        vaccineAdministrationNurseStore.add(vac02);
        vaccineAdministrationNurseStore.add(vac03);
        vaccineAdministrationNurseStore.add(vac04);
        vaccineAdministrationNurseStore.add(vac05);
        vaccineAdministrationNurseStore.add(vac06);
        vaccineAdministrationNurseStore.add(vac07);
        vaccineAdministrationNurseStore.add(vac08);
        vaccineAdministrationNurseStore.add(vac09);
        vaccineAdministrationNurseStore.add(vac09a);
        vaccineAdministrationNurseStore.add(vac10);
        vaccineAdministrationNurseStore.add(vac11);
        vaccineAdministrationNurseStore.add(vac11a);
        vaccineAdministrationNurseStore.add(vac12);
        vaccineAdministrationNurseStore.add(vac13);
        vaccineAdministrationNurseStore.add(vac14);
        vaccineAdministrationNurseStore.add(vac15);
        vaccineAdministrationNurseStore.add(vac16);
        vaccineAdministrationNurseStore.add(vac17);
        vaccineAdministrationNurseStore.add(vac18);
        vaccineAdministrationNurseStore.add(vac19);


        // Not needed after serialization of employees
           Employee employee = new Employee("Joaquim d'Almeida","Avenida da Boavista 1670","910000001","ccord@lei.sem2.pt",
                                "10000001",Constants.ROLE_CENTER_COORDINATOR,vaccinationCenter);

           this.authFacade.addUserWithRole("Joaquim d'Almeida", "ccord@lei.sem2.pt", "123456",Constants.ROLE_CENTER_COORDINATOR);

           employeeStore.addEmployee(employee);

    }

}
