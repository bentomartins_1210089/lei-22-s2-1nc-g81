package app.domain.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringJoiner;

import static app.controller.SNSUserArrivalController.vaccinationCenter;

/**
 * @author Tiago Oliveira <1211669>
 */

public class VaccineAdministration implements Serializable {

    /**
     * Class attributes
     */
    private String vaccine;
    private String ageGroup;
    private int numDoses;
    private ArrayList<Double> dosages;
    private ArrayList<Integer> dosingIntervals;

    /**
     * Default attributes
     */
    public static final String DEFAULT_VACCINE = "No_Vaccine";
    public static final String DEFAULT_AGE_GROUP = "No_Age_Group";
    public static final int DEFAULT_NUMBER_DOSES = 0;
    public static final ArrayList<Double> DEFAULT_DOSAGES = new ArrayList<>();
    public static final ArrayList<Integer> DEFAULT_DOSING_INTERVALS = new ArrayList<>();
    private VaccinationCenter vaccinationCenter; //Tassio

    /**
     * Empty constructor
     */
    public VaccineAdministration() {
        vaccine = DEFAULT_VACCINE;
        ageGroup = DEFAULT_AGE_GROUP;
        numDoses = DEFAULT_NUMBER_DOSES;
        dosages = DEFAULT_DOSAGES;
        dosingIntervals = DEFAULT_DOSING_INTERVALS;
    }

    /**
     *
     * @param vaccine           vaccine
     * @param ageGroup          age group
     * @param numDoses          number of doses
     * @param dosages           dosages (mL)
     * @param dosingIntervals   recovery intervals for each dose (days)
     */
    public VaccineAdministration(String vaccine, String ageGroup, int numDoses, ArrayList<Double> dosages, ArrayList<Integer> dosingIntervals) {
        this.vaccine = vaccine;
        this.ageGroup = ageGroup;
        this.numDoses = numDoses;
        this.dosages = dosages;
        this.dosingIntervals = dosingIntervals;
    }

    /**
     * Getters
     */
    public String getVaccine() {
        return vaccine;
    }

    public String getAge() {
        return ageGroup;
    }

    public int getNumDoses() {
        return numDoses;
    }

    public ArrayList<Double> getDosages() {
        return dosages;
    }

    public ArrayList<Integer> getIntervals() {
        return dosingIntervals;
    }

    /**
     * Setters
     */
    public void setVaccine(String vaccine) {
        this.vaccine = vaccine;
    }

    public void setAge(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    public void setNumDoses(int numDoses) {
        this.numDoses = numDoses;
    }

    public void setDosages(ArrayList<Double> dosages) {
        this.dosages = dosages;
    }

    public void setInterval(ArrayList<Integer> intervals) {
        this.dosingIntervals = intervals;
    }

    /**
     * Adds value to list
     * @param value value
     */
    public void addDosage(double value) {
        dosages.add(value);
    }

    /**
     * Adds value to list
     * @param value value
     */
    public void addInterval(int value) {
        dosingIntervals.add(value);
    }

    /**
     * Clear
     */
    public void clear() {
        vaccine = DEFAULT_VACCINE;
        ageGroup = DEFAULT_AGE_GROUP;
        numDoses = DEFAULT_NUMBER_DOSES;
        dosages.clear();
        dosingIntervals.clear();
    }

    //Tassio
    public VaccinationCenter getVaccinationCenter() {
        return vaccinationCenter;
    }

    public void setVaccinationCenter(VaccinationCenter vaccinationCenter) {
        this.vaccinationCenter = vaccinationCenter;
    }

    /**
     * To string
     * @return Formatted string with all VaccineAdministration object attribute values
     */
    @Override
    public String toString() {
        String prefix = "(", delimiter = ", ", suffix = ")";
        StringJoiner stringDosages = new StringJoiner(delimiter, prefix, suffix);
        StringJoiner stringIntervals = new StringJoiner(delimiter, prefix, suffix);

        for (Double i : dosages) {
            stringDosages.add(i.toString());
        }
        for (Integer i : dosingIntervals) {
            stringIntervals.add(i.toString());
        }


        return String.format("ADMINISTRATION PROCESS: " +
                        "Vaccine: '%s' | Age Group: '%s' | " +
                        "Number of Doses: '%d' | Dosages: %s | " +
                        "Vaccination Intervals: %s",
                vaccine, ageGroup, numDoses, stringDosages, stringIntervals);
    }
}
