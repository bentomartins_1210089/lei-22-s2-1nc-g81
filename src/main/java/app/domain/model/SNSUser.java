package app.domain.model;
import app.exception.*;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * SNS User Class.
 *
 * @author Bento Martins <1210089>
 */
public class SNSUser implements Serializable {

    // Attributes
    private String nameUser;
    private Date birthDate;
    private long snsNumber;
    private long phoneNumber;
    private String emailAddress;
    private long ccNumber;
    private String userAddress;
    private String sexUser;
    Validator validator = new Validator();
    private String password;
    private ArrayList<VaccineAdministrationNurse> vaccineAdministration;

    /**
     * Gets SNS User name.
     *
     * @return the name of SNS User
     */
    public String getName() {
        return nameUser;
    }

    /**
     * Sets SNS User name.
     *
     * @param nameUser the name of SNS User
     */
    public void setName(String nameUser) {
        this.nameUser = validator.validateName(nameUser);;
    }

    /**
     * Gets SNS User birth date.
     *
     * @return the birth date of SNS User
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Sets SNS User birth date.
     *
     * @param birthDate the birth date of SNS User
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = validator.validateBirthDate(birthDate);
    }

    /**
     * Gets SNS User SNS number.
     *
     * @return the SNS number of SNS User
     */
    public long getSnsNumber() {
        return snsNumber;
    }

    /**
     * Sets SNS User SNS number.
     *
     * @param snsNumber the SNS number of SNS User
     */
    public void setSnsNumber(long snsNumber) {
        this.snsNumber = validator.validateSnsNumber(snsNumber);
    }

    /**
     * Gets SNS User phone.
     *
     * @return the phone of SNS User
     */
    public long getPhone() {
        return phoneNumber;
    }

    /**
     * Sets SNS User phone.
     *
     * @param phoneNumber the phone number of SNS User
     */
    public void setPhone(long phoneNumber) {
        this.phoneNumber = validator.validatePhoneNumber(phoneNumber);;
    }

    /**
     * Gets SNS User e-mail.
     *
     * @return the email of SNS User
     */
    public String getEmail() {
        return emailAddress;
    }

    /**
     * Sets SNS User e-mail.
     *
     * @param email the email of SNS User
     */
    public void setEmail(String email) {
        this.emailAddress = validator.validateEmailAddress(emailAddress);
    }

    /**
     * Gets SNS User Citizen Card number.
     *
     * @return the Citizan Card number of SNS User
     */
    public long getCcNumber() { return ccNumber;  }

    /**
     * Sets SNS User Citizan Card number.
     *
     * @param ccNumber the Citizan Card number of SNS User
     */
    public void setCcNumber(long ccNumber) {this.ccNumber = validator.validateCcNumber(ccNumber); }

    /**
     * Gets SNS User sex.
     *
     * @return the sex of SNS User
     */
    public String getSexUser() { return sexUser; }

    /**
     * Sets SNS User sex.
     *
     * @param sexUser the sex of SNS User
     */
    public void setSexUser(String sexUser) { this.sexUser = validator.validateSex(sexUser); }


    /**
     * Gets SNS User address.
     *
     * @return the user address of SNS User
     */
    public String getUserAddress() { return userAddress;
    }


    /**
     * Sets SNS User address.
     *
     * @param userAddress the user address of SNS User
     */
    public void setUserAddress(String userAddress) { this.userAddress = validator.validateAddress(userAddress);;
    }

    /**
     * Gets SNS User password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets SNS User password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Instantiates a new SNS User.
     */
    public SNSUser(){
    }

    /**
     * Constructor of a new SNS User with sex (no password).
     *
     * @param nameUser     the name of SNS User
     * @param birthDate    the birth date of SNS User
     * @param snsNumber    the SNS number of SNS User
     * @param phoneNumber  the Phone number of SNS User
     * @param emailAddress the e-mail address of SNS User
     * @param ccNumber     the Citizen Card number of SNS User
     * @param userAddress  the address of SNS User
     * @param sexUser      the sex of SNS User
     */
    public SNSUser(String nameUser, Date birthDate, long snsNumber, long phoneNumber, String emailAddress, long ccNumber, String userAddress, String sexUser) {
        this.nameUser = validator.validateName(nameUser);
        this.birthDate = validator.validateBirthDate(birthDate);
        this.snsNumber = validator.validateSnsNumber(snsNumber);
        this.phoneNumber = validator.validatePhoneNumber(phoneNumber);
        this.emailAddress = validator.validateEmailAddress(emailAddress);
        this.ccNumber = validator.validateCcNumber(ccNumber);
        this.userAddress = validator.validateAddress(userAddress);
        this.sexUser = validator.validateSex(sexUser);
    }

    /**
     * Constructor of a new SNS User without sex (no password).
     *
     * @param nameUser     the name of SNS User
     * @param birthDate    the birth date of SNS User
     * @param snsNumber    the SNS number of SNS User
     * @param phoneNumber  the Phone number of SNS User
     * @param emailAddress the e-mail address of SNS User
     * @param ccNumber     the Citizen Card number of SNS User
     * @param userAddress  the address of SNS User
     */
    // Constructor without Sex
    public SNSUser(String nameUser, Date birthDate, long snsNumber, long phoneNumber, String emailAddress, long ccNumber, String userAddress) {
        this.nameUser = validator.validateName(nameUser);
        this.birthDate = validator.validateBirthDate(birthDate);
        this.snsNumber = validator.validateSnsNumber(snsNumber);
        this.phoneNumber = validator.validatePhoneNumber(phoneNumber);
        this.emailAddress = validator.validateEmailAddress(emailAddress);
        this.ccNumber = validator.validateCcNumber(ccNumber);
        this.userAddress = validator.validateAddress(userAddress);
        this.sexUser = "NA";
    }

    /**
     * Constructor of a new SNS User with sex (password).
     *
     * @param nameUser     the name of SNS User
     * @param birthDate    the birth date of SNS User
     * @param snsNumber    the SNS number of SNS User
     * @param phoneNumber  the Phone number of SNS User
     * @param emailAddress the e-mail address of SNS User
     * @param ccNumber     the Citizen Card number of SNS User
     * @param userAddress  the address of SNS User
     * @param sexUser      the sex of SNS User
     * @param password     the password of SNS User
     */
    public SNSUser(String nameUser, Date birthDate, long snsNumber, long phoneNumber, String emailAddress, long ccNumber, String userAddress, String sexUser, String password) {
        this.nameUser = validator.validateName(nameUser);
        this.birthDate = validator.validateBirthDate(birthDate);
        this.snsNumber = validator.validateSnsNumber(snsNumber);
        this.phoneNumber = validator.validatePhoneNumber(phoneNumber);
        this.emailAddress = validator.validateEmailAddress(emailAddress);
        this.ccNumber = validator.validateCcNumber(ccNumber);
        this.userAddress = validator.validateAddress(userAddress);
        this.sexUser = validator.validateSex(sexUser);
        this.vaccineAdministration = vaccineAdministration;
        this.password = password;
    }


    /**
     * Prints to String SNS User.
     *
     * @return string with information about SNS User
     */
    @Override
    public String toString() {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = formatter.format(this.birthDate);

        return  "Name: " + nameUser + "\n"+
                "Birth Date: " + strDate + "\n"+
                "Sex: " + sexUser + "\n"+
                "SNS Number: " + snsNumber + "\n"+
                "CC Number: " + ccNumber + "\n"+
                "Phone: " + phoneNumber + "\n"+
                "E-mail: " + emailAddress + "\n"+
                "Address: " + userAddress;

    }
}
