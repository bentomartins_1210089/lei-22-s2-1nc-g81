package app.domain.model;
import app.domain.shared.Enums.VaccineTypeTechnology;
import app.exception.*;
/**
 * Vaccine Type Class
 * @author Delfim Costa <1081348>
 */

public class VaccineType {

    /**
     * Class attributes
     */
    private String vaccineTypeCode;
    private String vaccineTypeDescription;
    private String vaccineTypeTechnology;
    //Tiago Oliveira <1211669>
    private String vaccineName;
    private String vaccineBrand;

    /**
     * Class default attribute values
     */
    private static final String DEFAULT_VACCINE_TYPE_CODE = "No_code";
    private static final String DEFAULT_VACCINE_TYPE_DESC = "No_Description";
    private static final String DEFAULT_VACCINE_NAME = "No_Name";
    private static final String DEFAULT_VACCINE_BRAND = "No_Brand";
    private static final String DEFAULT_VACCINE_TYPE_TECH = VaccineTypeTechnology.NTS.getVaccineTypeTechnology();

    /**
     * Class attribute values conditions
     */

    /**
     * Validation for vaccineTypeCode attribute
     * Attribute input format [AA999]
     */

    private static final String TYPE_CODE_REGEX = "[a-zA-Z]{2}\\d{3}";

    /**
     * Validation for vaccineTypeDescription attribute
     * Attribute input format Alphanumeric with maximum lenght 30 characters
     */


    private static final String TYPE_DESC_REGEX = "^[ A-Za-z0-9_@./#&+-]*$";

    public final int TYPE_DESC_MAX_LENGTH=30;
    public final int TYPE_DESC_MIN_LENGTH=0;



    /**
     * Contructors
     */

    /**
     * Empty Constructor
     */
    public VaccineType(){
        this.vaccineName = DEFAULT_VACCINE_NAME;
        this.vaccineBrand = DEFAULT_VACCINE_BRAND;
        this.vaccineTypeCode = DEFAULT_VACCINE_TYPE_CODE;
        this.vaccineTypeDescription = DEFAULT_VACCINE_TYPE_DESC;
        this.vaccineTypeTechnology = DEFAULT_VACCINE_TYPE_TECH;
    }

    /**
     * Full Constructor
     * @param vaccineTypeCode Code for vaccine Type
     * @param vaccineTypeDescription Description of vaccine Type
     * @param vaccineTypeTechnology Technology of vaccine Type
     *  (Source : https://www.pfizer.com/news/articles/understanding_six_types_of_vaccine_technologies)
     */
    public VaccineType(String vaccineTypeCode, String vaccineTypeDescription, String vaccineTypeTechnology, String vaccineName, String vaccineBrand){
        this.vaccineName = vaccineName;
        this.vaccineBrand = vaccineBrand;
        this.vaccineTypeCode = validateVaccineTypeCode(vaccineTypeCode);
        this.vaccineTypeDescription = validateVaccineTypeDesc(vaccineTypeDescription);
        this.vaccineTypeTechnology = vaccineTypeTechnology;
    }

    /**
     * 2nd Constructor - With default description
     * @param vaccineTypeCode Code for vaccine Type
     * @param vaccineTypeTechnology Technology of vaccine Type
     */

    public VaccineType(String vaccineTypeCode, String vaccineTypeTechnology, String vaccineName, String vaccineBrand){
        this.vaccineName = vaccineName;
        this.vaccineBrand = vaccineBrand;
        this.vaccineTypeCode = validateVaccineTypeCode(vaccineTypeCode);
        this.vaccineTypeDescription = DEFAULT_VACCINE_TYPE_DESC;
        this.vaccineTypeTechnology = vaccineTypeTechnology;
    }

    /**
     * Getters
     */

    /**
     * Get Vaccine Type Code
     *
     * @return vaccineTypeCode Code for vaccine Type
     */

    public String getVaccineTypeCode() {
        return vaccineTypeCode;
    }

    /**
     * Get Vaccine Type Description
     *
     * @return vaccineTypeDescription Description of vaccine Type
     */

    public String getVaccineTypeDescription() {
        return vaccineTypeDescription;
    }

    /**
     * Get Vaccine Type Technology
     *
     * @return vaccineTypeTechnology Technology of vaccine Type
     */

    public String getVaccineTypeTechnology() {
        return vaccineTypeTechnology;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public String getVaccineBrand() {
        return vaccineBrand;
    }

    /**
     * Setters
     */

    /**
     * Set Vaccine Type Code
     *
     * @param vaccineTypeCode Code for vaccine Type
     */

    public void setVaccineTypeCode(String vaccineTypeCode) {

        this.vaccineTypeCode = validateVaccineTypeCode(vaccineTypeCode);
    }

    /**
     * Set Vaccine Type Description
     *
     * @param vaccineTypeDescription Description of vaccine Type
     */

    public void setVaccineTypeDescription(String vaccineTypeDescription) {

        this.vaccineTypeDescription = validateVaccineTypeDesc(vaccineTypeDescription);
    }

    /**
     * Set Vaccine Type Technology
     *
     * @param vaccineTypeTechnology Technology of vaccine Type
     */

    public void setVaccineTypeTechnology(String vaccineTypeTechnology) {

        this.vaccineTypeTechnology = vaccineTypeTechnology;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public void setVaccineBrand(String vaccineBrand) {
        this.vaccineBrand = vaccineBrand;
    }

    /**
     * Class Attributes Validation
     */

    /**
     * Validate Vaccine Type Code
     *
     * @param vaccineTypeCode Code for vaccine Type
     * @return vaccineTypeCode Code for vaccine Type
     * @throws IllegalArgumentException illegal argument exception
     */

    static String validateVaccineTypeCode(String vaccineTypeCode) {

        if(!vaccineTypeCode.matches(TYPE_CODE_REGEX)){
            throw new InvalidVaccineTypeCodeException();
        }

        return vaccineTypeCode;
    }

    /**
     * Validate Vaccine Type Description
     *
     * @param vaccineTypeDescription Description of vaccine Type
     * @return vaccineTypeDescription Description of vaccine Type
     * @throws IllegalArgumentException illegal argument exception
     */

    private String validateVaccineTypeDesc(String vaccineTypeDescription) {

        if(!vaccineTypeDescription.matches(TYPE_DESC_REGEX)){
            throw new InvalidVaccineTypeDescException();
        }

        if(vaccineTypeDescription.length() > TYPE_DESC_MAX_LENGTH || vaccineTypeDescription.length() < TYPE_DESC_MIN_LENGTH) {
            throw new InvalidVaccineTypeDescException();
        }

        return vaccineTypeDescription;
    }

    /**
     * Textual representation textual of a instance of Vaccine Type
     *
     * @return atributte values of instanciate VaccineType object
     *
     */

    @Override
    public String toString() {
        return String.format("VACCINE TYPE: Name: %s | Brand: %s | Code: %s | Description: %s | " +
                        "Technology Doses: %s",
                vaccineName, vaccineBrand, vaccineTypeCode, vaccineTypeDescription, vaccineTypeTechnology);
    }

} // End of class