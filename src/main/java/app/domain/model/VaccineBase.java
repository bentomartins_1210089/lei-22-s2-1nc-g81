package app.domain.model;

import app.controller.App;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Vaccine base.
 *
 * @author Tassio Gomes <1170065>
 * @author Mário Borja <1200586>
 */
public class VaccineBase implements Serializable {
    /**
     * Class attributes
     */
    private SNSUser snsUser;
    private String dateAndTimeRegistration;
    private VaccineType vaccineType;
    private VaccinationCenter vaccinationCenter;
    private String internalCode;
    /**
     * The Num maximo chars code.
     */
    final int NUM_MAXIMO_CHARS_CODE = 12;
    static int numberOfTests = 0;
    private Company company;

    /**
     * Instantiates a new Vaccine base.
     */
    public VaccineBase() {
    }

    /**
     * Instantiates a new Vaccine base.
     *
     * @param snsUser                 the sns user
     * @param dateAndTimeRegistration the date and time registration
     * @param vaccineType             the vaccine type
     * @param vaccinationCenter       the vaccination center
     */
    public VaccineBase(SNSUser snsUser, String dateAndTimeRegistration, VaccineType vaccineType, VaccinationCenter vaccinationCenter) {
        this.snsUser = snsUser;
        this.dateAndTimeRegistration = dateAndTimeRegistration;
        numberOfTests++;
        this.vaccineType = vaccineType;
        this.vaccinationCenter = vaccinationCenter;
        internalCode = generateInternalCode();
    }

    /**
     * Instantiates a new Vaccine base.
     *
     * @param snsUser           the sns user
     * @param vaccineType       the vaccine type
     * @param vaccinationCenter the vaccination center
     */
    public VaccineBase(SNSUser snsUser, VaccineType vaccineType, VaccinationCenter vaccinationCenter) {
        this.snsUser = snsUser;
        this.vaccineType = vaccineType;
        this.vaccinationCenter = vaccinationCenter;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        this.dateAndTimeRegistration = dtf.format(LocalDateTime.now());
        internalCode = generateInternalCode();
    }

    /**
     * Getters and Setters
     *
     * @return sns user
     */
    public SNSUser getSnsUser() {
        return snsUser;
    }

    /**
     * Sets sns user.
     *
     * @param snsUser the sns user
     */
    public void setSnsUser(SNSUser snsUser) {
        this.snsUser = snsUser;
    }

    /**
     * Gets date and time registration.
     *
     * @return the date and time registration
     */
    public String getDateAndTimeRegistration() {
        return dateAndTimeRegistration;
    }

    /**
     * Sets date and time registration.
     *
     * @param dateAndTimeRegistration the date and time registration
     */
    public void setDateAndTimeRegistration(String dateAndTimeRegistration) {
        this.dateAndTimeRegistration = dateAndTimeRegistration;
    }

    /**
     * Gets vaccine type.
     *
     * @return the vaccine type
     */
    public VaccineType getVaccineType() {
        return vaccineType;
    }

    /**
     * Sets vaccine type.
     *
     * @param vaccineType the vaccine type
     */
    public void setVaccineType(VaccineType vaccineType) {
        this.vaccineType = vaccineType;
    }

    /**
     * Gets vaccination center.
     *
     * @return the vaccination center
     */
    public VaccinationCenter getVaccinationCenter() {
        return vaccinationCenter;
    }

    /**
     * Sets vaccination center.
     *
     * @param vaccinationCenter the vaccination center
     */
    public void setVaccinationCenter(VaccinationCenter vaccinationCenter) {
        this.vaccinationCenter = vaccinationCenter;
    }

    /**
     * Gets internal code.
     *
     * @return the internal code
     */
    public String getInternalCode() {
        return internalCode;
    }

    /**
     * Sets internal code.
     *
     * @param internalCode the internal code
     */
    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    /**
     * Gets num maximo chars code.
     *
     * @return the num maximo chars code
     */
    public int getNUM_MAXIMO_CHARS_CODE() {
        return NUM_MAXIMO_CHARS_CODE;
    }

    /**
     * Gets number of tests.
     *
     * @return the number of tests
     */
    public static int getNumberOfTests() {
        return numberOfTests;
    }

    /**
     * Sets number of tests.
     *
     * @param numberOfTests the number of tests
     */
    public static void setNumberOfTests(int numberOfTests) {
        VaccineBase.numberOfTests = numberOfTests;
    }

    /**
     * Generate internal code.
     *
     * @return internal code
     */
    public String generateInternalCode()
    {
        StringBuilder intCode = new StringBuilder(String.valueOf(numberOfTests));
        int numberOfDigits = intCode.length();
        for (int i = 0; i < NUM_MAXIMO_CHARS_CODE - numberOfDigits; i++) {
            intCode.insert(0, "0");
        }
        return intCode.toString();
    }

    @Override
    public String toString()
    {
        return String.format("%nTEST:%nINTERNAL CODE: %s%nVACCINATION CENTER: %s%nVACCINE TYPE %s%nDATE OF REGISTRATION: %s %n",
                internalCode, vaccinationCenter, vaccineType, dateAndTimeRegistration);
    }


    /**
     * Bootstrap vaccine base list.
     *
     * @return the list
     */
    public List<VaccineBase> bootstrapVaccineBase() {
        company = App.getInstance().getCompany();
        //Bootstrap Vaccine Scheduling
        List<VaccineBase> vaccineBootstrap = new ArrayList<>();
        Date date = Date.from(Instant.now());
        VaccineBase vaccine1 = new VaccineBase(new SNSUser("Vitor", date, 123456789, 984563251, "email@email.com", 12345678, "rua", "Masculino"), company.getVaccineTypeStore().getVcTypeList().get(0), company.getVaccinationCenterStore().getAllVacCenters().get(0));
        vaccineBootstrap.add(vaccine1);
        return vaccineBootstrap;
    }


}
