package app.domain.model;

import java.io.Serializable;

/**
 * @author Tiago Oliveira <1211669>
 */

public class LegacySystemData implements Serializable {

    /**
     * Class attributes
     */
    private String snsNumber;
    private String vaccine;
    private int dose;
    private String lotNumber;
    private String schedule;
    private String arrival;
    private String administration;
    private String leaving;
    private String snsUserName;
    private String vaccineDesc;

    /**
     * Contructor instantiates an legacy system data
     * @param snsNumber         SNS user number
     * @param vaccine           Vaccine name
     * @param dose              Dose number
     * @param lotNumber         Lot number
     * @param schedule          Schedule date and time
     * @param arrival           Arrival date and time
     * @param administration    Administration date and time
     * @param leaving           Leaving date and time
     */
    public LegacySystemData(String snsNumber, String vaccine, int dose, String lotNumber, String schedule, String arrival, String administration, String leaving) {
        this.snsNumber = snsNumber;
        this.vaccine = vaccine;
        this.dose = dose;
        this.lotNumber = lotNumber;
        this.schedule = schedule;
        this.arrival = arrival;
        this.administration = administration;
        this.leaving = leaving;
    }

    /**
     * Getters
     */
    public String getSNSNUmber() {
        return this.snsNumber;
    }

    public String getVaccine() {
        return this.vaccine;
    }

    public int getDose() {
        return this.dose;
    }

    public String getLotNumber() {
        return this.lotNumber;
    }

    public String getSchedule() {
        return this.schedule;
    }

    public String getArrival() {
        return this.arrival;
    }

    public String getAdministration() {
        return this.administration;
    }

    public String getLeaving() {
        return this.leaving;
    }

    public String getSnsUserName() {
        return this.snsUserName;
    }

    public String getVaccineDesc() {
        return this.vaccineDesc;
    }

    /**
     * Setters
     */
    public void setSNSNUmber(String snsNumber) {
        this.snsNumber = snsNumber;
    }

    public void setVaccine(String vaccine) {
        this.vaccine = vaccine;
    }

    public void setDose(int dose) {
        this.dose = dose;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public void setAdministration(String administration) {
        this.administration = administration;
    }

    public void setLeaving(String leaving) {
        this.leaving = leaving;
    }

    public void setSnsUserName(String snsUserName) {
        this.snsUserName = snsUserName;
    }

    public void setVaccineDesc(String vaccineDesc) {
        this.vaccineDesc = vaccineDesc;
    }

    /**
     * toString method
     * @return String representation of the LegacySystemData object
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(snsNumber + " ");
        s.append(snsUserName + " ");
        s.append(vaccine + " ");
        s.append(vaccineDesc + " ");
        s.append(dose + " ");
        s.append(lotNumber + " ");
        s.append(schedule + " ");
        s.append(arrival + " ");
        s.append(administration + " ");
        s.append(leaving);

        return s.toString();
    }
}
