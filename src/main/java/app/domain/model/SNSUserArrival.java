package app.domain.model;

import java.io.Serializable;

/**
 * @author Tiago Oliveira <1211669>
 */

public class SNSUserArrival implements Serializable {

    /**
     * Class attributes
     */
    private VaccineBase vaccineBase;
    private long snsNumber;
    private String time;
    private String date;

    /**
     * Contructor instantiates an sns user arrival
     * @param snsNumber         SNS user number
     * @param time              User arrival time
     * @param date              User arrival date
     */
    public SNSUserArrival(long snsNumber, String time, String date) {
        this.snsNumber = snsNumber;
        this.time = time;
        this.date = date;
    }

    public SNSUserArrival(VaccineBase vaccineBase, long snsNumber, String time, String date) {
        this.vaccineBase = vaccineBase;
        this.snsNumber = snsNumber;
        this.time = time;
        this.date = date;
    }
    /**
     * Getters
     */
    public long getSnsNumber() {
        return snsNumber;
    }

    public String getTime() {
        return time;
    }

    public String getDate() {
        return date;
    }

    public VaccineBase getVaccineBase() {
        return vaccineBase;
    }

    /**
     * Setters
     */
    public void setSnsNumber(long snsNumber) {
        this.snsNumber = snsNumber;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setVaccineBase(VaccineBase vaccineBase) {
        this.vaccineBase = vaccineBase;
    }
    /**
     * To string
     * @return Formatted string with all user arrival registration data
     */
    @Override
    public String toString() {
        return String.format("WAITING USER: SNS number: %d | " +
                        "Time : %s | Date: %s",
                snsNumber, time, date);
    }
}
