package app.domain.model;

import app.controller.SNSUserArrivalController;
import app.exception.*;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Validate Data
 *
 * @author Bento Martins <1210089>
 */
public class Validator implements Serializable {

    // Variables
    private Company company;
    private String nameUser;
    private Date birthDate;
    private long snsNumber;
    private long phoneNumber;
    private String emailAddress;
    private long ccNumber;
    private String userAddress;
    private String sexUser;
    private VaccineBase vaccine;
    private VaccinationCenter vaccinationCenter;

    // Conditions
    private static final String EMAIL_REGEX = "^[a-zA-Z0-9'_+&*-]+(?:\\."+
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";

    private static final String NAME_REGEX = "[a-zA-ZãáàâÂÁÀÂéêÉÊíÍóôõÓÔÕúÚçÇ' .][a-zA-ZãáàâÂÁÀÂéêÉÊíÍóôõÓÔÕúÚçÇ' .]*";
    private static final String ADDRESS_REGEX = "[a-zA-Z0-9ãáàâÂÁÀÂéêÉÊíÍóôõÓÔÕúÚçÇ' .+|º-][a-zA-Z0-9ãáàâÂÁÀÂéêÉÊíÍóôõÓÔÕúÚçÇ' .+|º-]*";

    private final int NAME_MAX_LENGTH=150;
    private final int NAME_MIN_LENGTH=2;

    private final int ADDRESS_MAX_LENGTH=200;
    private final int ADDRESS_MIN_LENGTH=3;

    private String SEX_DEFAULT="NA";
    private static final String MALE = "Masculino";
    private static final String FEMALE = "Feminino";

    private final long MIN_CITIZENCARDNUMBER= 1000000L;
    private final long MAX_CITIZENCARDNUMBER= 99999999L;

    private static final long MIN_SNSNUMBER = 100000000L;
    private static final long MAX_SNSNUMBER = 999999999L;

    private final long MIN_PHONENUMBER = 100000000L;
    private final long MAX_PHONENUMBER = 999999999L;


    /**
     * Instantiates a new Validator.
     */
//    public Validator() {
//        this.company = App.getInstance().getCompany();
//    }
    public Validator() {
    }

    /**
     * Validate e-mail.
     *
     * @param emailAddress the e-mail
     * @return the e-mail
     */
    public String validateEmailAddress(String emailAddress) {

        if (!emailAddress.matches(EMAIL_REGEX)){
            throw new InvalidEmailAddressException("Please correct E-mail address.");
        }
        return emailAddress;
    }

    public boolean validateEmail(Email email) {
        if (email == null)
            throw new IllegalArgumentException("Email is invalid.");
        return true;
    }

    /**
     * Validate SNS number.
     *
     * @param snsNumber the SNS number
     * @return the SNS number
     */
    public long validateSnsNumber(long snsNumber) {

        if (!validateNumberLength(snsNumber, MIN_SNSNUMBER, MAX_SNSNUMBER)){
            throw new InvalidSnsNumberException("Please correct SNS number.");
        }

        return snsNumber;
    }

    /**
     * Validate Citizen Card number.
     *
     * @param ccNumber the Citizen Card number
     * @return the Citizen Card number
     */
    public long validateCcNumber(long ccNumber) {

        if (!validateNumberLength(ccNumber, MIN_CITIZENCARDNUMBER, MAX_CITIZENCARDNUMBER)){
            throw new InvalidCcNumberException("Please correct citizen card number.");
        }

        return ccNumber;
    }

    /**
     * Validate phone number.
     *
     * @param phoneNumber the phone number
     * @return the phone number
     */
    public long validatePhoneNumber(long phoneNumber) {

        if (!validateNumberLength(phoneNumber, MIN_PHONENUMBER, MAX_PHONENUMBER)){
            throw new InvalidPhoneNumberException("Please correct phone number.");
        }

        return phoneNumber;
    }

    /**
     * Validate name.
     *
     * @param nameUser the name
     * @return the name
     */
    public String validateName(String nameUser) {

        if (!nameUser.matches(NAME_REGEX)){
            throw new InvalidNameException("Please correct SNS user name.");
        }

        if(nameUser.length() > NAME_MAX_LENGTH || nameUser.length() < NAME_MIN_LENGTH){
            throw new InvalidNameException("Please correct SNS user name.");
        }

        return nameUser;
    }

    /**
     * Validate address.
     *
     * @param userAddress the address
     * @return the address
     */
    public String validateAddress(String userAddress) {

        if (!userAddress.matches(ADDRESS_REGEX)){
            throw new InvalidAddressException("Please correct address.");
        }

        if(userAddress.length() > ADDRESS_MAX_LENGTH || userAddress.length() < ADDRESS_MIN_LENGTH){
            throw new InvalidAddressException("Please correct address.");
        }

        return userAddress;
    }

    /**
     * Validate address.
     *
     * @param sexUser the sex
     * @return the sex
     */
    public String validateSex(String sexUser) {

        if (!sexUser.equalsIgnoreCase(MALE) && !sexUser.equalsIgnoreCase(FEMALE) && !sexUser.equalsIgnoreCase("NA") ){
            throw new InvalidSexException("Please correct sex.");
        }

        return sexUser;
    }

    /**
     * Validate birth date.
     *
     * @param birthDate birth date
     * @return birth date
     */
    public Date validateBirthDate(Date birthDate) {
        Date currentDate = new Date();
        long milisecDiff = currentDate.getTime() - birthDate.getTime();
        long yearsDiff = (milisecDiff / (1000L*60*60*24*365));

        if (yearsDiff > 150 || milisecDiff < 0){
            throw new InvalidBirthDateException("Please correct birthdate.");
        }

        return birthDate;
    }

    /**
     * Validate birth date.
     *
     * @param number number to validate
     * @param min minimal accepted value
     * @param max maximum accepted value
     * @return true if value is accepted, false if value is not accepted
     */
    private static boolean validateNumberLength(long number, long min, long max) {
        return number >= min && number <= max;
    }

    /**
     * Validates date and time of pattern yyyy-MM-dd HH:mm
     *
     * @param dateTime Date and time string
     * @return True if Date-Time format is valid. False otherwise
     */
    public boolean validDateTime(String dateTime) {
        DateTimeFormatter fmtDate, fmtTime;

        fmtDate= DateTimeFormatter.ofPattern("MM/dd/yyyy");
        fmtTime = DateTimeFormatter.ofPattern("HH:mm");

        String[] dateTimeParts = dateTime.split(" ");
        try {
            String[] dateParts= dateTimeParts[0].split("/");
            if (dateParts[0].length() == 1)
                dateTimeParts[0] = "0" + dateTimeParts[0];
            LocalDate.parse(dateTimeParts[0], fmtDate);

            String[] timeParts= dateTimeParts[1].split(":");
            if (timeParts[0].length() == 1)
                dateTimeParts[1] = "0" + dateTimeParts[1];
            LocalTime.parse(dateTimeParts[1], fmtTime);
        } catch (RuntimeException e) {
            return false;
        }
        return true;
    }

    /**
     * Validates date of pattern yyyy-MM-dd
     *
     * @param date Date string
     * @return True if Date-Time format is valid. False otherwise
     */
    public boolean validDate(String date) {
        DateTimeFormatter fmtDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        try {
            String[] dateParts= date.split("-");
            if (dateParts[0].length() == 1)
                date = "0" + date;
            LocalDate.parse(date, fmtDate);

        } catch (RuntimeException e) {
            return false;
        }
        return true;
    }

    /**
     * Validates dosages values in list
     *
     * @param dosages dosages (mL)
     * @return boolean. True if valid, false otherwise.
     */
    public boolean validDosages(ArrayList<Double> dosages) {
        int index = 0;

        for (double i : dosages) {
            index = dosages.indexOf(i);

            if (dosages.get(index) <= 0)
                throw new InvalidDosagesException();
        }
        return true;
    }

    /**
     * Validates intervals values in list
     *
     * @param intervals recovery intervals for each dose (days)
     * @return boolean. True if valid, false otherwise.
     */
    public boolean validIntervals(ArrayList<Integer> intervals) {
        int index = 0;

        for (int i : intervals) {
            index = intervals.indexOf(i);

            if (intervals.get(index) <= 0)
                throw new InvalidDosingIntervalsException();
        }
        return true;
    }

    /**
     * Validates an user scheduled vaccine for that day and vaccination center
     *
     * @param snsNumber SNS user number
     * @param date User arrival date
     * @return true if has scheduled, false otherwise
     */
    public boolean validateScheduledVaccination(long snsNumber, String date) {
        SNSUserArrivalController ctrl = new SNSUserArrivalController();
        List<VaccineBase> vcSchedules = company.getVaccinationCenterStore().getAllVacs();

        for (VaccineBase vaccineBase : vcSchedules) {
            if (vaccineBase.getVaccinationCenter().equals(ctrl.getVaccinationCenter()))
                if (vaccineBase.getSnsUser().getSnsNumber() == snsNumber) {
                    String dateAndTimeReg = String.valueOf(vaccineBase.getDateAndTimeRegistration());
                    String[] dateTimeParts = dateAndTimeReg.split( "T");

                    if (dateTimeParts[0].equals(date))
                        return true;
                }
        }
        return false;
    }



    public boolean validateVaccine(VaccineType vaccine) {
        if (vaccine == null)
            throw new IllegalArgumentException("Vaccine cannot be null.");
        return true;
    }

    public String validateAgeGroup(int age) {
        String ageGroup = null;

        if (age >= 5 && age <= 12) {
            ageGroup = "5-12";
        } else if (age >= 13 && age <= 17) {
            ageGroup = "13-18";
        } else if (age >= 18) {
            ageGroup = "18+";
        } else {
            throw new InvalidAgeException();
        }
        return ageGroup;
    }

    public boolean validateNumDoses(int numDoses) {
        if ( numDoses > 0 ) {
            return true;
        } else {
            throw new InvalidNumberDosesException("\nWrong number of doses.");
        }
    }

    public boolean validateVaccinationCenter(VaccinationCenter vaccinationCenter) {
        if (vaccinationCenter == null)
            throw new IllegalArgumentException("Date cannot be null.");
        return true;

    }

    public boolean checkLocalDateTimeRules(LocalDateTime localDateTime) {
        String dayString, monthString, hourString, minuteString;
        if (localDateTime.getDayOfMonth() < 10)
            dayString = "0" + localDateTime.getDayOfMonth();
        else
            dayString = String.valueOf(localDateTime.getDayOfMonth());
        if (localDateTime.getMonthValue() < 10)
            monthString = "0" + localDateTime.getMonthValue();
        else
            monthString = String.valueOf(localDateTime.getMonthValue());
        if (localDateTime.getHour() < 10)
            hourString = "0" + localDateTime.getHour();
        else
            hourString = String.valueOf(localDateTime.getHour());
        if (localDateTime.getMinute() < 10)
            minuteString = "0" + localDateTime.getMinute();
        else
            minuteString = String.valueOf(localDateTime.getMinute());
        String localDateTimeString = dayString + "-" + monthString + "-" + localDateTime.getYear() + " " + hourString + ":" + minuteString;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        try {
            formatter.parse(localDateTimeString);
            return true;
        } catch (DateTimeParseException e){
            return false;
        }
    }

    public boolean checkLotNumber(String lotNumber) throws IllegalArgumentException {
        if(lotNumber == null){
            throw new IllegalArgumentException("Lot number cannot be null");
        }

        if (lotNumber.isBlank()) {
            throw new IllegalArgumentException("Lot number cannot be blank");
        }

        if (lotNumber.length() == 8) {
            String[] array = lotNumber.split("-");
            if(array.length == 2) {
                if (!array[1].matches("-?\\d+")) {
                    throw new IllegalArgumentException("Lot number must contain 5 alphanumeric characters, a hyphen and 2 numeric characters (e.g.: 21C16-05)");
                }
            }else{
                throw new IllegalArgumentException("Lot number must contain 5 alphanumeric characters, a hyphen and 2 numeric characters (e.g.: 21C16-05)");
            }
        } else {
            throw new IllegalArgumentException("Lot number must have 8 digits");
        }
        return true;
    }

}


