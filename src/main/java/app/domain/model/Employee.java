package app.domain.model;

import app.domain.shared.Enums.EmployeeRole;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

public class Employee implements Serializable {
    private String name;
    private String address;
    private String phoneNumber;
    private String email;
    private String cc;
    private String role;
    private VaccinationCenter vaccinationCenter;
    private String ROLE_BY_OMISSION = "Without role";
    private String NAME_BY_OMISSION = "Without name";
    private String ADDRESS_BY_OMISSION = "Without address";
    private String PHONE_NUMBER_BY_OMISSION = "000000000";

    public Employee(String name, String address, String phoneNumber, String email, String cc, String role) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.cc = cc;
        this.role = role;
    }

    public Employee(String name, String address, String phoneNumber, String email, String cc, String role,VaccinationCenter vaccinationCenter) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.cc = cc;
        this.role = role;
        this.vaccinationCenter = vaccinationCenter;
    }

    public Employee()
    {
        this.role = ROLE_BY_OMISSION;
        this.name = NAME_BY_OMISSION;
        this.address = ADDRESS_BY_OMISSION;
        this.phoneNumber = PHONE_NUMBER_BY_OMISSION;
    }

    /**
     * Checks if a String contain a non alpha numeric character
     *
     * @param s string
     * @return true or false
     */
    private static boolean isAlphaNumeric(String s)
    {
        return s != null && s.matches("^[a-zA-Z0-9]*$");
    }

    /**
     * Check name boolean.
     *
     * @param name name
     * @return boolean
     * @throws IllegalArgumentException illegal argument exception
     */
    public static boolean checkName(String name) throws IllegalArgumentException
    {
        int NAME_MAX_CHARS = 35;
        if (name == null)
        {
            throw new IllegalArgumentException("Name is null");
        }
        if (StringUtils.isBlank(name))
        {
            throw new IllegalArgumentException("Name cannot be empty");
        }
        if (name.length() > NAME_MAX_CHARS)
        {
            throw new IllegalArgumentException("Name has a maximum of 35 characters");
        }
        return true;
    }

    /**
     * Check email boolean.
     *
     * @param email email
     * @return boolean
     * @throws IllegalArgumentException illegal argument exception
     */
    public static boolean checkEmail(String email) throws IllegalArgumentException
    {
        int EMAIL_MAX_CHARS = 30;

        if (email == null)
        {
            throw new IllegalArgumentException("Email is null");
        }
        if (StringUtils.isBlank(email))
        {
            throw new IllegalArgumentException("Email cannot be empty");
        }
        if (email.length() > EMAIL_MAX_CHARS)
        {
            throw new IllegalArgumentException("Email must have a maximum of 30 characters.");
        }
        return true;
    }

    /**
     * Check cc boolean.
     *
     * @param cc cc
     * @return boolean
     * @throws IllegalArgumentException illegal argument exception
     */
    public static boolean checkCc(String cc) throws IllegalArgumentException
    {
        int CC_MAX_CHARS = 8;

        if (cc == null)
        {
            throw new IllegalArgumentException("Cc is null");
        }
        if (StringUtils.isBlank(cc))
        {
            throw new IllegalArgumentException("Cc cannot be empty");
        }
        if (cc.length() > CC_MAX_CHARS)
        {
            throw new IllegalArgumentException("Cc must have a maximum of 8 characters.");
        }
        return true;
    }

    /**
     * Check address boolean.
     *
     * @param address address
     * @return boolean
     * @throws IllegalArgumentException illegal argument exception
     */
    public static boolean checkAddress(String address) throws IllegalArgumentException
    {
        int ADDRESS_MAX_CHARS = 30;

        if (address == null)
        {
            throw new IllegalArgumentException("Address is null");
        }
        if (StringUtils.isBlank(address))
        {
            throw new IllegalArgumentException("Address cannot be empty");
        }
        if (address.length() > ADDRESS_MAX_CHARS)
        {
            throw new IllegalArgumentException("Address must have a maximum of 30 characters.");
        }
        return true;
    }

    /**
     * Check phone number boolean.
     *
     * @param phoneNumber phone number
     * @return boolean
     * @throws IllegalArgumentException illegal argument exception
     */
    public static boolean checkPhoneNumber(String phoneNumber) throws IllegalArgumentException
    {
        int PHONE_NUMBER_MAX_CHARS = 9;

        if (phoneNumber == null)
        {
            throw new IllegalArgumentException("Phone number is null");
        }
        if (!isAlphaNumeric(phoneNumber))
        {
            throw new IllegalArgumentException("Phone number must have only alphanumeric chars");
        }
        if (StringUtils.isBlank(phoneNumber))
        {
            throw new IllegalArgumentException("Phone number cannot be empty");
        }
        if (phoneNumber.length() != PHONE_NUMBER_MAX_CHARS)
        {
            throw new IllegalArgumentException("Phone number must have 9 digits");
        }
        return true;
    }

    /**
     * Check organization role boolean.
     *
     * @param organizationRole organization role
     * @return boolean
     * @throws IllegalArgumentException illegal argument exception
     */
    public static boolean checkOrganizationRole(String organizationRole) throws IllegalArgumentException
    {
        int ORGANIZATION_ROLE_MAX_CHARS = 25;

        if (organizationRole == null)
        {
            throw new IllegalArgumentException("Organization role is null");
        }
        if (StringUtils.isBlank(organizationRole))
        {
            throw new IllegalArgumentException("Organization role cannot be empty");
        }
        if (organizationRole.length() > ORGANIZATION_ROLE_MAX_CHARS)
        {
            throw new IllegalArgumentException("Organization role have a maximum of 25 characters");
        }
        if(EnumUtils.isValidEnum(EmployeeRole.class, organizationRole))
        {
            throw new IllegalArgumentException("Organization role doesn't exist or it's written wrong!");
        }
        return true;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public VaccinationCenter getVaccinationCenter() {
        return vaccinationCenter;
    }

    public void setVaccinationCenter(VaccinationCenter vaccinationCenter) {
        this.vaccinationCenter = vaccinationCenter;
    }

    /**
     * To string.
     *
     * @return string
     */

    @Override
    public String toString()
    {
        return String.format("EMPLOYEE %nNAME:"
                        + " %s %nORGANIZATION ROLE: %s %nCC:" +
                        "%s %nADDRESS: %s %nEMAIL: %s %nPHONE NUMBER: %s %n",
                name, role, cc,
                address, email,
                phoneNumber);
    }

}
