package app.domain.interfaces;

import app.domain.model.SNSUserArrival;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

/**
 * The type User arrival time comparator.
 */
public class UserArrivalTimeComparator implements Comparator<SNSUserArrival> {
    @Override
    public int compare (SNSUserArrival a, SNSUserArrival b) {
        return dateTimeParse(a.getDate(), a.getTime()).compareTo(dateTimeParse(b.getDate(), b.getTime()));
    }

    private Date dateTimeParse (String date, String time) {
        String           dateTime  = date + " " + time;
        SimpleDateFormat dateParser = new SimpleDateFormat("dd/MM/yy HH:mm");
        {
            try {
                Date parsedDate = dateParser.parse(dateTime);
                return parsedDate;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return new Date();
    }
}
