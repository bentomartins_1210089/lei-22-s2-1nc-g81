package app.domain.interfaces;

import app.domain.model.LegacySystemData;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Comparator;
import java.util.Date;

/**
 * @author Tiago Oliveira <1211669>
 *
 * Legacy System Data Sort By Leaving Time
 */
public class LegacySystemLeavingSort implements Comparator<LegacySystemData> {
    @Override
    public int compare(LegacySystemData obj, LegacySystemData obj2) {
        SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Date arrival = null, compArrival = null;

        try {
            arrival = fmt.parse(obj.getLeaving());
            compArrival = fmt.parse(obj2.getLeaving());
        } catch (DateTimeParseException e) {
            System.out.println("Error parsing legacy system file dates by leaving time");
            e.printStackTrace();
        } finally {
            return arrival.compareTo(compArrival);
        }
    }
}
