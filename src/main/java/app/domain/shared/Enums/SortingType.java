package app.domain.shared.Enums;

/**
 * Enum with different sorting types
 */
public enum SortingType {
    ARRIVAL("Arrival time"),
    LEAVING("Leaving time");

    private final String SORTING_TYPE;

    SortingType(String sortingType)
    {
        this.SORTING_TYPE = sortingType;
    }

    public String getSortingType()
    {
        return SORTING_TYPE;
    }
}
