package app.domain.shared.Enums;

/**
 * Vaccine Type Technology Enum Class
 * @author Delfim Costa <1081348>
 */

public enum VaccineTypeTechnology {

    NTS("No Type specified"),
    LAV("Live-attenuated vaccines"),
    INV("Inactivated vaccines"),
    SUV("Subunit vaccines"),
    TXV("Toxoid vaccines"),
    VVV("Viral vector vaccines"),
    MRV("Messenger RNA (mRNA) vaccines");

    private final String vTypeTech;

    VaccineTypeTechnology(String vTypeTech)
    {
        this.vTypeTech = vTypeTech;
    }

    public String getVaccineTypeTechnology()
    {
        return vTypeTech;
    }

}