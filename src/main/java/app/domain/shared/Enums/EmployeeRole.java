package app.domain.shared.Enums;

public enum EmployeeRole {
    //ROLE_ADMINISTRATOR("Administrator"),
    ROLE_NURSE("Nurse"),
    ROLE_CENTER_COORDINATOR("Center Coordinator"),
    ROLE_RECEPTIONIST("Receptionist");
    private final String value;

    EmployeeRole(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}




