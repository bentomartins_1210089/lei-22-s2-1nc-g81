# Use Case Diagram (UCD)

![Use Case Diagram](UCD.svg)

| **UC**   | **Description**                                                                                                                                            |
|:---------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| **UC01** | As a SNS user, I intend to use the application to schedule a vaccine                                                                                       |
| **UC02** | As a receptionist at one vaccination center, I want to schedule a vaccination                                                                              |
| **UC03** | As a receptionist, I want to register a SNS User                                                                                                           |
| **UC04** | As a receptionist at a vaccination center, I want to register the arrival of a SNS user to take the vaccine                                                |
| **UC05** | As a nurse, I intend to consult the users in the waiting room of a vaccination center                                                                      |
| **UC06** | As a company, I record daily the total number of people vaccinated in each vaccination center                                                              |
| **UC07** | As a nurse, I register adverse reactions of a SNS User                                                                                                     |
| **UC08** | As a nurse, I want to record the administration of a vaccine to SNS user and send a SMS message to inform the SNS user he can leave the vaccination center |
| **UC09** | As an administrator, I want to register a vaccination center to respond to a certain pandemic                                                              |
| **UC10** | As an administrator, I want to register an Employee                                                                                                        |
| **UC11** | As an administrator, I want to get a list of Employees with a given function/role                                                                          |
| **UC12** | As an administrator, I intend to specify a new vaccine type                                                                                                |
| **UC13** | As an administrator, I intend to specify a new vaccine and its administration process                                                                      |
| **UC14** | As an administrator, I want to load a set of users from a CSV file                                                                                         |
| **UC15** | As a center coordinator, I intend to check and export vaccination statistics                                                                               |
| **UC16** | As a center coordinator, I intend to analyse the performance of a center                                                                                   |
| **UC17** | As a center coordinator, I want to import data from a legacy system                                                                                        |
| **UC18** | As a center coordinator, I want to get a list of all vaccines                                                                                              |
| UC19     | As the system, I send notifications to the SNS User with the schedule information                                                                          |
| UC20     | As a nurse, I check SNS User info and health conditions                                                                                                    |
| UC21     | As an administrator, I register new SNS User                                                                                                               |
| UC22     | As a nurse, I request and deliver a vaccination certificate to a SNS User                                                                                  |
| UC23     | As a nurse, I check which vaccine and how it should be administrated on a SNS User                                                                         |                                                                   |
| UC24     | As a SNS user, I request a vaccination certificate                                                                                                         |
| UC25     | As a SNS user, I accept that the Company/DGS can send me notifications                                                                                     |

* Defined by the client