# OO Analysis #

## Rationale to identify domain conceptual classes ##

### _Conceptual Class Category List_ ###

**Business Transactions**

- VaccineSchedule
- VaccineAdministration

---
**Transaction Line Items**

---
**Product/Service related to a Transaction or Transaction Line Item**

- Vaccine

---
**Transaction Records**

--- 
**Roles of People or Organizations**

- SNSUser
- Employee
- Receptionist
- ReceptionistHealthcareCenters
- Nurse
- NurseHealthcareCenters
- Administrator
- CenterCoordinator

---
**Places**

- VaccinationCenter
- CommunityMassVaccinationCenter
- HealthcareCenter
- WaitingRoom
- RecoveryRoom

---
**Noteworthy Events**

- VaccineSchedule
- VaccineAdministration
- RecoveryPeriod
- AdverseReaction
- VaccineAdministrationInstruction
- VaccinationCertificate

---
**Physical Objects**

- Vaccine

---
**Descriptions of Things**

- VaccineType
- VaccineName
- VaccineBrand

---
**Catalogs**

---
**Containers**

---
**Elements of Containers**

---
**Organizations**

- DGS (Company)
- VaccinationCenter
- ARS 
- AGES 

---
**Other External/Collaborating Systems**

---
**Records of finance, work, contracts, legal matters**

---
**Documents mentioned/used to perform some work/**

* Notification
* SMS
* E-mail
* VaccinationCertificate
* Reports
* StatisticsData

---

###**Rationale to identify associations between conceptual classes**

| Concept (A) 		                 | Association   	 |            Concept (B) |
|--------------------------------|:---------------:|-----------------------:|
| Company                        |       has       |      VaccinationCenter |
| Company                        |       has       |                Vaccine |
| Company                        |       has       |          Administrator |
| Company                        |       has       |                SNSUser |
| Company                        |       has       |           Notification |
| Administrator                  |     manages     |                Employe |
| Administrator                  |     manages     |                SNSUser |
| Administrator                  |     manages     |      VaccinationCenter |
| Administrator                  |     manages     |                Vaccine |
| Administrator                  |     manages     |            VaccineType |
| SNSUser                        |    requests     | VaccinationCertificate |
| SNSUser                        |   authorizes    |           Notification |
| SNSUser                        |      makes      |        VaccineSchedule |
| VaccinationCenter              |       has       |      CoordinatorCenter |
| HealthCareCenter               |   belongs to    |                   AGES |
| AGES                           |   is part of    |                    ARS |
| HealthCareCenter               | can administer  |            VaccineType |
| CommunityMassVaccinationCenter | can administer  |            VaccineType |
| Employee                       |    works on     |      VaccinationCenter |
| Nurse                          |   administers   |                Vaccine |
| Nurse                          |    executes     |  VaccineAdministration |
| Nurse                          |   checks info   |                SNSUser |
| Nurse                          |    initiates    |        Recovery Period |
| Nurse                          |    registers    |       AdverseReactions |
| Receptionist                   |    confirms     |        VaccineSchedule |
| Receptionist                   |  acknowledges   |  VaccineAdministration |
| Receptionist                   |    checks in    |                SNSUser |
| Vaccine                        |      is of      |            VaccineType |
| VaccineSchedule                |  for taking a   |            VaccineType |
| VaccineSchedule                |     creates     |           Notification |
| VaccineSchedule                |  allocated to   |      VaccinationCenter |
| VaccineAdministration          |   reported in   |                Company |
| VaccineAdministration          |   carries out   |        VaccineSchedule |
| VaccineAdministration          | administered on |                SNSUser |
| CoordinatorCenter              |    generates    |                Reports |
| CoordinatorCenter              |      sees       |         StatisticsData |
| RecoveryPeriod                 |     creates     |           Notification |
| Company                        |     manages     |                    ARS |

## Domain Model

![MD Diagram](DM.svg)
