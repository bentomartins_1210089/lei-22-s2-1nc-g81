# Use Case Diagram (UCD)

![Use Case Diagram](UCD.svg)

| **UC** | **Description**                                                                              |
|:-------|:---------------------------------------------------------------------------------------------|
| UC01   | As a SNS User, I want to schedule vaccine.                                                   |
| UC02   | As a Receptionist, I want to schedule a vaccine for a SNS User                               |
| UC03   | As a SNS User, I accept that the System/DGS can send me notifications                        |
| UC04   | As the system, I send notifications to the SNS User with the schedule information            |
| UC05   | As a Receptionist, I acknowledge the system that the SNS User is ready for vaccination       |
| UC06   | As a Nurse, I check which SNS Users are ready for vaccination                                |
| UC07   | As a Nurse, I check SNS User info and health conditions                                      |
| UC08   | As a Nurse, I check which vaccine and how it should be administrated on a SNS User           |
| UC09   | As a Nurse, I register that a vaccine was administrated on a SNS User                        |
| UC10   | As a Nurse, I register what type of vaccine/brand/lot was administrated on a SNS User        |
| UC11   | As a Nurse, I register adverse reactions of a SNS User                                       |
| UC12   | As the System, I trigger a notification to alert that a SNS User can leave the recovery room |
| UC13   | As a Nurse, I request and deliver a vaccination certificate to a SNS User                    |
| UC14   | As a SNS User, I request a vaccination certificate                                           |
| UC15   | As an Administrator, I register a new vaccination center                                     |
| UC16   | As an Administrator, I register a new employee                                               |
| UC17   | As an Administrator, I register new SNS User                                                 |
| UC18   | As an Administrator, I register new vaccine                                                  |
| UC19   | As an Administrator, I register new type of vaccine                                          |
| UC20   | As a Coordinator, I monitor the vaccination process by statistics/charts/reports             |
| UC21   | As a Coordinator, I access other centers data, including law systems                         |