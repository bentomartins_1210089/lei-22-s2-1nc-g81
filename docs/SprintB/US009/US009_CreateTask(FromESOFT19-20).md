# US 009 - To create a Task 

## 1. Requirements Engineering


### 1.1. User Story Description


As an administrator, I want to register a vaccination center to respond to a certain pandemic.



### 1.2. Customer Specifications and Clarifications 


**From the specifications document:**

>	Each community mass vaccination center is characterized by having by a name, an address, a phone number, an e-mail address, a fax number, a website address, opening and closing hours, slot duration and the maximum number of vaccines that can be given per slot.

>	A vaccination center has one coordinator. 



**From the client clarifications:**

> **Question:** Which is the unit of measurement used to estimate duration?
>  
> **Answer:** Duration is estimated in days.

-

> **Question:** Monetary data is expressed in any particular currency?
>  
> **Answer:** Monetary data (e.g. estimated cost of a task) is indicated in POTs (virtual currency internal to the platform).


### 1.3. Acceptance Criteria


n/a


### 1.4. Found out Dependencies


* There is a dependency to "US10 As an administrator, I want to register an Employee" since at least one employee with the Coordinator category must exist to be added to the vaccination center being created.


### 1.5 Input and Output Data


**Input Data:**

* Typed data:
    * a name, 
    * an address, 
    * a phone number, 
    * an e-mail address, 
    * a fax number, 
    * a website address, 
    * opening and closing hours, 
    * slot duration 
    * maximum number of vaccines that can be given per slot
	
* Selected data:
	* Coordinator category User 


**Output Data:**

* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)

**Alternative 1**

![US009_SSD](US009_SSD.svg)


**Alternative 2**

![US009_SSD_v2](US009_SSD_v2.svg)


**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

n/a

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US009_MD](US009_MD.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization 

### 3.1. Rationale

**SSD - Alternative 1 is adopted.**

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Organization
 * CMCenter

Other software classes (i.e. Pure Fabrication) identified: 

 * CreateCMCenterUI  
 * CreateCMCenterController


## 3.2. Sequence Diagram (SD)

**Alternative 1**

![US009_SD](US009_SD.svg)

**Alternative 2**

![US009_SD](US009_SD_v2.svg)

## 3.3. Class Diagram (CD)

**From alternative 1**

![US009_CD](US009_CD.svg)

# 4. Tests 

**Test 1:** Check that it is not possible to create an instance of the CMCenter class with null values. 

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		CMCenter instance = new CMCenter(null, null, null, null, null, null, null);
	}


*It is also recommended to organize this content by subsections.* 

# 5. Construction (Implementation)

# 6. Integration and Demo 

* A new option on the Main menu options was added.


# 7. Observations

Platform and Organization classes are getting too many responsibilities due to IE pattern and, therefore, they are becoming huge and harder to maintain. 

Is there any way to avoid this to happen?





