# Use Case Diagram (UCD)

![Use Case Diagram](UCD.svg)

| **UC** | **Description**                                                                                             |
|:-------|:------------------------------------------------------------------------------------------------------------|
| UC01*  | As a SNS user, I intend to use the application to schedule a vaccine                                        |
| UC02*  | As a receptionist at one vaccination center, I want to schedule a vaccination                               |
| UC03*  | As a receptionist, I want to register a SNS User                                                            |
| UC04*  | As a receptionist at a vaccination center, I want to register the arrival of a SNS user to take the vaccine |
| UC05*  | As a nurse, I intend to consult the users in the waiting room of a vaccination center                       |
| UC06   | As a SNS user, I accept that the System/DGS can send me notifications                                       |
| UC07   | As the system, I send notifications to the SNS User with the schedule information                           |
| UC08   | As a nurse, I check SNS User info and health conditions                                                     |
| UC09*  | As an administrator, I want to register a vaccination center to respond to a certain pandemic               |
| UC10*  | As an administrator, I want to register an Employee                                                         |
| UC11*  | As an administrator, I want to get a list of Employees with a given function/role                           |
| US12*  | As an administrator, I intend to specify a new vaccine type                                                 |
| US13*  | As an administrator, I intend to specify a new vaccine and its administration process                       |
| UC14*  | As an administrator, I want to load a set of users from a CSV file                                          |
| UC15   | As an administrator, I register new SNS User                                                                |
| UC16   | As a nurse, I register adverse reactions of a SNS User                                                      |
| UC17   | As a nurse, I request and deliver a vaccination certificate to a SNS User                                   |
| UC18*  | As a center coordinator, I want to get a list of all vaccines                                               |
| UC19   | As the system, I trigger a notification to alert that a SNS User can leave the recovery room                |
| UC20   | As a nurse, I check which vaccine and how it should be administrated on a SNS User                          |
| UC21   | As a nurse, I register that a vaccine was administrated on a SNS User                                       |
| UC22   | As a nurse, I register what type of vaccine/brand/lot was administrated on a SNS User                       ||
| UC23   | As a SNS user, I request a vaccination certificate                                                          |
| UC24   | As a coordinator, I monitor the vaccination process by statistics/charts/reports                            |
| UC25   | As a coordinator, I access other centers data, including law systems                                        |

* Defined by the client