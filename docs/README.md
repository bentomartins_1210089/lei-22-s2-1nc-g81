# Integrating Project for the 2nd Semester of LEI-ISEP 2021-22 

# 1. Team Members - ESOFT only

The teams consists of students identified in the following table. 

| Student Number	 | Name          |
|-----------------|---------------|
| **1210089**     | Bento Martins |
| **1980292**     | Sérgio Soares |
| **1170065**     | Tassio Gomes  |

# 2. Task Distribution - ESOFT only ###


Throughout the project's development period, the distribution of _tasks / requirements / features_ by the team members was carried out as described in the following table. 

**Keep this table must always up-to-date.**

| Task                                                                                   | [Sprint A]                 | [Sprint B]                        | [Sprint C]                        | [Sprint D]                        |
|----------------------------------------------------------------------------------------|----------------------------|-----------------------------------|-----------------------------------|-----------------------------------|
| Glossary                                                                               | [all](SprintA/Glossary.md) | [all](SprintB/Glossary.md)        | [all](SprintC/Glossary.md)        | [all](SprintD/Glossary.md)        |
| Use Case Diagram (UCD)                                                                 | [all](SprintA/UCD.md)      | [all](SprintB/UCD.md)             | [all](SprintC/UCD.md)             | [all](SprintD/UCD.md)             |
| Supplementary Specification                                                            | [all](SprintA/FURPS.md)    | [all](SprintB/FURPS.md)           | [all](SprintC/FURPS.md)           | [all](SprintD/FURPS.md)           |
| Domain Model                                                                           | [all](SprintA/DM.md)       | [all](SprintB/DM.md)              | [all](SprintC/DM.md)              | [all](SprintD/DM.md)              |
| US 003 - Register a new SNS User                                                       |                            | [1210089](SprintB/US003/US003.md) |                                   |                                   |
| US 009 - Register a vaccination center to respond to certain pandemic                  |                            | [1980292](SprintB/US009/US009.md) |                                   |                                   |
| US 011 - Get a list of Employees with a given function/role                            |                            | [1170065](SprintB/US011/US011.md) |                                   |                                   |
| US 001 - Use the application to schedule a vaccine                                     |                            |                                   | [1170065](SprintC/US001/US001.md) |                                   |
| US 005 - Consult the users in the waiting room of a vacination center                  |                            |                                   | [1980292](SprintC/US005/US005.md) |                                   |
| US 014 - Load a set of users from a CSV file                                           |                            |                                   | [1210089](SprintC/US014/US014.md) |                                   |
| US 006 - Record daily the total number of people vaccinated in each vaccination center |                            |                                   |                                   | [1170065](SprintD/US006/US006.md) |
| US 015 - Check and export vaccination statistics                                       |                            |                                   |                                   | [1210089](SprintD/US015/US015.md) |

