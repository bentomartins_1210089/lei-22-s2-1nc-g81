# Supplementary Specification (FURPS+)

## Functionality

- Authentication to all users;
- Passwords must hold seven alphanumeric characters, including three capital letters and two digits;

## Usability

_No Usability Constraints were identified_

## Reliability

_No Reliability Constraints were identified_

## Performance

_No Performance Constraints were identified_

## Supportability

- The application must support, at least, the Portuguese and the English languages;
- Unit tests for all methods, except for methods that implement Input/Output operations;
- Coverage reports should be made and used;

### Design Constraints

- Developed in **Java language** using the **IntelliJ IDE** or **NetBeans**;
- The graphical interface must be developed in **JavaFX 11**;
- Adopt best practices for identifying requirements, and for **OO software analysis and design**;
- Adopt recognized coding standards (e.g., **CamelCase**);
- Use **Javadoc** to generate useful documentation for Java code;
- Unit tests should be implemented using the **JUnit 5** framework.
- The **JaCoCo plugin** should be used to generate the coverage report;
- All the images/figures produced during the software development process should be recorded in **SVG format**;
- **FIFO** should be the choice to handle the queues;
- Use of data serialization to ensure data persistence;

### Implementation Constraints

_No Implementation Constraints were identified_

### Interface Constraints

_No Interface Constraints were identified_

### Physical Constraints

_No Physical Constraints were identified_